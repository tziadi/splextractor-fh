/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package artefact.generic;

import java.math.BigInteger;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Construction Primitive</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link artefact.generic.ConstructionPrimitive#getNext <em>Next</em>}</li>
 *   <li>{@link artefact.generic.ConstructionPrimitive#getId <em>Id</em>}</li>
 *   <li>{@link artefact.generic.ConstructionPrimitive#getLien <em>Lien</em>}</li>
 * </ul>
 * </p>
 *
 * @see artefact.generic.GenericPackage#getConstructionPrimitive()
 * @model
 * @generated
 */
public interface ConstructionPrimitive extends EObject {
	
	 String getlien();
	 void setlien(String l);
	
	
	/**
	 * Returns the value of the '<em><b>Next</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next</em>' reference.
	 * @see #setNext(ConstructionPrimitive)
	 * @see artefact.generic.GenericPackage#getConstructionPrimitive_Next()
	 * @model
	 * @generated
	 */
	ConstructionPrimitive getNext();

	/**
	 * Sets the value of the '{@link artefact.generic.ConstructionPrimitive#getNext <em>Next</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next</em>' reference.
	 * @see #getNext()
	 * @generated
	 */
	void setNext(ConstructionPrimitive value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(BigInteger)
	 * @see artefact.generic.GenericPackage#getConstructionPrimitive_Id()
	 * @model
	 * @generated
	 */
	BigInteger getId();

	/**
	 * Sets the value of the '{@link artefact.generic.ConstructionPrimitive#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(BigInteger value);
	/**
	 * Returns the value of the '<em><b>Lien</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lien</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lien</em>' attribute.
	 * @see #setLien(String)
	 * @see artefact.generic.GenericPackage#getConstructionPrimitive_Lien()
	 * @model
	 * @generated
	 */
	String getLien();
	/**
	 * Sets the value of the '{@link artefact.generic.ConstructionPrimitive#getLien <em>Lien</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lien</em>' attribute.
	 * @see #getLien()
	 * @generated
	 */
	void setLien(String value);

} // ConstructionPrimitive
