/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package artefact.generic;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Artefact</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link artefact.generic.Artefact#getFirst <em>First</em>}</li>
 *   <li>{@link artefact.generic.Artefact#getConstraints <em>Constraints</em>}</li>
 * </ul>
 * </p>
 *
 * @see artefact.generic.GenericPackage#getArtefact()
 * @model
 * @generated
 */
public interface Artefact extends EObject {
	
	ConstructionPrimitive getlast();
        void setLast(ConstructionPrimitive last); 
	public String getDir();
	public void setDir(String dir);
	public int getId() ;

	public void setId(int idd);
	
	/**
	 * Returns the value of the '<em><b>First</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First</em>' containment reference.
	 * @see #setFirst(ConstructionPrimitive)
	 * @see artefact.generic.GenericPackage#getArtefact_First()
	 * @model containment="true"
	 * @generated
	 */
	ConstructionPrimitive getFirst();

	/**
	 * Sets the value of the '{@link artefact.generic.Artefact#getFirst <em>First</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First</em>' containment reference.
	 * @see #getFirst()
	 * @generated
	 */
	void setFirst(ConstructionPrimitive value);

	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraints</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints</em>' reference.
	 * @see #setConstraints(Constraint)
	 * @see artefact.generic.GenericPackage#getArtefact_Constraints()
	 * @model
	 * @generated
	 */
	Constraint getConstraints();

	/**
	 * Sets the value of the '{@link artefact.generic.Artefact#getConstraints <em>Constraints</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constraints</em>' reference.
	 * @see #getConstraints()
	 * @generated
	 */
	void setConstraints(Constraint value);

} // Artefact
