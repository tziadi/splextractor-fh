/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package artefact.generic;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see artefact.generic.GenericFactory
 * @model kind="package"
 * @generated
 */
public interface GenericPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "generic";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://generic/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "generic";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GenericPackage eINSTANCE = artefact.generic.impl.GenericPackageImpl.init();

	/**
	 * The meta object id for the '{@link artefact.generic.impl.ArtefactImpl <em>Artefact</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see artefact.generic.impl.ArtefactImpl
	 * @see artefact.generic.impl.GenericPackageImpl#getArtefact()
	 * @generated
	 */
	int ARTEFACT = 0;

	/**
	 * The feature id for the '<em><b>First</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__FIRST = 0;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT__CONSTRAINTS = 1;

	/**
	 * The number of structural features of the '<em>Artefact</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTEFACT_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link artefact.generic.impl.ConstructionPrimitiveImpl <em>Construction Primitive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see artefact.generic.impl.ConstructionPrimitiveImpl
	 * @see artefact.generic.impl.GenericPackageImpl#getConstructionPrimitive()
	 * @generated
	 */
	int CONSTRUCTION_PRIMITIVE = 1;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRUCTION_PRIMITIVE__NEXT = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRUCTION_PRIMITIVE__ID = 1;

	/**
	 * The feature id for the '<em><b>Lien</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRUCTION_PRIMITIVE__LIEN = 2;

	/**
	 * The number of structural features of the '<em>Construction Primitive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRUCTION_PRIMITIVE_FEATURE_COUNT = 3;


	/**
	 * The meta object id for the '{@link artefact.generic.impl.ConstraintImpl <em>Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see artefact.generic.impl.ConstraintImpl
	 * @see artefact.generic.impl.GenericPackageImpl#getConstraint()
	 * @generated
	 */
	int CONSTRAINT = 2;

	/**
	 * The number of structural features of the '<em>Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_FEATURE_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link artefact.generic.Artefact <em>Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Artefact</em>'.
	 * @see artefact.generic.Artefact
	 * @generated
	 */
	EClass getArtefact();

	/**
	 * Returns the meta object for the containment reference '{@link artefact.generic.Artefact#getFirst <em>First</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>First</em>'.
	 * @see artefact.generic.Artefact#getFirst()
	 * @see #getArtefact()
	 * @generated
	 */
	EReference getArtefact_First();

	/**
	 * Returns the meta object for the reference '{@link artefact.generic.Artefact#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constraints</em>'.
	 * @see artefact.generic.Artefact#getConstraints()
	 * @see #getArtefact()
	 * @generated
	 */
	EReference getArtefact_Constraints();

	/**
	 * Returns the meta object for class '{@link artefact.generic.ConstructionPrimitive <em>Construction Primitive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Construction Primitive</em>'.
	 * @see artefact.generic.ConstructionPrimitive
	 * @generated
	 */
	EClass getConstructionPrimitive();

	/**
	 * Returns the meta object for the reference '{@link artefact.generic.ConstructionPrimitive#getNext <em>Next</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Next</em>'.
	 * @see artefact.generic.ConstructionPrimitive#getNext()
	 * @see #getConstructionPrimitive()
	 * @generated
	 */
	EReference getConstructionPrimitive_Next();

	/**
	 * Returns the meta object for the attribute '{@link artefact.generic.ConstructionPrimitive#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see artefact.generic.ConstructionPrimitive#getId()
	 * @see #getConstructionPrimitive()
	 * @generated
	 */
	EAttribute getConstructionPrimitive_Id();

	/**
	 * Returns the meta object for the attribute '{@link artefact.generic.ConstructionPrimitive#getLien <em>Lien</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lien</em>'.
	 * @see artefact.generic.ConstructionPrimitive#getLien()
	 * @see #getConstructionPrimitive()
	 * @generated
	 */
	EAttribute getConstructionPrimitive_Lien();

	/**
	 * Returns the meta object for class '{@link artefact.generic.Constraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constraint</em>'.
	 * @see artefact.generic.Constraint
	 * @generated
	 */
	EClass getConstraint();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GenericFactory getGenericFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link artefact.generic.impl.ArtefactImpl <em>Artefact</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see artefact.generic.impl.ArtefactImpl
		 * @see artefact.generic.impl.GenericPackageImpl#getArtefact()
		 * @generated
		 */
		EClass ARTEFACT = eINSTANCE.getArtefact();

		/**
		 * The meta object literal for the '<em><b>First</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTEFACT__FIRST = eINSTANCE.getArtefact_First();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTEFACT__CONSTRAINTS = eINSTANCE.getArtefact_Constraints();

		/**
		 * The meta object literal for the '{@link artefact.generic.impl.ConstructionPrimitiveImpl <em>Construction Primitive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see artefact.generic.impl.ConstructionPrimitiveImpl
		 * @see artefact.generic.impl.GenericPackageImpl#getConstructionPrimitive()
		 * @generated
		 */
		EClass CONSTRUCTION_PRIMITIVE = eINSTANCE.getConstructionPrimitive();

		/**
		 * The meta object literal for the '<em><b>Next</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTRUCTION_PRIMITIVE__NEXT = eINSTANCE.getConstructionPrimitive_Next();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTRUCTION_PRIMITIVE__ID = eINSTANCE.getConstructionPrimitive_Id();

		/**
		 * The meta object literal for the '<em><b>Lien</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTRUCTION_PRIMITIVE__LIEN = eINSTANCE.getConstructionPrimitive_Lien();

		/**
		 * The meta object literal for the '{@link artefact.generic.impl.ConstraintImpl <em>Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see artefact.generic.impl.ConstraintImpl
		 * @see artefact.generic.impl.GenericPackageImpl#getConstraint()
		 * @generated
		 */
		EClass CONSTRAINT = eINSTANCE.getConstraint();

	}

} //GenericPackage
