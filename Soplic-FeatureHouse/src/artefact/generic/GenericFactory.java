/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package artefact.generic;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see artefact.generic.GenericPackage
 * @generated
 */
public interface GenericFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GenericFactory eINSTANCE = artefact.generic.impl.GenericFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Artefact</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Artefact</em>'.
	 * @generated
	 */
	Artefact createArtefact();

	/**
	 * Returns a new object of class '<em>Construction Primitive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Construction Primitive</em>'.
	 * @generated
	 */
	ConstructionPrimitive createConstructionPrimitive();

	/**
	 * Returns a new object of class '<em>Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Constraint</em>'.
	 * @generated
	 */
	Constraint createConstraint();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	GenericPackage getGenericPackage();

} //GenericFactory
