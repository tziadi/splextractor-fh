/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package artefact.generic.impl;

import artefact.generic.Artefact;
import artefact.generic.Constraint;
import artefact.generic.ConstructionPrimitive;
import artefact.generic.GenericPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Artefact</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link artefact.generic.impl.ArtefactImpl#getFirst <em>First</em>}</li>
 *   <li>{@link artefact.generic.impl.ArtefactImpl#getConstraints <em>Constraints</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ArtefactImpl extends EObjectImpl implements Artefact {
	
private ConstructionPrimitive last;
private int id;
	
private String dir;	
	

public String getDir() {
	return this.dir;
}

public void setDir(String dir) {
	this.dir=dir;
}
	
	public int getId() {
		return id;
	}

	public void setId(int idd) {
		this.id = idd;
	}
	
	/**
	 * The cached value of the '{@link #getFirst() <em>First</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirst()
	 * @generated
	 * @ordered
	 */
	protected ConstructionPrimitive first;

	/**
	 * The cached value of the '{@link #getConstraints() <em>Constraints</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraints()
	 * @generated
	 * @ordered
	 */
	protected Constraint constraints;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArtefactImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GenericPackage.Literals.ARTEFACT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstructionPrimitive getFirst() {
		return first;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFirst(ConstructionPrimitive newFirst, NotificationChain msgs) {
		ConstructionPrimitive oldFirst = first;
		first = newFirst;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, GenericPackage.ARTEFACT__FIRST, oldFirst, newFirst);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirst(ConstructionPrimitive newFirst) {
		if (newFirst != first) {
			NotificationChain msgs = null;
			if (first != null)
				msgs = ((InternalEObject)first).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GenericPackage.ARTEFACT__FIRST, null, msgs);
			if (newFirst != null)
				msgs = ((InternalEObject)newFirst).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GenericPackage.ARTEFACT__FIRST, null, msgs);
			msgs = basicSetFirst(newFirst, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericPackage.ARTEFACT__FIRST, newFirst, newFirst));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constraint getConstraints() {
		if (constraints != null && constraints.eIsProxy()) {
			InternalEObject oldConstraints = (InternalEObject)constraints;
			constraints = (Constraint)eResolveProxy(oldConstraints);
			if (constraints != oldConstraints) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GenericPackage.ARTEFACT__CONSTRAINTS, oldConstraints, constraints));
			}
		}
		return constraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constraint basicGetConstraints() {
		return constraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstraints(Constraint newConstraints) {
		Constraint oldConstraints = constraints;
		constraints = newConstraints;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GenericPackage.ARTEFACT__CONSTRAINTS, oldConstraints, constraints));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GenericPackage.ARTEFACT__FIRST:
				return basicSetFirst(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GenericPackage.ARTEFACT__FIRST:
				return getFirst();
			case GenericPackage.ARTEFACT__CONSTRAINTS:
				if (resolve) return getConstraints();
				return basicGetConstraints();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GenericPackage.ARTEFACT__FIRST:
				setFirst((ConstructionPrimitive)newValue);
				return;
			case GenericPackage.ARTEFACT__CONSTRAINTS:
				setConstraints((Constraint)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GenericPackage.ARTEFACT__FIRST:
				setFirst((ConstructionPrimitive)null);
				return;
			case GenericPackage.ARTEFACT__CONSTRAINTS:
				setConstraints((Constraint)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GenericPackage.ARTEFACT__FIRST:
				return first != null;
			case GenericPackage.ARTEFACT__CONSTRAINTS:
				return constraints != null;
		}
		return super.eIsSet(featureID);
	}

	@Override
	public ConstructionPrimitive getlast() {
		// TODO Auto-generated method stub
		return last;
	}

	@Override
	public void setLast(ConstructionPrimitive last) {
		// TODO Auto-generated method stub
		this.last=last;
	}

} //ArtefactImpl
