/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package artefact.generic;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see artefact.generic.GenericPackage#getConstraint()
 * @model
 * @generated
 */
public interface Constraint extends EObject {
} // Constraint
