/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package artefact.FST.impl;

import artefact.FST.FSTPackage;
import artefact.FST.FeatureTreeSet;

import artefact.generic.impl.ArtefactImpl;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature Tree Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class FeatureTreeSetImpl extends ArtefactImpl implements FeatureTreeSet {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureTreeSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FSTPackage.Literals.FEATURE_TREE_SET;
	}

} //FeatureTreeSetImpl
