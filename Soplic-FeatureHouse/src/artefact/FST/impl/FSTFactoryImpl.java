/**
 */
package artefact.FST.impl;

import artefact.FST.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FSTFactoryImpl extends EFactoryImpl implements FSTFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FSTFactory init() {
		try {
			FSTFactory theFSTFactory = (FSTFactory)EPackage.Registry.INSTANCE.getEFactory("http://fst/1.0"); 
			if (theFSTFactory != null) {
				return theFSTFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new FSTFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSTFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case FSTPackage.CREATE_FST_TERMINAL: return createCreateFSTTerminal();
			case FSTPackage.CREATE_FST_NON_TERMINAL: return createCreateFSTNonTerminal();
			case FSTPackage.CREATE_FST_NODE: return createCreateFSTNode();
			case FSTPackage.FEATURE_TREE_SET: return createFeatureTreeSet();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CreateFSTTerminal createCreateFSTTerminal() {
		CreateFSTTerminalImpl createFSTTerminal = new CreateFSTTerminalImpl();
		return createFSTTerminal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CreateFSTNonTerminal createCreateFSTNonTerminal() {
		CreateFSTNonTerminalImpl createFSTNonTerminal = new CreateFSTNonTerminalImpl();
		return createFSTNonTerminal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CreateFSTNode createCreateFSTNode() {
		CreateFSTNodeImpl createFSTNode = new CreateFSTNodeImpl();
		return createFSTNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureTreeSet createFeatureTreeSet() {
		FeatureTreeSetImpl featureTreeSet = new FeatureTreeSetImpl();
		return featureTreeSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSTPackage getFSTPackage() {
		return (FSTPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static FSTPackage getPackage() {
		return FSTPackage.eINSTANCE;
	}

} //FSTFactoryImpl
