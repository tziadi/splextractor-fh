/**
 */
package artefact.FST.impl;

import artefact.FST.CreateFSTNode;
import artefact.FST.CreateFSTNonTerminal;
import artefact.FST.CreateFSTTerminal;
import artefact.FST.FSTFactory;
import artefact.FST.FSTPackage;

import artefact.FST.FeatureTreeSet;
import artefact.generic.GenericPackage;

import artefact.generic.impl.GenericPackageImpl;

import artefact.umlClassDiagram.UmlClassDiagramPackage;

import artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FSTPackageImpl extends EPackageImpl implements FSTPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass createFSTTerminalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass createFSTNonTerminalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass createFSTNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureTreeSetEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see artefact.FST.FSTPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private FSTPackageImpl() {
		super(eNS_URI, FSTFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link FSTPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static FSTPackage init() {
		if (isInited) return (FSTPackage)EPackage.Registry.INSTANCE.getEPackage(FSTPackage.eNS_URI);

		// Obtain or create and register package
		FSTPackageImpl theFSTPackage = (FSTPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof FSTPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new FSTPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		GenericPackageImpl theGenericPackage = (GenericPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GenericPackage.eNS_URI) instanceof GenericPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GenericPackage.eNS_URI) : GenericPackage.eINSTANCE);
		UmlClassDiagramPackageImpl theUmlClassDiagramPackage = (UmlClassDiagramPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UmlClassDiagramPackage.eNS_URI) instanceof UmlClassDiagramPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UmlClassDiagramPackage.eNS_URI) : UmlClassDiagramPackage.eINSTANCE);

		// Create package meta-data objects
		theFSTPackage.createPackageContents();
		theGenericPackage.createPackageContents();
		theUmlClassDiagramPackage.createPackageContents();

		// Initialize created meta-data
		theFSTPackage.initializePackageContents();
		theGenericPackage.initializePackageContents();
		theUmlClassDiagramPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theFSTPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(FSTPackage.eNS_URI, theFSTPackage);
		return theFSTPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCreateFSTTerminal() {
		return createFSTTerminalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreateFSTTerminal_Body() {
		return (EAttribute)createFSTTerminalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreateFSTTerminal_Prefix() {
		return (EAttribute)createFSTTerminalEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCreateFSTNonTerminal() {
		return createFSTNonTerminalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCreateFSTNode() {
		return createFSTNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreateFSTNode_Name() {
		return (EAttribute)createFSTNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreateFSTNode_Type() {
		return (EAttribute)createFSTNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreateFSTNode_Index() {
		return (EAttribute)createFSTNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreateFSTNode_Parent() {
		return (EAttribute)createFSTNodeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeatureTreeSet() {
		return featureTreeSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSTFactory getFSTFactory() {
		return (FSTFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		createFSTTerminalEClass = createEClass(CREATE_FST_TERMINAL);
		createEAttribute(createFSTTerminalEClass, CREATE_FST_TERMINAL__BODY);
		createEAttribute(createFSTTerminalEClass, CREATE_FST_TERMINAL__PREFIX);

		createFSTNonTerminalEClass = createEClass(CREATE_FST_NON_TERMINAL);

		createFSTNodeEClass = createEClass(CREATE_FST_NODE);
		createEAttribute(createFSTNodeEClass, CREATE_FST_NODE__NAME);
		createEAttribute(createFSTNodeEClass, CREATE_FST_NODE__TYPE);
		createEAttribute(createFSTNodeEClass, CREATE_FST_NODE__INDEX);
		createEAttribute(createFSTNodeEClass, CREATE_FST_NODE__PARENT);

		featureTreeSetEClass = createEClass(FEATURE_TREE_SET);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		GenericPackage theGenericPackage = (GenericPackage)EPackage.Registry.INSTANCE.getEPackage(GenericPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		createFSTTerminalEClass.getESuperTypes().add(this.getCreateFSTNode());
		createFSTNonTerminalEClass.getESuperTypes().add(this.getCreateFSTNode());
		createFSTNodeEClass.getESuperTypes().add(theGenericPackage.getConstructionPrimitive());
		featureTreeSetEClass.getESuperTypes().add(theGenericPackage.getArtefact());

		// Initialize classes and features; add operations and parameters
		initEClass(createFSTTerminalEClass, CreateFSTTerminal.class, "CreateFSTTerminal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCreateFSTTerminal_Body(), ecorePackage.getEString(), "body", null, 0, 1, CreateFSTTerminal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCreateFSTTerminal_Prefix(), ecorePackage.getEString(), "prefix", null, 0, 1, CreateFSTTerminal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(createFSTNonTerminalEClass, CreateFSTNonTerminal.class, "CreateFSTNonTerminal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(createFSTNodeEClass, CreateFSTNode.class, "CreateFSTNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCreateFSTNode_Name(), ecorePackage.getEString(), "name", null, 0, 1, CreateFSTNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCreateFSTNode_Type(), ecorePackage.getEString(), "type", null, 0, 1, CreateFSTNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCreateFSTNode_Index(), ecorePackage.getEInt(), "index", null, 0, 1, CreateFSTNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCreateFSTNode_Parent(), ecorePackage.getEString(), "parent", null, 0, 1, CreateFSTNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(featureTreeSetEClass, FeatureTreeSet.class, "FeatureTreeSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //FSTPackageImpl
