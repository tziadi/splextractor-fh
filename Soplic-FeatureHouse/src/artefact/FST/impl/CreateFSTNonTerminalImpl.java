/**
 */
package artefact.FST.impl;

import artefact.FST.CreateFSTNonTerminal;
import artefact.FST.FSTPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Create FST Non Terminal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class CreateFSTNonTerminalImpl extends CreateFSTNodeImpl implements CreateFSTNonTerminal {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CreateFSTNonTerminalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FSTPackage.Literals.CREATE_FST_NON_TERMINAL;
	}

} //CreateFSTNonTerminalImpl
