/**
 */
package artefact.FST;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Create FST Non Terminal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see artefact.FST.FSTPackage#getCreateFSTNonTerminal()
 * @model
 * @generated
 */
public interface CreateFSTNonTerminal extends CreateFSTNode {
} // CreateFSTNonTerminal
