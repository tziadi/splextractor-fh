/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package artefact.FST;

import artefact.generic.Artefact;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Tree Set</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see artefact.FST.FSTPackage#getFeatureTreeSet()
 * @model
 * @generated
 */
public interface FeatureTreeSet extends Artefact {
} // FeatureTreeSet
