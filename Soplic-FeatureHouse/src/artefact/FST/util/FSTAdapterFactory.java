/**
 */
package artefact.FST.util;

import artefact.FST.*;

import artefact.generic.Artefact;
import artefact.generic.ConstructionPrimitive;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see artefact.FST.FSTPackage
 * @generated
 */
public class FSTAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static FSTPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FSTAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = FSTPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FSTSwitch<Adapter> modelSwitch =
		new FSTSwitch<Adapter>() {
			@Override
			public Adapter caseCreateFSTTerminal(CreateFSTTerminal object) {
				return createCreateFSTTerminalAdapter();
			}
			@Override
			public Adapter caseCreateFSTNonTerminal(CreateFSTNonTerminal object) {
				return createCreateFSTNonTerminalAdapter();
			}
			@Override
			public Adapter caseCreateFSTNode(CreateFSTNode object) {
				return createCreateFSTNodeAdapter();
			}
			@Override
			public Adapter caseFeatureTreeSet(FeatureTreeSet object) {
				return createFeatureTreeSetAdapter();
			}
			@Override
			public Adapter caseConstructionPrimitive(ConstructionPrimitive object) {
				return createConstructionPrimitiveAdapter();
			}
			@Override
			public Adapter caseArtefact(Artefact object) {
				return createArtefactAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link artefact.FST.CreateFSTTerminal <em>Create FST Terminal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see artefact.FST.CreateFSTTerminal
	 * @generated
	 */
	public Adapter createCreateFSTTerminalAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link artefact.FST.CreateFSTNonTerminal <em>Create FST Non Terminal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see artefact.FST.CreateFSTNonTerminal
	 * @generated
	 */
	public Adapter createCreateFSTNonTerminalAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link artefact.FST.CreateFSTNode <em>Create FST Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see artefact.FST.CreateFSTNode
	 * @generated
	 */
	public Adapter createCreateFSTNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link artefact.FST.FeatureTreeSet <em>Feature Tree Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see artefact.FST.FeatureTreeSet
	 * @generated
	 */
	public Adapter createFeatureTreeSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link artefact.generic.ConstructionPrimitive <em>Construction Primitive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see artefact.generic.ConstructionPrimitive
	 * @generated
	 */
	public Adapter createConstructionPrimitiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link artefact.generic.Artefact <em>Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see artefact.generic.Artefact
	 * @generated
	 */
	public Adapter createArtefactAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //FSTAdapterFactory
