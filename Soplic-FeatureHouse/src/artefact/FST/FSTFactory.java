/**
 */
package artefact.FST;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see artefact.FST.FSTPackage
 * @generated
 */
public interface FSTFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FSTFactory eINSTANCE = artefact.FST.impl.FSTFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Create FST Terminal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Create FST Terminal</em>'.
	 * @generated
	 */
	CreateFSTTerminal createCreateFSTTerminal();

	/**
	 * Returns a new object of class '<em>Create FST Non Terminal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Create FST Non Terminal</em>'.
	 * @generated
	 */
	CreateFSTNonTerminal createCreateFSTNonTerminal();

	/**
	 * Returns a new object of class '<em>Create FST Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Create FST Node</em>'.
	 * @generated
	 */
	CreateFSTNode createCreateFSTNode();

	/**
	 * Returns a new object of class '<em>Feature Tree Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Tree Set</em>'.
	 * @generated
	 */
	FeatureTreeSet createFeatureTreeSet();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	FSTPackage getFSTPackage();

} //FSTFactory
