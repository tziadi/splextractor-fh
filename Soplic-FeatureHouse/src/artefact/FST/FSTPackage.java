/**
 */
package artefact.FST;

import artefact.generic.GenericPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see artefact.FST.FSTFactory
 * @model kind="package"
 * @generated
 */
public interface FSTPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "FST";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://fst/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "fst";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FSTPackage eINSTANCE = artefact.FST.impl.FSTPackageImpl.init();

	/**
	 * The meta object id for the '{@link artefact.FST.impl.CreateFSTNodeImpl <em>Create FST Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see artefact.FST.impl.CreateFSTNodeImpl
	 * @see artefact.FST.impl.FSTPackageImpl#getCreateFSTNode()
	 * @generated
	 */
	int CREATE_FST_NODE = 2;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_NODE__NEXT = GenericPackage.CONSTRUCTION_PRIMITIVE__NEXT;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_NODE__ID = GenericPackage.CONSTRUCTION_PRIMITIVE__ID;

	/**
	 * The feature id for the '<em><b>Lien</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_NODE__LIEN = GenericPackage.CONSTRUCTION_PRIMITIVE__LIEN;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_NODE__NAME = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_NODE__TYPE = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_NODE__INDEX = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_NODE__PARENT = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Create FST Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_NODE_FEATURE_COUNT = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link artefact.FST.impl.CreateFSTTerminalImpl <em>Create FST Terminal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see artefact.FST.impl.CreateFSTTerminalImpl
	 * @see artefact.FST.impl.FSTPackageImpl#getCreateFSTTerminal()
	 * @generated
	 */
	int CREATE_FST_TERMINAL = 0;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_TERMINAL__NEXT = CREATE_FST_NODE__NEXT;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_TERMINAL__ID = CREATE_FST_NODE__ID;

	/**
	 * The feature id for the '<em><b>Lien</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_TERMINAL__LIEN = CREATE_FST_NODE__LIEN;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_TERMINAL__NAME = CREATE_FST_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_TERMINAL__TYPE = CREATE_FST_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_TERMINAL__INDEX = CREATE_FST_NODE__INDEX;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_TERMINAL__PARENT = CREATE_FST_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Body</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_TERMINAL__BODY = CREATE_FST_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_TERMINAL__PREFIX = CREATE_FST_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Create FST Terminal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_TERMINAL_FEATURE_COUNT = CREATE_FST_NODE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link artefact.FST.impl.CreateFSTNonTerminalImpl <em>Create FST Non Terminal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see artefact.FST.impl.CreateFSTNonTerminalImpl
	 * @see artefact.FST.impl.FSTPackageImpl#getCreateFSTNonTerminal()
	 * @generated
	 */
	int CREATE_FST_NON_TERMINAL = 1;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_NON_TERMINAL__NEXT = CREATE_FST_NODE__NEXT;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_NON_TERMINAL__ID = CREATE_FST_NODE__ID;

	/**
	 * The feature id for the '<em><b>Lien</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_NON_TERMINAL__LIEN = CREATE_FST_NODE__LIEN;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_NON_TERMINAL__NAME = CREATE_FST_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_NON_TERMINAL__TYPE = CREATE_FST_NODE__TYPE;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_NON_TERMINAL__INDEX = CREATE_FST_NODE__INDEX;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_NON_TERMINAL__PARENT = CREATE_FST_NODE__PARENT;

	/**
	 * The number of structural features of the '<em>Create FST Non Terminal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_FST_NON_TERMINAL_FEATURE_COUNT = CREATE_FST_NODE_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link artefact.FST.impl.FeatureTreeSetImpl <em>Feature Tree Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see artefact.FST.impl.FeatureTreeSetImpl
	 * @see artefact.FST.impl.FSTPackageImpl#getFeatureTreeSet()
	 * @generated
	 */
	int FEATURE_TREE_SET = 3;

	/**
	 * The feature id for the '<em><b>First</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TREE_SET__FIRST = GenericPackage.ARTEFACT__FIRST;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TREE_SET__CONSTRAINTS = GenericPackage.ARTEFACT__CONSTRAINTS;

	/**
	 * The number of structural features of the '<em>Feature Tree Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_TREE_SET_FEATURE_COUNT = GenericPackage.ARTEFACT_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link artefact.FST.CreateFSTTerminal <em>Create FST Terminal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Create FST Terminal</em>'.
	 * @see artefact.FST.CreateFSTTerminal
	 * @generated
	 */
	EClass getCreateFSTTerminal();

	/**
	 * Returns the meta object for the attribute '{@link artefact.FST.CreateFSTTerminal#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Body</em>'.
	 * @see artefact.FST.CreateFSTTerminal#getBody()
	 * @see #getCreateFSTTerminal()
	 * @generated
	 */
	EAttribute getCreateFSTTerminal_Body();

	/**
	 * Returns the meta object for the attribute '{@link artefact.FST.CreateFSTTerminal#getPrefix <em>Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Prefix</em>'.
	 * @see artefact.FST.CreateFSTTerminal#getPrefix()
	 * @see #getCreateFSTTerminal()
	 * @generated
	 */
	EAttribute getCreateFSTTerminal_Prefix();

	/**
	 * Returns the meta object for class '{@link artefact.FST.CreateFSTNonTerminal <em>Create FST Non Terminal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Create FST Non Terminal</em>'.
	 * @see artefact.FST.CreateFSTNonTerminal
	 * @generated
	 */
	EClass getCreateFSTNonTerminal();

	/**
	 * Returns the meta object for class '{@link artefact.FST.CreateFSTNode <em>Create FST Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Create FST Node</em>'.
	 * @see artefact.FST.CreateFSTNode
	 * @generated
	 */
	EClass getCreateFSTNode();

	/**
	 * Returns the meta object for the attribute '{@link artefact.FST.CreateFSTNode#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see artefact.FST.CreateFSTNode#getName()
	 * @see #getCreateFSTNode()
	 * @generated
	 */
	EAttribute getCreateFSTNode_Name();

	/**
	 * Returns the meta object for the attribute '{@link artefact.FST.CreateFSTNode#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see artefact.FST.CreateFSTNode#getType()
	 * @see #getCreateFSTNode()
	 * @generated
	 */
	EAttribute getCreateFSTNode_Type();

	/**
	 * Returns the meta object for the attribute '{@link artefact.FST.CreateFSTNode#getIndex <em>Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Index</em>'.
	 * @see artefact.FST.CreateFSTNode#getIndex()
	 * @see #getCreateFSTNode()
	 * @generated
	 */
	EAttribute getCreateFSTNode_Index();

	/**
	 * Returns the meta object for the attribute '{@link artefact.FST.CreateFSTNode#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Parent</em>'.
	 * @see artefact.FST.CreateFSTNode#getParent()
	 * @see #getCreateFSTNode()
	 * @generated
	 */
	EAttribute getCreateFSTNode_Parent();

	/**
	 * Returns the meta object for class '{@link artefact.FST.FeatureTreeSet <em>Feature Tree Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Tree Set</em>'.
	 * @see artefact.FST.FeatureTreeSet
	 * @generated
	 */
	EClass getFeatureTreeSet();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FSTFactory getFSTFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link artefact.FST.impl.CreateFSTTerminalImpl <em>Create FST Terminal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see artefact.FST.impl.CreateFSTTerminalImpl
		 * @see artefact.FST.impl.FSTPackageImpl#getCreateFSTTerminal()
		 * @generated
		 */
		EClass CREATE_FST_TERMINAL = eINSTANCE.getCreateFSTTerminal();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATE_FST_TERMINAL__BODY = eINSTANCE.getCreateFSTTerminal_Body();

		/**
		 * The meta object literal for the '<em><b>Prefix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATE_FST_TERMINAL__PREFIX = eINSTANCE.getCreateFSTTerminal_Prefix();

		/**
		 * The meta object literal for the '{@link artefact.FST.impl.CreateFSTNonTerminalImpl <em>Create FST Non Terminal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see artefact.FST.impl.CreateFSTNonTerminalImpl
		 * @see artefact.FST.impl.FSTPackageImpl#getCreateFSTNonTerminal()
		 * @generated
		 */
		EClass CREATE_FST_NON_TERMINAL = eINSTANCE.getCreateFSTNonTerminal();

		/**
		 * The meta object literal for the '{@link artefact.FST.impl.CreateFSTNodeImpl <em>Create FST Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see artefact.FST.impl.CreateFSTNodeImpl
		 * @see artefact.FST.impl.FSTPackageImpl#getCreateFSTNode()
		 * @generated
		 */
		EClass CREATE_FST_NODE = eINSTANCE.getCreateFSTNode();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATE_FST_NODE__NAME = eINSTANCE.getCreateFSTNode_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATE_FST_NODE__TYPE = eINSTANCE.getCreateFSTNode_Type();

		/**
		 * The meta object literal for the '<em><b>Index</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATE_FST_NODE__INDEX = eINSTANCE.getCreateFSTNode_Index();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATE_FST_NODE__PARENT = eINSTANCE.getCreateFSTNode_Parent();

		/**
		 * The meta object literal for the '{@link artefact.FST.impl.FeatureTreeSetImpl <em>Feature Tree Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see artefact.FST.impl.FeatureTreeSetImpl
		 * @see artefact.FST.impl.FSTPackageImpl#getFeatureTreeSet()
		 * @generated
		 */
		EClass FEATURE_TREE_SET = eINSTANCE.getFeatureTreeSet();

	}

} //FSTPackage
