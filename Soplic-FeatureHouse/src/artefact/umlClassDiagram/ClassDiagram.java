/**
 */
package artefact.umlClassDiagram;

import artefact.generic.Artefact;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Diagram</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see artefact.umlClassDiagram.UmlClassDiagramPackage#getClassDiagram()
 * @model
 * @generated
 */
public interface ClassDiagram extends Artefact {
} // ClassDiagram
