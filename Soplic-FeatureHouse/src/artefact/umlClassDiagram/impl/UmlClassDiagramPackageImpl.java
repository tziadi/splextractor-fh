/**
 */
package artefact.umlClassDiagram.impl;

import artefact.FST.FSTPackage;

import artefact.FST.impl.FSTPackageImpl;

import artefact.generic.GenericPackage;

import artefact.generic.impl.GenericPackageImpl;

import artefact.umlClassDiagram.AssociationInvariant;
import artefact.umlClassDiagram.ClassDiagram;
import artefact.umlClassDiagram.CreateAssociation;
import artefact.umlClassDiagram.CreateAttribute;
import artefact.umlClassDiagram.CreateClass;
import artefact.umlClassDiagram.CreateGeneralization;
import artefact.umlClassDiagram.CreateInterface;
import artefact.umlClassDiagram.CreateOperation;
import artefact.umlClassDiagram.CreateOperationRefinement;
import artefact.umlClassDiagram.CreatePackage;
import artefact.umlClassDiagram.UmlClassDiagramFactory;
import artefact.umlClassDiagram.UmlClassDiagramPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UmlClassDiagramPackageImpl extends EPackageImpl implements UmlClassDiagramPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass classDiagramEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass createClassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass createAssociationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass createAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass createOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass createGeneralizationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass associationInvariantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass createPackageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass createOperationRefinementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass createInterfaceEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see artefact.umlClassDiagram.UmlClassDiagramPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private UmlClassDiagramPackageImpl() {
		super(eNS_URI, UmlClassDiagramFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link UmlClassDiagramPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static UmlClassDiagramPackage init() {
		if (isInited) return (UmlClassDiagramPackage)EPackage.Registry.INSTANCE.getEPackage(UmlClassDiagramPackage.eNS_URI);

		// Obtain or create and register package
		UmlClassDiagramPackageImpl theUmlClassDiagramPackage = (UmlClassDiagramPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof UmlClassDiagramPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new UmlClassDiagramPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		GenericPackageImpl theGenericPackage = (GenericPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(GenericPackage.eNS_URI) instanceof GenericPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(GenericPackage.eNS_URI) : GenericPackage.eINSTANCE);
		FSTPackageImpl theFSTPackage = (FSTPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(FSTPackage.eNS_URI) instanceof FSTPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(FSTPackage.eNS_URI) : FSTPackage.eINSTANCE);

		// Create package meta-data objects
		theUmlClassDiagramPackage.createPackageContents();
		theGenericPackage.createPackageContents();
		theFSTPackage.createPackageContents();

		// Initialize created meta-data
		theUmlClassDiagramPackage.initializePackageContents();
		theGenericPackage.initializePackageContents();
		theFSTPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theUmlClassDiagramPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(UmlClassDiagramPackage.eNS_URI, theUmlClassDiagramPackage);
		return theUmlClassDiagramPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClassDiagram() {
		return classDiagramEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCreateClass() {
		return createClassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreateClass_Name() {
		return (EAttribute)createClassEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreateClass_Owener() {
		return (EAttribute)createClassEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCreateAssociation() {
		return createAssociationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreateAssociation_Source() {
		return (EAttribute)createAssociationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreateAssociation_Target() {
		return (EAttribute)createAssociationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCreateAttribute() {
		return createAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreateAttribute_Name() {
		return (EAttribute)createAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreateAttribute_Owner() {
		return (EAttribute)createAttributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreateAttribute_Type() {
		return (EAttribute)createAttributeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCreateOperation() {
		return createOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreateOperation_Name() {
		return (EAttribute)createOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreateOperation_Owener() {
		return (EAttribute)createOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCreateGeneralization() {
		return createGeneralizationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreateGeneralization_Sub() {
		return (EAttribute)createGeneralizationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreateGeneralization_Super() {
		return (EAttribute)createGeneralizationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAssociationInvariant() {
		return associationInvariantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCreatePackage() {
		return createPackageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreatePackage_Name() {
		return (EAttribute)createPackageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCreateOperationRefinement() {
		return createOperationRefinementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCreateOperationRefinement_Super() {
		return (EAttribute)createOperationRefinementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCreateInterface() {
		return createInterfaceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UmlClassDiagramFactory getUmlClassDiagramFactory() {
		return (UmlClassDiagramFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		classDiagramEClass = createEClass(CLASS_DIAGRAM);

		createClassEClass = createEClass(CREATE_CLASS);
		createEAttribute(createClassEClass, CREATE_CLASS__NAME);
		createEAttribute(createClassEClass, CREATE_CLASS__OWENER);

		createAssociationEClass = createEClass(CREATE_ASSOCIATION);
		createEAttribute(createAssociationEClass, CREATE_ASSOCIATION__SOURCE);
		createEAttribute(createAssociationEClass, CREATE_ASSOCIATION__TARGET);

		createAttributeEClass = createEClass(CREATE_ATTRIBUTE);
		createEAttribute(createAttributeEClass, CREATE_ATTRIBUTE__NAME);
		createEAttribute(createAttributeEClass, CREATE_ATTRIBUTE__OWNER);
		createEAttribute(createAttributeEClass, CREATE_ATTRIBUTE__TYPE);

		createOperationEClass = createEClass(CREATE_OPERATION);
		createEAttribute(createOperationEClass, CREATE_OPERATION__NAME);
		createEAttribute(createOperationEClass, CREATE_OPERATION__OWENER);

		createGeneralizationEClass = createEClass(CREATE_GENERALIZATION);
		createEAttribute(createGeneralizationEClass, CREATE_GENERALIZATION__SUB);
		createEAttribute(createGeneralizationEClass, CREATE_GENERALIZATION__SUPER);

		associationInvariantEClass = createEClass(ASSOCIATION_INVARIANT);

		createPackageEClass = createEClass(CREATE_PACKAGE);
		createEAttribute(createPackageEClass, CREATE_PACKAGE__NAME);

		createOperationRefinementEClass = createEClass(CREATE_OPERATION_REFINEMENT);
		createEAttribute(createOperationRefinementEClass, CREATE_OPERATION_REFINEMENT__SUPER);

		createInterfaceEClass = createEClass(CREATE_INTERFACE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		GenericPackage theGenericPackage = (GenericPackage)EPackage.Registry.INSTANCE.getEPackage(GenericPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		classDiagramEClass.getESuperTypes().add(theGenericPackage.getArtefact());
		createClassEClass.getESuperTypes().add(theGenericPackage.getConstructionPrimitive());
		createAssociationEClass.getESuperTypes().add(theGenericPackage.getConstructionPrimitive());
		createAttributeEClass.getESuperTypes().add(theGenericPackage.getConstructionPrimitive());
		createOperationEClass.getESuperTypes().add(theGenericPackage.getConstructionPrimitive());
		createGeneralizationEClass.getESuperTypes().add(theGenericPackage.getConstructionPrimitive());
		associationInvariantEClass.getESuperTypes().add(theGenericPackage.getConstraint());
		createPackageEClass.getESuperTypes().add(theGenericPackage.getConstructionPrimitive());
		createOperationRefinementEClass.getESuperTypes().add(this.getCreateOperation());
		createInterfaceEClass.getESuperTypes().add(this.getCreateClass());

		// Initialize classes and features; add operations and parameters
		initEClass(classDiagramEClass, ClassDiagram.class, "ClassDiagram", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(createClassEClass, CreateClass.class, "CreateClass", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCreateClass_Name(), ecorePackage.getEString(), "name", null, 0, 1, CreateClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCreateClass_Owener(), ecorePackage.getEString(), "owener", null, 0, 1, CreateClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(createAssociationEClass, CreateAssociation.class, "CreateAssociation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCreateAssociation_Source(), ecorePackage.getEString(), "source", null, 0, 1, CreateAssociation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCreateAssociation_Target(), ecorePackage.getEString(), "target", null, 0, 1, CreateAssociation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(createAttributeEClass, CreateAttribute.class, "CreateAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCreateAttribute_Name(), ecorePackage.getEString(), "name", null, 0, 1, CreateAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCreateAttribute_Owner(), ecorePackage.getEString(), "owner", null, 0, 1, CreateAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCreateAttribute_Type(), ecorePackage.getEString(), "type", null, 0, 1, CreateAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(createOperationEClass, CreateOperation.class, "CreateOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCreateOperation_Name(), ecorePackage.getEString(), "name", null, 0, 1, CreateOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCreateOperation_Owener(), ecorePackage.getEString(), "owener", null, 0, 1, CreateOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(createGeneralizationEClass, CreateGeneralization.class, "CreateGeneralization", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCreateGeneralization_Sub(), ecorePackage.getEString(), "sub", null, 0, 1, CreateGeneralization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCreateGeneralization_Super(), ecorePackage.getEString(), "super", null, 0, 1, CreateGeneralization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(associationInvariantEClass, AssociationInvariant.class, "AssociationInvariant", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(createPackageEClass, CreatePackage.class, "CreatePackage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCreatePackage_Name(), ecorePackage.getEString(), "name", null, 0, 1, CreatePackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(createOperationRefinementEClass, CreateOperationRefinement.class, "CreateOperationRefinement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCreateOperationRefinement_Super(), ecorePackage.getEString(), "super", null, 0, 1, CreateOperationRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(createInterfaceEClass, CreateInterface.class, "CreateInterface", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //UmlClassDiagramPackageImpl
