/**
 */
package artefact.umlClassDiagram.impl;

import artefact.generic.impl.ArtefactImpl;

import artefact.umlClassDiagram.ClassDiagram;
import artefact.umlClassDiagram.UmlClassDiagramPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class Diagram</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ClassDiagramImpl extends ArtefactImpl implements ClassDiagram {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassDiagramImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UmlClassDiagramPackage.Literals.CLASS_DIAGRAM;
	}

} //ClassDiagramImpl
