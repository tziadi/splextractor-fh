/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package artefact.umlClassDiagram.impl;

import artefact.umlClassDiagram.CreateInterface;
import artefact.umlClassDiagram.UmlClassDiagramPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Create Interface</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class CreateInterfaceImpl extends CreateClassImpl implements CreateInterface {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CreateInterfaceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UmlClassDiagramPackage.Literals.CREATE_INTERFACE;
	}

} //CreateInterfaceImpl
