/**
 */
package artefact.umlClassDiagram.impl;

import artefact.generic.impl.ConstructionPrimitiveImpl;

import artefact.umlClassDiagram.CreateGeneralization;
import artefact.umlClassDiagram.UmlClassDiagramPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Create Generalization</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link artefact.umlClassDiagram.impl.CreateGeneralizationImpl#getSub <em>Sub</em>}</li>
 *   <li>{@link artefact.umlClassDiagram.impl.CreateGeneralizationImpl#getSuper <em>Super</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CreateGeneralizationImpl extends ConstructionPrimitiveImpl implements CreateGeneralization {
	/**
	 * The default value of the '{@link #getSub() <em>Sub</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSub()
	 * @generated
	 * @ordered
	 */
	protected static final String SUB_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSub() <em>Sub</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSub()
	 * @generated
	 * @ordered
	 */
	protected String sub = SUB_EDEFAULT;

	/**
	 * The default value of the '{@link #getSuper() <em>Super</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuper()
	 * @generated
	 * @ordered
	 */
	protected static final String SUPER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSuper() <em>Super</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuper()
	 * @generated
	 * @ordered
	 */
	protected String super_ = SUPER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CreateGeneralizationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UmlClassDiagramPackage.Literals.CREATE_GENERALIZATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSub() {
		return sub;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSub(String newSub) {
		String oldSub = sub;
		sub = newSub;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UmlClassDiagramPackage.CREATE_GENERALIZATION__SUB, oldSub, sub));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSuper() {
		return super_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuper(String newSuper) {
		String oldSuper = super_;
		super_ = newSuper;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UmlClassDiagramPackage.CREATE_GENERALIZATION__SUPER, oldSuper, super_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UmlClassDiagramPackage.CREATE_GENERALIZATION__SUB:
				return getSub();
			case UmlClassDiagramPackage.CREATE_GENERALIZATION__SUPER:
				return getSuper();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UmlClassDiagramPackage.CREATE_GENERALIZATION__SUB:
				setSub((String)newValue);
				return;
			case UmlClassDiagramPackage.CREATE_GENERALIZATION__SUPER:
				setSuper((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UmlClassDiagramPackage.CREATE_GENERALIZATION__SUB:
				setSub(SUB_EDEFAULT);
				return;
			case UmlClassDiagramPackage.CREATE_GENERALIZATION__SUPER:
				setSuper(SUPER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UmlClassDiagramPackage.CREATE_GENERALIZATION__SUB:
				return SUB_EDEFAULT == null ? sub != null : !SUB_EDEFAULT.equals(sub);
			case UmlClassDiagramPackage.CREATE_GENERALIZATION__SUPER:
				return SUPER_EDEFAULT == null ? super_ != null : !SUPER_EDEFAULT.equals(super_);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (sub: ");
		result.append(sub);
		result.append(", super: ");
		result.append(super_);
		result.append(')');
		return result.toString();
	}

} //CreateGeneralizationImpl
