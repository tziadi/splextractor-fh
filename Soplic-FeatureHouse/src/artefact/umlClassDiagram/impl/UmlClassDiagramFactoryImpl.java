/**
 */
package artefact.umlClassDiagram.impl;

import artefact.umlClassDiagram.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UmlClassDiagramFactoryImpl extends EFactoryImpl implements UmlClassDiagramFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static UmlClassDiagramFactory init() {
		try {
			UmlClassDiagramFactory theUmlClassDiagramFactory = (UmlClassDiagramFactory)EPackage.Registry.INSTANCE.getEFactory("http://cd/1.0"); 
			if (theUmlClassDiagramFactory != null) {
				return theUmlClassDiagramFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new UmlClassDiagramFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UmlClassDiagramFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case UmlClassDiagramPackage.CLASS_DIAGRAM: return createClassDiagram();
			case UmlClassDiagramPackage.CREATE_CLASS: return createCreateClass();
			case UmlClassDiagramPackage.CREATE_ASSOCIATION: return createCreateAssociation();
			case UmlClassDiagramPackage.CREATE_ATTRIBUTE: return createCreateAttribute();
			case UmlClassDiagramPackage.CREATE_OPERATION: return createCreateOperation();
			case UmlClassDiagramPackage.CREATE_GENERALIZATION: return createCreateGeneralization();
			case UmlClassDiagramPackage.ASSOCIATION_INVARIANT: return createAssociationInvariant();
			case UmlClassDiagramPackage.CREATE_PACKAGE: return createCreatePackage();
			case UmlClassDiagramPackage.CREATE_OPERATION_REFINEMENT: return createCreateOperationRefinement();
			case UmlClassDiagramPackage.CREATE_INTERFACE: return createCreateInterface();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassDiagram createClassDiagram() {
		ClassDiagramImpl classDiagram = new ClassDiagramImpl();
		return classDiagram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CreateClass createCreateClass() {
		CreateClassImpl createClass = new CreateClassImpl();
		return createClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CreateAssociation createCreateAssociation() {
		CreateAssociationImpl createAssociation = new CreateAssociationImpl();
		return createAssociation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CreateAttribute createCreateAttribute() {
		CreateAttributeImpl createAttribute = new CreateAttributeImpl();
		return createAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CreateOperation createCreateOperation() {
		CreateOperationImpl createOperation = new CreateOperationImpl();
		return createOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CreateGeneralization createCreateGeneralization() {
		CreateGeneralizationImpl createGeneralization = new CreateGeneralizationImpl();
		return createGeneralization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssociationInvariant createAssociationInvariant() {
		AssociationInvariantImpl associationInvariant = new AssociationInvariantImpl();
		return associationInvariant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CreatePackage createCreatePackage() {
		CreatePackageImpl createPackage = new CreatePackageImpl();
		return createPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CreateOperationRefinement createCreateOperationRefinement() {
		CreateOperationRefinementImpl createOperationRefinement = new CreateOperationRefinementImpl();
		return createOperationRefinement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CreateInterface createCreateInterface() {
		CreateInterfaceImpl createInterface = new CreateInterfaceImpl();
		return createInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UmlClassDiagramPackage getUmlClassDiagramPackage() {
		return (UmlClassDiagramPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static UmlClassDiagramPackage getPackage() {
		return UmlClassDiagramPackage.eINSTANCE;
	}

} //UmlClassDiagramFactoryImpl
