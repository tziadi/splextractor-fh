/**
 */
package artefact.umlClassDiagram.impl;

import artefact.generic.impl.ConstraintImpl;

import artefact.umlClassDiagram.AssociationInvariant;
import artefact.umlClassDiagram.UmlClassDiagramPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Association Invariant</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class AssociationInvariantImpl extends ConstraintImpl implements AssociationInvariant {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AssociationInvariantImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UmlClassDiagramPackage.Literals.ASSOCIATION_INVARIANT;
	}

} //AssociationInvariantImpl
