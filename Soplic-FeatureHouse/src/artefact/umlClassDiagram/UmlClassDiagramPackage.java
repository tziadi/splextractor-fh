/**
 */
package artefact.umlClassDiagram;

import artefact.generic.GenericPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see artefact.umlClassDiagram.UmlClassDiagramFactory
 * @model kind="package"
 * @generated
 */
public interface UmlClassDiagramPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "umlClassDiagram";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://cd/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "umlCD";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UmlClassDiagramPackage eINSTANCE = artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl.init();

	/**
	 * The meta object id for the '{@link artefact.umlClassDiagram.impl.ClassDiagramImpl <em>Class Diagram</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see artefact.umlClassDiagram.impl.ClassDiagramImpl
	 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getClassDiagram()
	 * @generated
	 */
	int CLASS_DIAGRAM = 0;

	/**
	 * The feature id for the '<em><b>First</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_DIAGRAM__FIRST = GenericPackage.ARTEFACT__FIRST;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_DIAGRAM__CONSTRAINTS = GenericPackage.ARTEFACT__CONSTRAINTS;

	/**
	 * The number of structural features of the '<em>Class Diagram</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_DIAGRAM_FEATURE_COUNT = GenericPackage.ARTEFACT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link artefact.umlClassDiagram.impl.CreateClassImpl <em>Create Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see artefact.umlClassDiagram.impl.CreateClassImpl
	 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getCreateClass()
	 * @generated
	 */
	int CREATE_CLASS = 1;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_CLASS__NEXT = GenericPackage.CONSTRUCTION_PRIMITIVE__NEXT;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_CLASS__ID = GenericPackage.CONSTRUCTION_PRIMITIVE__ID;

	/**
	 * The feature id for the '<em><b>Lien</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_CLASS__LIEN = GenericPackage.CONSTRUCTION_PRIMITIVE__LIEN;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_CLASS__NAME = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Owener</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_CLASS__OWENER = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Create Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_CLASS_FEATURE_COUNT = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link artefact.umlClassDiagram.impl.CreateAssociationImpl <em>Create Association</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see artefact.umlClassDiagram.impl.CreateAssociationImpl
	 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getCreateAssociation()
	 * @generated
	 */
	int CREATE_ASSOCIATION = 2;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ASSOCIATION__NEXT = GenericPackage.CONSTRUCTION_PRIMITIVE__NEXT;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ASSOCIATION__ID = GenericPackage.CONSTRUCTION_PRIMITIVE__ID;

	/**
	 * The feature id for the '<em><b>Lien</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ASSOCIATION__LIEN = GenericPackage.CONSTRUCTION_PRIMITIVE__LIEN;

	/**
	 * The feature id for the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ASSOCIATION__SOURCE = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ASSOCIATION__TARGET = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Create Association</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ASSOCIATION_FEATURE_COUNT = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link artefact.umlClassDiagram.impl.CreateAttributeImpl <em>Create Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see artefact.umlClassDiagram.impl.CreateAttributeImpl
	 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getCreateAttribute()
	 * @generated
	 */
	int CREATE_ATTRIBUTE = 3;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ATTRIBUTE__NEXT = GenericPackage.CONSTRUCTION_PRIMITIVE__NEXT;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ATTRIBUTE__ID = GenericPackage.CONSTRUCTION_PRIMITIVE__ID;

	/**
	 * The feature id for the '<em><b>Lien</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ATTRIBUTE__LIEN = GenericPackage.CONSTRUCTION_PRIMITIVE__LIEN;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ATTRIBUTE__NAME = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ATTRIBUTE__OWNER = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ATTRIBUTE__TYPE = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Create Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_ATTRIBUTE_FEATURE_COUNT = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link artefact.umlClassDiagram.impl.CreateOperationImpl <em>Create Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see artefact.umlClassDiagram.impl.CreateOperationImpl
	 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getCreateOperation()
	 * @generated
	 */
	int CREATE_OPERATION = 4;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_OPERATION__NEXT = GenericPackage.CONSTRUCTION_PRIMITIVE__NEXT;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_OPERATION__ID = GenericPackage.CONSTRUCTION_PRIMITIVE__ID;

	/**
	 * The feature id for the '<em><b>Lien</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_OPERATION__LIEN = GenericPackage.CONSTRUCTION_PRIMITIVE__LIEN;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_OPERATION__NAME = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Owener</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_OPERATION__OWENER = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Create Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_OPERATION_FEATURE_COUNT = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link artefact.umlClassDiagram.impl.CreateGeneralizationImpl <em>Create Generalization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see artefact.umlClassDiagram.impl.CreateGeneralizationImpl
	 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getCreateGeneralization()
	 * @generated
	 */
	int CREATE_GENERALIZATION = 5;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_GENERALIZATION__NEXT = GenericPackage.CONSTRUCTION_PRIMITIVE__NEXT;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_GENERALIZATION__ID = GenericPackage.CONSTRUCTION_PRIMITIVE__ID;

	/**
	 * The feature id for the '<em><b>Lien</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_GENERALIZATION__LIEN = GenericPackage.CONSTRUCTION_PRIMITIVE__LIEN;

	/**
	 * The feature id for the '<em><b>Sub</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_GENERALIZATION__SUB = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Super</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_GENERALIZATION__SUPER = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Create Generalization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_GENERALIZATION_FEATURE_COUNT = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link artefact.umlClassDiagram.impl.AssociationInvariantImpl <em>Association Invariant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see artefact.umlClassDiagram.impl.AssociationInvariantImpl
	 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getAssociationInvariant()
	 * @generated
	 */
	int ASSOCIATION_INVARIANT = 6;

	/**
	 * The number of structural features of the '<em>Association Invariant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSOCIATION_INVARIANT_FEATURE_COUNT = GenericPackage.CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link artefact.umlClassDiagram.impl.CreatePackageImpl <em>Create Package</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see artefact.umlClassDiagram.impl.CreatePackageImpl
	 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getCreatePackage()
	 * @generated
	 */
	int CREATE_PACKAGE = 7;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_PACKAGE__NEXT = GenericPackage.CONSTRUCTION_PRIMITIVE__NEXT;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_PACKAGE__ID = GenericPackage.CONSTRUCTION_PRIMITIVE__ID;

	/**
	 * The feature id for the '<em><b>Lien</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_PACKAGE__LIEN = GenericPackage.CONSTRUCTION_PRIMITIVE__LIEN;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_PACKAGE__NAME = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Create Package</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_PACKAGE_FEATURE_COUNT = GenericPackage.CONSTRUCTION_PRIMITIVE_FEATURE_COUNT + 1;


	/**
	 * The meta object id for the '{@link artefact.umlClassDiagram.impl.CreateOperationRefinementImpl <em>Create Operation Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see artefact.umlClassDiagram.impl.CreateOperationRefinementImpl
	 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getCreateOperationRefinement()
	 * @generated
	 */
	int CREATE_OPERATION_REFINEMENT = 8;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_OPERATION_REFINEMENT__NEXT = CREATE_OPERATION__NEXT;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_OPERATION_REFINEMENT__ID = CREATE_OPERATION__ID;

	/**
	 * The feature id for the '<em><b>Lien</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_OPERATION_REFINEMENT__LIEN = CREATE_OPERATION__LIEN;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_OPERATION_REFINEMENT__NAME = CREATE_OPERATION__NAME;

	/**
	 * The feature id for the '<em><b>Owener</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_OPERATION_REFINEMENT__OWENER = CREATE_OPERATION__OWENER;

	/**
	 * The feature id for the '<em><b>Super</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_OPERATION_REFINEMENT__SUPER = CREATE_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Create Operation Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_OPERATION_REFINEMENT_FEATURE_COUNT = CREATE_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link artefact.umlClassDiagram.impl.CreateInterfaceImpl <em>Create Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see artefact.umlClassDiagram.impl.CreateInterfaceImpl
	 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getCreateInterface()
	 * @generated
	 */
	int CREATE_INTERFACE = 9;

	/**
	 * The feature id for the '<em><b>Next</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_INTERFACE__NEXT = CREATE_CLASS__NEXT;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_INTERFACE__ID = CREATE_CLASS__ID;

	/**
	 * The feature id for the '<em><b>Lien</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_INTERFACE__LIEN = CREATE_CLASS__LIEN;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_INTERFACE__NAME = CREATE_CLASS__NAME;

	/**
	 * The feature id for the '<em><b>Owener</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_INTERFACE__OWENER = CREATE_CLASS__OWENER;

	/**
	 * The number of structural features of the '<em>Create Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREATE_INTERFACE_FEATURE_COUNT = CREATE_CLASS_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link artefact.umlClassDiagram.ClassDiagram <em>Class Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Diagram</em>'.
	 * @see artefact.umlClassDiagram.ClassDiagram
	 * @generated
	 */
	EClass getClassDiagram();

	/**
	 * Returns the meta object for class '{@link artefact.umlClassDiagram.CreateClass <em>Create Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Create Class</em>'.
	 * @see artefact.umlClassDiagram.CreateClass
	 * @generated
	 */
	EClass getCreateClass();

	/**
	 * Returns the meta object for the attribute '{@link artefact.umlClassDiagram.CreateClass#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see artefact.umlClassDiagram.CreateClass#getName()
	 * @see #getCreateClass()
	 * @generated
	 */
	EAttribute getCreateClass_Name();

	/**
	 * Returns the meta object for the attribute '{@link artefact.umlClassDiagram.CreateClass#getOwener <em>Owener</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Owener</em>'.
	 * @see artefact.umlClassDiagram.CreateClass#getOwener()
	 * @see #getCreateClass()
	 * @generated
	 */
	EAttribute getCreateClass_Owener();

	/**
	 * Returns the meta object for class '{@link artefact.umlClassDiagram.CreateAssociation <em>Create Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Create Association</em>'.
	 * @see artefact.umlClassDiagram.CreateAssociation
	 * @generated
	 */
	EClass getCreateAssociation();

	/**
	 * Returns the meta object for the attribute '{@link artefact.umlClassDiagram.CreateAssociation#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source</em>'.
	 * @see artefact.umlClassDiagram.CreateAssociation#getSource()
	 * @see #getCreateAssociation()
	 * @generated
	 */
	EAttribute getCreateAssociation_Source();

	/**
	 * Returns the meta object for the attribute '{@link artefact.umlClassDiagram.CreateAssociation#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target</em>'.
	 * @see artefact.umlClassDiagram.CreateAssociation#getTarget()
	 * @see #getCreateAssociation()
	 * @generated
	 */
	EAttribute getCreateAssociation_Target();

	/**
	 * Returns the meta object for class '{@link artefact.umlClassDiagram.CreateAttribute <em>Create Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Create Attribute</em>'.
	 * @see artefact.umlClassDiagram.CreateAttribute
	 * @generated
	 */
	EClass getCreateAttribute();

	/**
	 * Returns the meta object for the attribute '{@link artefact.umlClassDiagram.CreateAttribute#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see artefact.umlClassDiagram.CreateAttribute#getName()
	 * @see #getCreateAttribute()
	 * @generated
	 */
	EAttribute getCreateAttribute_Name();

	/**
	 * Returns the meta object for the attribute '{@link artefact.umlClassDiagram.CreateAttribute#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Owner</em>'.
	 * @see artefact.umlClassDiagram.CreateAttribute#getOwner()
	 * @see #getCreateAttribute()
	 * @generated
	 */
	EAttribute getCreateAttribute_Owner();

	/**
	 * Returns the meta object for the attribute '{@link artefact.umlClassDiagram.CreateAttribute#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see artefact.umlClassDiagram.CreateAttribute#getType()
	 * @see #getCreateAttribute()
	 * @generated
	 */
	EAttribute getCreateAttribute_Type();

	/**
	 * Returns the meta object for class '{@link artefact.umlClassDiagram.CreateOperation <em>Create Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Create Operation</em>'.
	 * @see artefact.umlClassDiagram.CreateOperation
	 * @generated
	 */
	EClass getCreateOperation();

	/**
	 * Returns the meta object for the attribute '{@link artefact.umlClassDiagram.CreateOperation#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see artefact.umlClassDiagram.CreateOperation#getName()
	 * @see #getCreateOperation()
	 * @generated
	 */
	EAttribute getCreateOperation_Name();

	/**
	 * Returns the meta object for the attribute '{@link artefact.umlClassDiagram.CreateOperation#getOwener <em>Owener</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Owener</em>'.
	 * @see artefact.umlClassDiagram.CreateOperation#getOwener()
	 * @see #getCreateOperation()
	 * @generated
	 */
	EAttribute getCreateOperation_Owener();

	/**
	 * Returns the meta object for class '{@link artefact.umlClassDiagram.CreateGeneralization <em>Create Generalization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Create Generalization</em>'.
	 * @see artefact.umlClassDiagram.CreateGeneralization
	 * @generated
	 */
	EClass getCreateGeneralization();

	/**
	 * Returns the meta object for the attribute '{@link artefact.umlClassDiagram.CreateGeneralization#getSub <em>Sub</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sub</em>'.
	 * @see artefact.umlClassDiagram.CreateGeneralization#getSub()
	 * @see #getCreateGeneralization()
	 * @generated
	 */
	EAttribute getCreateGeneralization_Sub();

	/**
	 * Returns the meta object for the attribute '{@link artefact.umlClassDiagram.CreateGeneralization#getSuper <em>Super</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Super</em>'.
	 * @see artefact.umlClassDiagram.CreateGeneralization#getSuper()
	 * @see #getCreateGeneralization()
	 * @generated
	 */
	EAttribute getCreateGeneralization_Super();

	/**
	 * Returns the meta object for class '{@link artefact.umlClassDiagram.AssociationInvariant <em>Association Invariant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Association Invariant</em>'.
	 * @see artefact.umlClassDiagram.AssociationInvariant
	 * @generated
	 */
	EClass getAssociationInvariant();

	/**
	 * Returns the meta object for class '{@link artefact.umlClassDiagram.CreatePackage <em>Create Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Create Package</em>'.
	 * @see artefact.umlClassDiagram.CreatePackage
	 * @generated
	 */
	EClass getCreatePackage();

	/**
	 * Returns the meta object for the attribute '{@link artefact.umlClassDiagram.CreatePackage#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see artefact.umlClassDiagram.CreatePackage#getName()
	 * @see #getCreatePackage()
	 * @generated
	 */
	EAttribute getCreatePackage_Name();

	/**
	 * Returns the meta object for class '{@link artefact.umlClassDiagram.CreateOperationRefinement <em>Create Operation Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Create Operation Refinement</em>'.
	 * @see artefact.umlClassDiagram.CreateOperationRefinement
	 * @generated
	 */
	EClass getCreateOperationRefinement();

	/**
	 * Returns the meta object for the attribute '{@link artefact.umlClassDiagram.CreateOperationRefinement#getSuper <em>Super</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Super</em>'.
	 * @see artefact.umlClassDiagram.CreateOperationRefinement#getSuper()
	 * @see #getCreateOperationRefinement()
	 * @generated
	 */
	EAttribute getCreateOperationRefinement_Super();

	/**
	 * Returns the meta object for class '{@link artefact.umlClassDiagram.CreateInterface <em>Create Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Create Interface</em>'.
	 * @see artefact.umlClassDiagram.CreateInterface
	 * @generated
	 */
	EClass getCreateInterface();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	UmlClassDiagramFactory getUmlClassDiagramFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link artefact.umlClassDiagram.impl.ClassDiagramImpl <em>Class Diagram</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see artefact.umlClassDiagram.impl.ClassDiagramImpl
		 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getClassDiagram()
		 * @generated
		 */
		EClass CLASS_DIAGRAM = eINSTANCE.getClassDiagram();

		/**
		 * The meta object literal for the '{@link artefact.umlClassDiagram.impl.CreateClassImpl <em>Create Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see artefact.umlClassDiagram.impl.CreateClassImpl
		 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getCreateClass()
		 * @generated
		 */
		EClass CREATE_CLASS = eINSTANCE.getCreateClass();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATE_CLASS__NAME = eINSTANCE.getCreateClass_Name();

		/**
		 * The meta object literal for the '<em><b>Owener</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATE_CLASS__OWENER = eINSTANCE.getCreateClass_Owener();

		/**
		 * The meta object literal for the '{@link artefact.umlClassDiagram.impl.CreateAssociationImpl <em>Create Association</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see artefact.umlClassDiagram.impl.CreateAssociationImpl
		 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getCreateAssociation()
		 * @generated
		 */
		EClass CREATE_ASSOCIATION = eINSTANCE.getCreateAssociation();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATE_ASSOCIATION__SOURCE = eINSTANCE.getCreateAssociation_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATE_ASSOCIATION__TARGET = eINSTANCE.getCreateAssociation_Target();

		/**
		 * The meta object literal for the '{@link artefact.umlClassDiagram.impl.CreateAttributeImpl <em>Create Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see artefact.umlClassDiagram.impl.CreateAttributeImpl
		 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getCreateAttribute()
		 * @generated
		 */
		EClass CREATE_ATTRIBUTE = eINSTANCE.getCreateAttribute();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATE_ATTRIBUTE__NAME = eINSTANCE.getCreateAttribute_Name();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATE_ATTRIBUTE__OWNER = eINSTANCE.getCreateAttribute_Owner();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATE_ATTRIBUTE__TYPE = eINSTANCE.getCreateAttribute_Type();

		/**
		 * The meta object literal for the '{@link artefact.umlClassDiagram.impl.CreateOperationImpl <em>Create Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see artefact.umlClassDiagram.impl.CreateOperationImpl
		 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getCreateOperation()
		 * @generated
		 */
		EClass CREATE_OPERATION = eINSTANCE.getCreateOperation();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATE_OPERATION__NAME = eINSTANCE.getCreateOperation_Name();

		/**
		 * The meta object literal for the '<em><b>Owener</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATE_OPERATION__OWENER = eINSTANCE.getCreateOperation_Owener();

		/**
		 * The meta object literal for the '{@link artefact.umlClassDiagram.impl.CreateGeneralizationImpl <em>Create Generalization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see artefact.umlClassDiagram.impl.CreateGeneralizationImpl
		 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getCreateGeneralization()
		 * @generated
		 */
		EClass CREATE_GENERALIZATION = eINSTANCE.getCreateGeneralization();

		/**
		 * The meta object literal for the '<em><b>Sub</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATE_GENERALIZATION__SUB = eINSTANCE.getCreateGeneralization_Sub();

		/**
		 * The meta object literal for the '<em><b>Super</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATE_GENERALIZATION__SUPER = eINSTANCE.getCreateGeneralization_Super();

		/**
		 * The meta object literal for the '{@link artefact.umlClassDiagram.impl.AssociationInvariantImpl <em>Association Invariant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see artefact.umlClassDiagram.impl.AssociationInvariantImpl
		 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getAssociationInvariant()
		 * @generated
		 */
		EClass ASSOCIATION_INVARIANT = eINSTANCE.getAssociationInvariant();

		/**
		 * The meta object literal for the '{@link artefact.umlClassDiagram.impl.CreatePackageImpl <em>Create Package</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see artefact.umlClassDiagram.impl.CreatePackageImpl
		 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getCreatePackage()
		 * @generated
		 */
		EClass CREATE_PACKAGE = eINSTANCE.getCreatePackage();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATE_PACKAGE__NAME = eINSTANCE.getCreatePackage_Name();

		/**
		 * The meta object literal for the '{@link artefact.umlClassDiagram.impl.CreateOperationRefinementImpl <em>Create Operation Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see artefact.umlClassDiagram.impl.CreateOperationRefinementImpl
		 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getCreateOperationRefinement()
		 * @generated
		 */
		EClass CREATE_OPERATION_REFINEMENT = eINSTANCE.getCreateOperationRefinement();

		/**
		 * The meta object literal for the '<em><b>Super</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CREATE_OPERATION_REFINEMENT__SUPER = eINSTANCE.getCreateOperationRefinement_Super();

		/**
		 * The meta object literal for the '{@link artefact.umlClassDiagram.impl.CreateInterfaceImpl <em>Create Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see artefact.umlClassDiagram.impl.CreateInterfaceImpl
		 * @see artefact.umlClassDiagram.impl.UmlClassDiagramPackageImpl#getCreateInterface()
		 * @generated
		 */
		EClass CREATE_INTERFACE = eINSTANCE.getCreateInterface();

	}

} //UmlClassDiagramPackage
