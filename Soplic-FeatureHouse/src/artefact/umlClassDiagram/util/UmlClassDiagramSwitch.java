/**
 */
package artefact.umlClassDiagram.util;

import artefact.generic.Artefact;
import artefact.generic.Constraint;
import artefact.generic.ConstructionPrimitive;

import artefact.umlClassDiagram.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see artefact.umlClassDiagram.UmlClassDiagramPackage
 * @generated
 */
public class UmlClassDiagramSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static UmlClassDiagramPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UmlClassDiagramSwitch() {
		if (modelPackage == null) {
			modelPackage = UmlClassDiagramPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case UmlClassDiagramPackage.CLASS_DIAGRAM: {
				ClassDiagram classDiagram = (ClassDiagram)theEObject;
				T result = caseClassDiagram(classDiagram);
				if (result == null) result = caseArtefact(classDiagram);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UmlClassDiagramPackage.CREATE_CLASS: {
				CreateClass createClass = (CreateClass)theEObject;
				T result = caseCreateClass(createClass);
				if (result == null) result = caseConstructionPrimitive(createClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UmlClassDiagramPackage.CREATE_ASSOCIATION: {
				CreateAssociation createAssociation = (CreateAssociation)theEObject;
				T result = caseCreateAssociation(createAssociation);
				if (result == null) result = caseConstructionPrimitive(createAssociation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UmlClassDiagramPackage.CREATE_ATTRIBUTE: {
				CreateAttribute createAttribute = (CreateAttribute)theEObject;
				T result = caseCreateAttribute(createAttribute);
				if (result == null) result = caseConstructionPrimitive(createAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UmlClassDiagramPackage.CREATE_OPERATION: {
				CreateOperation createOperation = (CreateOperation)theEObject;
				T result = caseCreateOperation(createOperation);
				if (result == null) result = caseConstructionPrimitive(createOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UmlClassDiagramPackage.CREATE_GENERALIZATION: {
				CreateGeneralization createGeneralization = (CreateGeneralization)theEObject;
				T result = caseCreateGeneralization(createGeneralization);
				if (result == null) result = caseConstructionPrimitive(createGeneralization);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UmlClassDiagramPackage.ASSOCIATION_INVARIANT: {
				AssociationInvariant associationInvariant = (AssociationInvariant)theEObject;
				T result = caseAssociationInvariant(associationInvariant);
				if (result == null) result = caseConstraint(associationInvariant);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UmlClassDiagramPackage.CREATE_PACKAGE: {
				CreatePackage createPackage = (CreatePackage)theEObject;
				T result = caseCreatePackage(createPackage);
				if (result == null) result = caseConstructionPrimitive(createPackage);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UmlClassDiagramPackage.CREATE_OPERATION_REFINEMENT: {
				CreateOperationRefinement createOperationRefinement = (CreateOperationRefinement)theEObject;
				T result = caseCreateOperationRefinement(createOperationRefinement);
				if (result == null) result = caseCreateOperation(createOperationRefinement);
				if (result == null) result = caseConstructionPrimitive(createOperationRefinement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UmlClassDiagramPackage.CREATE_INTERFACE: {
				CreateInterface createInterface = (CreateInterface)theEObject;
				T result = caseCreateInterface(createInterface);
				if (result == null) result = caseCreateClass(createInterface);
				if (result == null) result = caseConstructionPrimitive(createInterface);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Class Diagram</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Class Diagram</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClassDiagram(ClassDiagram object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Create Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Create Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCreateClass(CreateClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Create Association</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Create Association</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCreateAssociation(CreateAssociation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Create Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Create Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCreateAttribute(CreateAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Create Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Create Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCreateOperation(CreateOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Create Generalization</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Create Generalization</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCreateGeneralization(CreateGeneralization object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Association Invariant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Association Invariant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAssociationInvariant(AssociationInvariant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Create Package</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Create Package</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCreatePackage(CreatePackage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Create Operation Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Create Operation Refinement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCreateOperationRefinement(CreateOperationRefinement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Create Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Create Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCreateInterface(CreateInterface object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Artefact</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Artefact</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArtefact(Artefact object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Construction Primitive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Construction Primitive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstructionPrimitive(ConstructionPrimitive object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstraint(Constraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //UmlClassDiagramSwitch
