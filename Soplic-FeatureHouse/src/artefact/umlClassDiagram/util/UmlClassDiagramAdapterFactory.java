/**
 */
package artefact.umlClassDiagram.util;

import artefact.generic.Artefact;
import artefact.generic.Constraint;
import artefact.generic.ConstructionPrimitive;

import artefact.umlClassDiagram.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see artefact.umlClassDiagram.UmlClassDiagramPackage
 * @generated
 */
public class UmlClassDiagramAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static UmlClassDiagramPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UmlClassDiagramAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = UmlClassDiagramPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UmlClassDiagramSwitch<Adapter> modelSwitch =
		new UmlClassDiagramSwitch<Adapter>() {
			@Override
			public Adapter caseClassDiagram(ClassDiagram object) {
				return createClassDiagramAdapter();
			}
			@Override
			public Adapter caseCreateClass(CreateClass object) {
				return createCreateClassAdapter();
			}
			@Override
			public Adapter caseCreateAssociation(CreateAssociation object) {
				return createCreateAssociationAdapter();
			}
			@Override
			public Adapter caseCreateAttribute(CreateAttribute object) {
				return createCreateAttributeAdapter();
			}
			@Override
			public Adapter caseCreateOperation(CreateOperation object) {
				return createCreateOperationAdapter();
			}
			@Override
			public Adapter caseCreateGeneralization(CreateGeneralization object) {
				return createCreateGeneralizationAdapter();
			}
			@Override
			public Adapter caseAssociationInvariant(AssociationInvariant object) {
				return createAssociationInvariantAdapter();
			}
			@Override
			public Adapter caseCreatePackage(CreatePackage object) {
				return createCreatePackageAdapter();
			}
			@Override
			public Adapter caseCreateOperationRefinement(CreateOperationRefinement object) {
				return createCreateOperationRefinementAdapter();
			}
			@Override
			public Adapter caseCreateInterface(CreateInterface object) {
				return createCreateInterfaceAdapter();
			}
			@Override
			public Adapter caseArtefact(Artefact object) {
				return createArtefactAdapter();
			}
			@Override
			public Adapter caseConstructionPrimitive(ConstructionPrimitive object) {
				return createConstructionPrimitiveAdapter();
			}
			@Override
			public Adapter caseConstraint(Constraint object) {
				return createConstraintAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link artefact.umlClassDiagram.ClassDiagram <em>Class Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see artefact.umlClassDiagram.ClassDiagram
	 * @generated
	 */
	public Adapter createClassDiagramAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link artefact.umlClassDiagram.CreateClass <em>Create Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see artefact.umlClassDiagram.CreateClass
	 * @generated
	 */
	public Adapter createCreateClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link artefact.umlClassDiagram.CreateAssociation <em>Create Association</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see artefact.umlClassDiagram.CreateAssociation
	 * @generated
	 */
	public Adapter createCreateAssociationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link artefact.umlClassDiagram.CreateAttribute <em>Create Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see artefact.umlClassDiagram.CreateAttribute
	 * @generated
	 */
	public Adapter createCreateAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link artefact.umlClassDiagram.CreateOperation <em>Create Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see artefact.umlClassDiagram.CreateOperation
	 * @generated
	 */
	public Adapter createCreateOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link artefact.umlClassDiagram.CreateGeneralization <em>Create Generalization</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see artefact.umlClassDiagram.CreateGeneralization
	 * @generated
	 */
	public Adapter createCreateGeneralizationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link artefact.umlClassDiagram.AssociationInvariant <em>Association Invariant</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see artefact.umlClassDiagram.AssociationInvariant
	 * @generated
	 */
	public Adapter createAssociationInvariantAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link artefact.umlClassDiagram.CreatePackage <em>Create Package</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see artefact.umlClassDiagram.CreatePackage
	 * @generated
	 */
	public Adapter createCreatePackageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link artefact.umlClassDiagram.CreateOperationRefinement <em>Create Operation Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see artefact.umlClassDiagram.CreateOperationRefinement
	 * @generated
	 */
	public Adapter createCreateOperationRefinementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link artefact.umlClassDiagram.CreateInterface <em>Create Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see artefact.umlClassDiagram.CreateInterface
	 * @generated
	 */
	public Adapter createCreateInterfaceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link artefact.generic.Artefact <em>Artefact</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see artefact.generic.Artefact
	 * @generated
	 */
	public Adapter createArtefactAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link artefact.generic.ConstructionPrimitive <em>Construction Primitive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see artefact.generic.ConstructionPrimitive
	 * @generated
	 */
	public Adapter createConstructionPrimitiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link artefact.generic.Constraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see artefact.generic.Constraint
	 * @generated
	 */
	public Adapter createConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //UmlClassDiagramAdapterFactory
