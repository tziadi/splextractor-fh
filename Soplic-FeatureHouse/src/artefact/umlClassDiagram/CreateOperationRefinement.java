/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package artefact.umlClassDiagram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Create Operation Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link artefact.umlClassDiagram.CreateOperationRefinement#getSuper <em>Super</em>}</li>
 * </ul>
 * </p>
 *
 * @see artefact.umlClassDiagram.UmlClassDiagramPackage#getCreateOperationRefinement()
 * @model
 * @generated
 */
public interface CreateOperationRefinement extends CreateOperation {

	/**
	 * Returns the value of the '<em><b>Super</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Super</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super</em>' attribute.
	 * @see #setSuper(String)
	 * @see artefact.umlClassDiagram.UmlClassDiagramPackage#getCreateOperationRefinement_Super()
	 * @model
	 * @generated
	 */
	String getSuper();

	/**
	 * Sets the value of the '{@link artefact.umlClassDiagram.CreateOperationRefinement#getSuper <em>Super</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Super</em>' attribute.
	 * @see #getSuper()
	 * @generated
	 */
	void setSuper(String value);
} // CreateOperationRefinement
