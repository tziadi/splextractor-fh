/**
 */
package artefact.umlClassDiagram;

import artefact.generic.ConstructionPrimitive;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Create Association</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link artefact.umlClassDiagram.CreateAssociation#getSource <em>Source</em>}</li>
 *   <li>{@link artefact.umlClassDiagram.CreateAssociation#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see artefact.umlClassDiagram.UmlClassDiagramPackage#getCreateAssociation()
 * @model
 * @generated
 */
public interface CreateAssociation extends ConstructionPrimitive {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' attribute.
	 * @see #setSource(String)
	 * @see artefact.umlClassDiagram.UmlClassDiagramPackage#getCreateAssociation_Source()
	 * @model
	 * @generated
	 */
	String getSource();

	/**
	 * Sets the value of the '{@link artefact.umlClassDiagram.CreateAssociation#getSource <em>Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' attribute.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(String value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' attribute.
	 * @see #setTarget(String)
	 * @see artefact.umlClassDiagram.UmlClassDiagramPackage#getCreateAssociation_Target()
	 * @model
	 * @generated
	 */
	String getTarget();

	/**
	 * Sets the value of the '{@link artefact.umlClassDiagram.CreateAssociation#getTarget <em>Target</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' attribute.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(String value);

} // CreateAssociation
