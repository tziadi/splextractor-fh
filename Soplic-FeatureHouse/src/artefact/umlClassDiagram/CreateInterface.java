/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package artefact.umlClassDiagram;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Create Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see artefact.umlClassDiagram.UmlClassDiagramPackage#getCreateInterface()
 * @model
 * @generated
 */
public interface CreateInterface extends CreateClass {
} // CreateInterface
