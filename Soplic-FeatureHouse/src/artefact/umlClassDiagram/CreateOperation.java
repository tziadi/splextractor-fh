/**
 */
package artefact.umlClassDiagram;

import artefact.generic.ConstructionPrimitive;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Create Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link artefact.umlClassDiagram.CreateOperation#getName <em>Name</em>}</li>
 *   <li>{@link artefact.umlClassDiagram.CreateOperation#getOwener <em>Owener</em>}</li>
 * </ul>
 * </p>
 *
 * @see artefact.umlClassDiagram.UmlClassDiagramPackage#getCreateOperation()
 * @model
 * @generated
 */
public interface CreateOperation extends ConstructionPrimitive {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see artefact.umlClassDiagram.UmlClassDiagramPackage#getCreateOperation_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link artefact.umlClassDiagram.CreateOperation#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Owener</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owener</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owener</em>' attribute.
	 * @see #setOwener(String)
	 * @see artefact.umlClassDiagram.UmlClassDiagramPackage#getCreateOperation_Owener()
	 * @model
	 * @generated
	 */
	String getOwener();

	/**
	 * Sets the value of the '{@link artefact.umlClassDiagram.CreateOperation#getOwener <em>Owener</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owener</em>' attribute.
	 * @see #getOwener()
	 * @generated
	 */
	void setOwener(String value);

} // CreateOperation
