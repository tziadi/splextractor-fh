/**
 */
package artefact.umlClassDiagram;

import artefact.generic.ConstructionPrimitive;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Create Generalization</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link artefact.umlClassDiagram.CreateGeneralization#getSub <em>Sub</em>}</li>
 *   <li>{@link artefact.umlClassDiagram.CreateGeneralization#getSuper <em>Super</em>}</li>
 * </ul>
 * </p>
 *
 * @see artefact.umlClassDiagram.UmlClassDiagramPackage#getCreateGeneralization()
 * @model
 * @generated
 */
public interface CreateGeneralization extends ConstructionPrimitive {
	/**
	 * Returns the value of the '<em><b>Sub</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub</em>' attribute.
	 * @see #setSub(String)
	 * @see artefact.umlClassDiagram.UmlClassDiagramPackage#getCreateGeneralization_Sub()
	 * @model
	 * @generated
	 */
	String getSub();

	/**
	 * Sets the value of the '{@link artefact.umlClassDiagram.CreateGeneralization#getSub <em>Sub</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sub</em>' attribute.
	 * @see #getSub()
	 * @generated
	 */
	void setSub(String value);

	/**
	 * Returns the value of the '<em><b>Super</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Super</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super</em>' attribute.
	 * @see #setSuper(String)
	 * @see artefact.umlClassDiagram.UmlClassDiagramPackage#getCreateGeneralization_Super()
	 * @model
	 * @generated
	 */
	String getSuper();

	/**
	 * Sets the value of the '{@link artefact.umlClassDiagram.CreateGeneralization#getSuper <em>Super</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Super</em>' attribute.
	 * @see #getSuper()
	 * @generated
	 */
	void setSuper(String value);

} // CreateGeneralization
