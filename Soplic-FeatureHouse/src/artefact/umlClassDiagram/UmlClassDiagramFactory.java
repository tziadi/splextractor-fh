/**
 */
package artefact.umlClassDiagram;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see artefact.umlClassDiagram.UmlClassDiagramPackage
 * @generated
 */
public interface UmlClassDiagramFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UmlClassDiagramFactory eINSTANCE = artefact.umlClassDiagram.impl.UmlClassDiagramFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Class Diagram</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Class Diagram</em>'.
	 * @generated
	 */
	ClassDiagram createClassDiagram();

	/**
	 * Returns a new object of class '<em>Create Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Create Class</em>'.
	 * @generated
	 */
	CreateClass createCreateClass();

	/**
	 * Returns a new object of class '<em>Create Association</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Create Association</em>'.
	 * @generated
	 */
	CreateAssociation createCreateAssociation();

	/**
	 * Returns a new object of class '<em>Create Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Create Attribute</em>'.
	 * @generated
	 */
	CreateAttribute createCreateAttribute();

	/**
	 * Returns a new object of class '<em>Create Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Create Operation</em>'.
	 * @generated
	 */
	CreateOperation createCreateOperation();

	/**
	 * Returns a new object of class '<em>Create Generalization</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Create Generalization</em>'.
	 * @generated
	 */
	CreateGeneralization createCreateGeneralization();

	/**
	 * Returns a new object of class '<em>Association Invariant</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Association Invariant</em>'.
	 * @generated
	 */
	AssociationInvariant createAssociationInvariant();

	/**
	 * Returns a new object of class '<em>Create Package</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Create Package</em>'.
	 * @generated
	 */
	CreatePackage createCreatePackage();

	/**
	 * Returns a new object of class '<em>Create Operation Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Create Operation Refinement</em>'.
	 * @generated
	 */
	CreateOperationRefinement createCreateOperationRefinement();

	/**
	 * Returns a new object of class '<em>Create Interface</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Create Interface</em>'.
	 * @generated
	 */
	CreateInterface createCreateInterface();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	UmlClassDiagramPackage getUmlClassDiagramPackage();

} //UmlClassDiagramFactory
