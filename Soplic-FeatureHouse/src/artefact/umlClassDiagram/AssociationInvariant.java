/**
 */
package artefact.umlClassDiagram;

import artefact.generic.Constraint;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Association Invariant</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see artefact.umlClassDiagram.UmlClassDiagramPackage#getAssociationInvariant()
 * @model
 * @generated
 */
public interface AssociationInvariant extends Constraint {
} // AssociationInvariant
