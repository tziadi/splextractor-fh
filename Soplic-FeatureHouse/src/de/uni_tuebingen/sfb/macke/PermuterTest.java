package de.uni_tuebingen.sfb.macke;
/*
 * PermuterTest.java
 *
 * Created on 3-mrt-2006
 *
 * Copyright (C) 2006 Hendrik Maryns <hendrik@sfs.uni-tuebingen.de>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import de.uni_tuebingen.sfb.macke.utilities.*;

import java.util.Arrays;

/**
 * A class for testing the combinatoric operators.
 *
 * @author <a href="mailto:hendrik.maryns@uni-tuebingen.de">Hendrik Maryns</a>
 */
public class PermuterTest {

        /**
         *
         *
         * @param args
         */
        public static void main(String args[]) {
                final String[] data = new String[5];
                for (int i = 0; i < data.length; i++) {
                        data[i] = "F"+i;
                }
//                for (String[] variation : new Permuter<String>(data)) {
//                        System.out.println(Arrays.toString(variation));
//                }
                System.out.println();
                for (int i = 0; i <= data.length; i++) {
                        for (String[] combination : new Combinator<String>(data, i)) {
                                System.out.println(Arrays.toString(combination));
                        }
                }
                /*System.out.println();
                for (int i = 0; i <= data.length; i++) {
                        for (String[] variation : new VariatorWithRepetition<String>(data,i)) {
                                System.out.println(Arrays.toString(variation));
                        }
                }*/
        }

}