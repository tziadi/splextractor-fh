package tests;

import java.util.ArrayList;
import java.util.Collection;

import aafst.constraint.Constraint;

import fr.lip6.meta.ple.featureIdentification.Feature;
import fr.lip6.meta.ple.featureIdentification.Product;
import fr.lip6.meta.splicfh.ConstraintsRulesExatrctionFST;
import fr.lip6.meta.splicfh.GenerateFeatureModelFST;
import fr.lip6.meta.tools.Trigger;

public class RulesExtractionTest {

	
	public static void test(){
		
		//Create features
		ArrayList<Feature> features = new ArrayList<Feature>();
		Feature f1 = new Feature();
		f1.setId("F1");
		f1.getProdIds().add(1);
		f1.getProdIds().add(2);
		features.add(f1);
		
		Feature f2 = new Feature();
		f2.setId("F2");
		f2.getProdIds().add(2);
		features.add(f2);
		
		
		Feature f3 = new Feature();
		f3.setId("F3");
		f3.getProdIds().add(2);
		f3.getProdIds().add(3);
		features.add(f3);
		
		
		Feature f4 = new Feature();
		f4.setId("F4");
		f4.getProdIds().add(3);
		features.add(f4);
		
		Feature f5 = new Feature();
		f5.setId("F5");
		f5.getProdIds().add(1);
		f5.getProdIds().add(3);
		features.add(f5);
		
		Feature f6 = new Feature();
		f6.setId("F6");
		f6.getProdIds().add(1);
		features.add(f6);
		//Create products
		ArrayList<Product> products = new ArrayList<Product>();
		Product p1 = new Product(1);
		products.add(p1);
		Product p2 = new Product(2);
		products.add(p2);
		Product p3 = new Product(3);
		products.add(p3);
		
		 ConstraintsRulesExatrctionFST ce2 = new ConstraintsRulesExatrctionFST();
	 	   GenerateFeatureModelFST gfm2 = new GenerateFeatureModelFST();
	 	    ArrayList<aafst.constraint.Constraint> constraintsBeh = new ArrayList<aafst.constraint.Constraint>(); 
	 	    
	 	   // Constraint c = ce2.extractProductRules(features, products);
	 	   //constraintsBeh.add(c);
	 	    
	 	   
	 	   //System.out.println("   Constrait :"+c.toString());
		
	}
	
	
	public static void main(String[] args){
		test();
		
	}
	
}
