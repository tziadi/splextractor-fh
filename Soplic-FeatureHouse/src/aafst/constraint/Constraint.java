package aafst.constraint;

import fr.lip6.meta.ple.featureIdentification.Feature;

public class Constraint {

 private Expression left;
 private Expression right;
 
public Constraint(){
	 
	 
 }
 
 public Constraint(Expression l, Expression r){
	 
	 left=l;
	 right =r;
 }

public Constraint(Feature f1, Feature f2){
	 left=new Leaf(f1);
	 right =new Leaf(f2);
 }

public Constraint(Expression e, Feature f2){
	 left=e;
	 right =new Leaf(f2);
}

public Constraint(Feature f1, Expression e){
	 left=new Leaf(f1);
	 right =e;
}



public Expression getLeft() {
	return left;
}

public Expression getRight() {
	return right;
}


@Override
public String toString() {
	return getLeft() + " implies " + getRight();
}

public void display()
{
	System.out.println(this.toString());
}
/**
 * 
 * @param c
 * @return true if the constrains contains the same nodes and leafs
 */
public boolean equals(Object o){
	
	if (o==null)
		return false;
	if (this.getClass()!=o.getClass())
		return false;
	
	else {
	//if (c.getLeft().equals(this) && c.getRight().equals(this))
	if (o instanceof Constraint){
		
	 Constraint c = (Constraint)o;
	if (this.getLeft().toXML().equals(c.getLeft().toXML())&&
		this.getRight().toXML().equals(c.getRight().toXML()))
			return true;
	else
		return false;
      }
	return false;
	}	
}
}




