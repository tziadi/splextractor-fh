package aafst.constraint;

public class Not extends Expression {

	public Not(Expression right) {
		super();
		this.right = right;
	}

	private Expression right;

	public Expression getRight() {
		return right;
	}

	public void setRight(Expression right) {
		this.right = right;
	}
	
	@Override
	public String toString() {
		return "not " + getRight();
	}
	
	public String toXML(){
		String result;
		result =("<not>"+right.toXML()+"</not>");
		
		
		return result;
	}
}
