package aafst.constraint;

public class And extends Binary {

	public And(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	public String toString() {
		return "(" + getLeft() + " and " + getRight() + ")";
	}
	public String toXML() {
		String result;
		result =("<conj>"+super.toXML()+"</conj>");
		
		
		return result;
	}
}
