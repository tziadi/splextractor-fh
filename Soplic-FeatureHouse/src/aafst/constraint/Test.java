package aafst.constraint;

public class Test {

	public Test() {
		// TODO Auto-generated constructor stub
	}
	public static  String gettext(Or o)
	{
		if(o.getLeft() instanceof Leaf && o.getRight() instanceof Leaf)
		{
			return "(" + o.getLeft().toString() + " or " + o.getRight().toString() + ")";

			
			
		}
		else
		{
			if(o.getLeft() instanceof Or)
			{
				return gettext((Or) o.getLeft())+" or "+o.getRight();
			}
			else
			{
			return " "+o.getLeft() ;	
			}
			
		}
		

	}
	
	

	
	public static  String getXml(Or o)
	{
		
			if(o.getLeft() instanceof Or)
			{
				return "\n<disj>\n"+o.getRight().toXML()+getXml((Or) o.getLeft())+"\n</disj>";
			}
			else
			{
				if(o.getRight()!=null)
			return "<disj>\n "+o.getLeft().toXML()+o.getRight().toXML()+"</disj>\n" ;	
				else
					return "<disj>\n "+o.getLeft().toXML()+"</disj>\n" ;	
			}
			
		
		

	}
	
}
