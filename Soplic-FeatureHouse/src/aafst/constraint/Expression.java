package aafst.constraint;

import java.util.ArrayList;

import fr.lip6.meta.ple.featureIdentification.Feature;

public abstract class Expression extends Constraint{
	
	
	public Expression() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Caution, the function verify if the trees are stricly equals, not equivalents.
	 * @param e
	 * @return true if the tree is the same
	 */
	/*public Boolean equals (Expression e){
		if((this instanceof Binary) && (e instanceof Binary)){
			
		}
		
		
	}*/

	public boolean containsFeature(Feature f){
		if(this instanceof Leaf){
			if (((Leaf)this).getFeature().getId()==f.getId())
				return true;
			else
				return false;
		}
		else if(this instanceof Not){
			return ((Not)this).getRight().containsFeature(f);
		}
		else if (this instanceof Binary){
			if( ((Binary)this).getLeft().containsFeature(f))
				return true;
			else
				return ((Binary)this).getRight().containsFeature(f);
		}
		else
			System.out.println("Type non reconnu, erreur");
		
		
		return false;
	}
	
	/**
	 * @return an arrayList of all Leafs, given by infix order.
	 */
	public ArrayList<Leaf> getAllLeafs(){
		ArrayList<Leaf> result = new ArrayList<Leaf>();
		this.getAllLeafsRecursive(result);		
		return result;
	}
	
	private void getAllLeafsRecursive(ArrayList<Leaf> result){
		/*verifiying if the expression is only a Leaf*/
		
		if (this instanceof Leaf){
			Leaf cursor=(Leaf)this;
			result.add(cursor);
			return;
		}
		
		if (this instanceof Not){
			Not cursor=(Not)this;
			cursor.getRight().getAllLeafsRecursive(result);
			return;
		}
		
		if (this instanceof Binary){
			Binary cursor=(Binary)this;
			cursor.getLeft().getAllLeafsRecursive(result);
			cursor.getRight().getAllLeafsRecursive(result);
			return;
		}
		System.out.println("Type non reconnu");
		return;
	}
	public  abstract String toXML();
	
}
