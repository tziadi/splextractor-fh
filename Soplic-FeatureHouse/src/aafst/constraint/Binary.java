package aafst.constraint;

abstract class Binary extends Expression {

	private Expression left;
	private Expression right;
	
	public Binary(Expression left, Expression right) {
		super();
		this.left = left;
		this.right = right;
	}

	
	
	public Expression getLeft() {
		return left;
	}
	
	public void setLeft(Expression left) {
		this.left = left;
	}
	
	public Expression getRight() {
		return right;
	}
	
	public void setRight(Expression right) {
		this.right = right;
	}
	
	public String toXML(){
		String result;
		result =(left.toXML()+"\n")+  (right.toXML()+"\n");
		
		
		return result;
	}

	
}
