package aafst.constraint;

import fr.lip6.meta.ple.featureIdentification.Feature;

public class Leaf extends Expression {
	
	//Leaf is an adaptator for Feature
	Feature feature;
	
	public Leaf(Feature feature) {
		super();
		this.feature = feature;
	}

	public Feature getFeature() {
		return feature;
	}

	public void setFeature(Feature feature) {
		this.feature = feature;
	}
	
	@Override
	public String toString() {
		return feature.getId();
	}
	public String toXML(){
		String result;
		if(feature.getId().split("_").length>1)
		{
			result =("<var>"+feature.getId()+"</var>");	
		}
		else{
		result =("<var>"+feature.getId()+"</var>");
		}
		
		
		return result;
	}
}
