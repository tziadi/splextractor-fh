package aafst.constraint;



public class Or extends Binary {

	public Or(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	public String toString() {
		return "(" + getLeft() + " or " + getRight() + ")";
	}

	public String toXML() {
		return Test.getXml(this);
		
		
		
	}
}
