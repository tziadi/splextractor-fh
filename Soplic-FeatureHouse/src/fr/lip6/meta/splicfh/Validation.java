package fr.lip6.meta.splicfh;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import printer.PrintVisitorException;
import cide.gparser.ParseException;

import artefact.FST.CreateFSTNonTerminal;
import artefact.FST.CreateFSTTerminal;
import artefact.generic.Artefact;
import artefact.generic.ConstructionPrimitive;
import de.ovgu.cide.fstgen.ast.FSTTerminal;
import fr.lip6.meta.ple.plextraction.Artefact2File;
import java.io.File;

public class Validation {

    static FileWriter fw;
    static PrintWriter pw;
    static ArrayList<Integer> found = new ArrayList<Integer>();

    public static void main(String[] args) {
        LanguageConfigurator.LANGUAGE = new JavaLanguage();
        validateInputProducts("./products/java/argouml",
                "./Results/Results_10.09.2012_for_java_argouml_with_10_variants/impl_view/allGeneratedInitialProducts",
                "./Results/Results_10.09.2012_for_java_argouml_with_10_variants", 9, 9, "ArgoUML");

    }

    public static void validateMandatoryFeatures(String pathToInputMandatoryFeatures,
            int nbMandatoryFeature,
            String pathToOutMandatoryFeature, String nameExample, String outputDir) {


        // Parse input mandatory features: one artefact per mandatory feature

        System.out.println("input");
        ArrayList<Artefact> inputs = readProducts(pathToInputMandatoryFeatures, nbMandatoryFeature);


        //union of mandatory artefacts
        Artefact input = union(inputs);

        //Parse output mandatory feature: one artafacts

        ReadFSTProduct rp1 = new ReadFSTProduct();
        Artefact output = null;
        System.out.println("output");
//        ArrayList<Artefact> outputs = readProducts(pathToOutMandatoryFeature+"/F0", 1);
//
//        output = union(outputs);

        output = readOneProduct(pathToOutMandatoryFeature + "/F0");



//        try {
//
//            output = rp1.readOneProduct(1, pathToOutMandatoryFeature, "F0");
//        } catch (FileNotFoundException e1) {
//            // TODO Auto-generated catch block
//            e1.printStackTrace();
//        } catch (ParseException e1) {
//            // TODO Auto-generated catch block
//            e1.printStackTrace();
//        } catch (PrintVisitorException e1) {
//            // TODO Auto-generated catch block
//            e1.printStackTrace();
//        }


        //compares features

        boolean result = equalsArtefact(input, output);
        System.out.println("equal:" + result);
        if (!result) {
            ArrayList<ConstructionPrimitive> cps = diff(input, output);


            for (ConstructionPrimitive c : cps) {
                System.out.println("missing: " + c);
            }
        }

        //print result

        try {
            fw = new FileWriter(outputDir + "/evaluationResultMandatory_" + nameExample + ".txt");
            pw = new PrintWriter(fw);

            pw.println("Input Mandaroty Features :" + inputs.size());
            ArrayList<ConstructionPrimitive> pc1 = pcOfArtefact(input);
            pw.println("          Size of the union of all input mandatory features :  " + input.getId() + " est de taille =" + pc1.size());


            pw.println("Output Mandaroty Feature :");
            ArrayList<ConstructionPrimitive> pc2 = pcOfArtefact(output);
            pw.println("          Size of the output mandatory feature :  " + output.getId() + " est de taille =" + pc2.size());

            pw.println("equal:" + (result ? "[OK]" : "[KO]"));
            if (!result) {
                ArrayList<ConstructionPrimitive> cps = diff(input, output);


                for (ConstructionPrimitive c : cps) {
                    pw.println("Missing construct. primitive: " + c);
                }
            }

            System.out.println("Taille INPUTS:" + inputs.size());
            pw.close();
        } catch (IOException e) {

            e.printStackTrace();
        }

    }

    private static ArrayList<ConstructionPrimitive> getPrimtivesOfArefact(Artefact a) {

        ArrayList<ConstructionPrimitive> result =
                new ArrayList<ConstructionPrimitive>();

        ConstructionPrimitive first = a.getFirst();
        result.add(first);
        ConstructionPrimitive primitive = first;

        while (primitive.getNext() != null) {

            result.add(primitive.getNext());
            primitive = primitive.getNext();

        }


        return result;
    }

    private static Artefact union(ArrayList<Artefact> inputs) {

        Artefact result = inputs.get(0);
        ConstructionPrimitive last = result.getlast();
        for (Artefact a : inputs) {
            if (a == result) {
                last.setNext(a.getFirst());
                last = a.getlast();

            }
        }

        return result;

    }

    public static void validateInputProducts(String pathInputProducts, String pathOutputProducts, String outputDir,
            int nbVariantInput, int nbVariantOutput, String nameExample) {


        try {
            fw = new FileWriter(outputDir + "/evaluationResultInputProducts_" + nameExample + ".txt");
            pw = new PrintWriter(fw);

            validateProducts(pathInputProducts, pathOutputProducts, nbVariantInput, nbVariantOutput);
            pw.close();
        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    public static boolean validateProducts(String pathInputProducts, String pathOutputProducts,
            int nbVariantInput, int nbVariantOutPut) throws IOException {



        ArrayList<Artefact> inputs = readProducts(pathInputProducts, nbVariantInput);

        ArrayList<Artefact> outputs = readProducts(pathOutputProducts, nbVariantOutPut);

        pw.println("");

        pw.println("NP Products Inputs :" + inputs.size());
        pw.println("NP Products Output :" + outputs.size());

        System.out.println("Taille INPUTS:" + inputs.size());
        for (Artefact artef : inputs) {

            if (!existArtefact(artef, outputs)) {

                pw.println("Validation IncoCorrect/False");
                return false;
            }

        }

        pw.println("Validation Correct/True");
        return true;
    }

    private static boolean existArtefact(Artefact a, ArrayList<Artefact> outputs) {

        for (Artefact ar : outputs) {
            if (equalsArtefact(a, ar)) {
                found.add(ar.getId());
                return true;

            } else {
                ArrayList<ConstructionPrimitive> diff = diff(a, ar);

                display(diff);

            }
        }
        return false;
    }

    private static void display(ArrayList<ConstructionPrimitive> diff) {

        for (ConstructionPrimitive p : diff) {
            if (p instanceof CreateFSTNonTerminal) {
                CreateFSTNonTerminal nt = (CreateFSTNonTerminal) p;

                //pw.println("        Diff CP :  "+nt.getName() +nt.getType());
            }
            if (p instanceof CreateFSTTerminal) {
                CreateFSTTerminal nt = (CreateFSTTerminal) p;

                //pw.println("        Diff CP :  "+nt.getName() +nt.getType());


            }

        }


    }

    private static ArrayList<ConstructionPrimitive> pcOfArtefact(Artefact a) {

//        ArrayList<ConstructionPrimitive> result = new ArrayList<ConstructionPrimitive>();
//
//
//        ConstructionPrimitive first = a.getFirst();
//
//        if (first != null) {
//
//            //if (first instanceof CreateFSTNonTerminal
//
//
//            result.add(first);
//        }
//        ConstructionPrimitive next = first.getNext();
//        if (next != null) {
//            result.add(next);
//        }
//
//        
//        while (next != null) {
//            next = next.getNext();
//            result.add(next);
//        }
//
//
//        return result;




        ArrayList<ConstructionPrimitive> result = new ArrayList<ConstructionPrimitive>();


        ConstructionPrimitive first = a.getFirst();

        if (first != null) {

            //if (first instanceof CreateFSTNonTerminal


            result.add(first);
        } else {
            return result;
        }

        if (a.getlast() != null) {
            if (first.equals(a.getlast())) {
                return result;
            }
        }


        ConstructionPrimitive next = first.getNext();


        if (a.getlast() != null) {
            while (!next.equals(a.getlast())) {
                next = next.getNext();
                if (next != null) {
                    result.add(next);
                }
            }
        } else {
            while (next != null) {
                next = next.getNext();
                if (next != null) {
                    result.add(next);
                }
            }


        }
        return result;
    }

    private static ArrayList<ConstructionPrimitive> diff(Artefact a1, Artefact a2) {

        ArrayList<ConstructionPrimitive> result = new ArrayList<ConstructionPrimitive>();

        ArrayList<ConstructionPrimitive> pc1 = pcOfArtefact(a1);
        ArrayList<ConstructionPrimitive> pc2 = pcOfArtefact(a2);


        for (ConstructionPrimitive p : pc1) {

            if (p != null && pc2 != null) {
                if (!existConstructionPrimitive(p, pc2)) {

                    result.add(p);

                }
            }
        }

        return result;

    }

    private static boolean equalsArtefact(Artefact a, Artefact ar) {



        pw.println("Compraraison :  " + a.getId() + " et " + ar.getId());

        ArrayList<ConstructionPrimitive> pc1 = pcOfArtefact(a);
        pw.println("                    Artefact :  " + a.getId() + " est de taille =" + pc1.size());


        ArrayList<ConstructionPrimitive> pc2 = pcOfArtefact(ar);
        pw.println("                    Artefact :  " + ar.getId() + " est de taille =" + pc2.size());

//                if (pc1.size()!=pc2.size())
//                        return false;

        for (ConstructionPrimitive p : pc1) {

            if (p != null && pc2 != null) {
                if (!existConstructionPrimitive(p, pc2)) {

                    pw.println("Compraraison :  " + a.getId() + " et " + ar.getId() + " FALSE");
                    return false;

                }
            }
        }


        return true;
    }

    private static boolean existConstructionPrimitive(ConstructionPrimitive p,
            ArrayList<ConstructionPrimitive> pc2) {

        if (p != null && pc2 != null) {
            for (ConstructionPrimitive pp : pc2) {
                if (pp != null && p != null) {
                    if (equalsConstructionPtimitive(p, pp)) {

                        return true;
                    }


                }
            }

        }
        return false;
    }

    private static boolean equalsConstructionPtimitive(ConstructionPrimitive p,
            ConstructionPrimitive pp) {

        if (p == pp) {
            return true;
        } else if ((p.getClass() != pp.getClass())) {
            return false;
        } else {
            if ((p instanceof CreateFSTNonTerminal)
                    && (pp instanceof CreateFSTNonTerminal)) {
                CreateFSTNonTerminal nt = (CreateFSTNonTerminal) p;
                CreateFSTNonTerminal nt2 = (CreateFSTNonTerminal) pp;
                // System.out.println("        Equal CP :  "+nt.getName() +nt.getType()+" et "+ nt2.getName()+nt2.getType());
                return nt.getName().equals(nt2.getName()) && nt.getType().equals(nt2.getType());
            }

            if ((p instanceof CreateFSTTerminal)
                    && (pp instanceof CreateFSTTerminal)) {
                CreateFSTTerminal nt = (CreateFSTTerminal) p;
                CreateFSTTerminal nt2 = (CreateFSTTerminal) pp;
                // System.out.println("        Equal CP :  "+nt.getName() +"  "+nt.getType()+
                //   " et "+ nt2.getName()+nt2.getType());


                if (nt.getBody() == null || nt2.getBody() == null) {
                    return (nt.getName().equals(nt2.getName()) && nt.getType().equals(nt2.getType()));
                } else {
                    return nt.getName().equals(nt2.getName()) && nt.getType().equals(nt2.getType())
                            && nt.getBody().equals(nt2.getBody());
                }
            }

        }
        return false;
    }

    private static Artefact readOneProduct(
            String pathInputProducts) {

        ArrayList<Artefact> artefacts = new ArrayList<Artefact>();

        UtiliClassFiles diskFileExplorer = new UtiliClassFiles(pathInputProducts, false);
        ArrayList<String> allDirVariants = diskFileExplorer.listDir(pathInputProducts);


        int j = 1;




        try {
            ReadFSTProduct rp1 = new ReadFSTProduct();
            rp1.readProduct(j, pathInputProducts);

            // if (rp1.getArtefact().getFirst()!=null) {
            artefacts.add(rp1.getArtefact());
            HashMap<String, ArrayList<HashMap<String, FSTTerminal>>> bd = rp1.getBodies_nodes();



            j++;



        } catch (Exception e) {

            e.printStackTrace();
        }


        return artefacts.get(0);

    }

    private static ArrayList<Artefact> readProducts(
            String pathInputProducts, int nb) {

        ArrayList<Artefact> artefacts = new ArrayList<Artefact>();

        UtiliClassFiles diskFileExplorer = new UtiliClassFiles(pathInputProducts, false);
        ArrayList<String> allDirVariants = diskFileExplorer.listDir(pathInputProducts);


        int j = 1;
        for (String dirVariant : allDirVariants) {

            if (j > nb) {
                break;
            }

            try {
                ReadFSTProduct rp1 = new ReadFSTProduct();
                rp1.readProduct(j, dirVariant);

                // if (rp1.getArtefact().getFirst()!=null) {
                artefacts.add(rp1.getArtefact());
                HashMap<String, ArrayList<HashMap<String, FSTTerminal>>> bd = rp1.getBodies_nodes();



                j++;



            } catch (Exception e) {

                e.printStackTrace();
            }




        }

        return artefacts;

    }
}