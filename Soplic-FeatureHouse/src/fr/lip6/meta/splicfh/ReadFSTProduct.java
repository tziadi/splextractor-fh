package fr.lip6.meta.splicfh;
import de.ovgu.cide.fstgen.ast.FSTNonTerminal;
import de.ovgu.cide.fstgen.ast.FSTTerminal;
import de.ovgu.cide.fstgen.ast.FSTNode;
import fr.lip6.meta.ple.featureIdentification.Feature;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

import printer.PrintVisitorException;

import tmp.generated_java15.Java15Parser;
import artefact.generic.Artefact;
import cide.gparser.OffsetCharStream;
import cide.gparser.ParseException;


public class ReadFSTProduct {

	private HashMap<String, ArrayList<HashMap<String, FSTTerminal>>> bodies_nodes = 
			new HashMap<String, ArrayList<HashMap<String, FSTTerminal>>>();
	HashMap<String, String> MethodsOfProduct = null;
	
	public HashMap<String, String> getBody() {
		return MethodsOfProduct;
	}
	

	


	
	private Artefact artefact = null;
	 
	
	

	public Artefact getArtefact() {
		return artefact;
	}

	
	

	public void readProduct(int idProduct, String pathToExplore) throws 
									FileNotFoundException, ParseException, PrintVisitorException{
      
		
		UtiliClassFiles diskFileExplorer = new UtiliClassFiles(pathToExplore, true);
        
        //ArrayList<String> allFiles = diskFileExplorer.listeFiles(pathToExplore);
		ArrayList<String> allFiles = diskFileExplorer.listFiles(null, new File(pathToExplore));
        System.out.println("#####  Parsing Files  ##### :"+pathToExplore);
       
       FST2SoCPVisitor fst2cp = new FST2SoCPVisitor();
       ArrayList<FSTNode> theNodes = new ArrayList<FSTNode>();
       for (String fileName: allFiles){
    	   System.out.println("   File:"+fileName);
    	  if (LanguageConfigurator.getLanguage().isALanguageProgram(fileName)){
    		  
    		  FSTNonTerminal node = LanguageConfigurator.getLanguage().parseFile(fileName);
    	       theNodes.add(node);
    	       System.out.println("##### FILE  ADDED ##### :"+fileName);
    	  } 
          }
       fst2cp.visit(theNodes);
        
      bodies_nodes=fst2cp.getBodies_nodes();
       artefact = fst2cp.getProduct();
		
	   artefact.setId(idProduct);
	   
	   this.MethodsOfProduct=fst2cp.getBody();
	   
	}


	public HashMap<String, ArrayList<HashMap<String, FSTTerminal>>> getBodies_nodes() {
		return bodies_nodes;
	}


	public HashMap<String, String> getMethodsOfProduct() {
		return MethodsOfProduct;
	}		
}
	
	
	

