package fr.lip6.meta.splicfh;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import de.ovgu.cide.fstgen.ast.FSTNode;

import aafst.constraint.*;


import fr.lip6.meta.ple.featureIdentification.Feature;
import fr.lip6.meta.ple.featureIdentification.NullRootException;
import fr.lip6.meta.ple.featureIdentification.Product;
import fr.lip6.meta.tools.PLConstructionUtil;

public class ConstraintsRulesExatrctionFST {


        private HashMap<Integer, ArrayList<Feature>> featuresOfProducts = new  HashMap<Integer, ArrayList<Feature>>();
        private HashMap<Integer, ArrayList<Feature>> featuresOutOfProducts = new  HashMap<Integer, ArrayList<Feature>>();
        private ArrayList<Expression> constraintsProducts = new  ArrayList<Expression>();
        FileWriter fw ;
        PrintWriter pw ;

        public Collection<Constraint> extractProductRules(Collection<Feature> features, Collection<Product> allProducts,
        		HashMap<Feature, HashMap<String, ArrayList<Feature>>> allFeatures
                   ){

        	
        	try {
				fw = new FileWriter("./constraint.txt");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        		    
        	       pw = new PrintWriter(fw);
           
                ArrayList<Constraint> result = new ArrayList<Constraint>();

                for (Feature f: features){

                    if (!PLConstructionUtil.isMandatory(f, allProducts))   {  
                	LinkedHashSet<Integer> prods = f.getProdIds();

                         for(Integer i: prods){

                                 if (this.featuresOfProducts.containsKey(i)){

                                         this.featuresOfProducts.get(i).add(f);
                                 }
                                 else {

                                         ArrayList<Feature> fts = new ArrayList<Feature>();
                                         fts.add(f);
                                         this.featuresOfProducts.put(i, fts);
                                 }
                         }

                   }
                }

                for(Integer h: this.featuresOfProducts.keySet()){

                        ArrayList<Feature> fp = this.featuresOfProducts.get(h);

                        ArrayList<Feature> diff =getNoSelectedFeatures(fp, allFeatures, allProducts);
                        this.featuresOutOfProducts.put(h, diff);
                }

                
                //Create constraint

                for (Integer j : this.featuresOfProducts.keySet()){


                        ArrayList<Feature> featuresPresent = this.featuresOfProducts.get(j);
                        ArrayList<Feature> featuresAbsent = this.featuresOutOfProducts.get(j);


                   if (!featuresAbsent.isEmpty() && !featuresPresent.isEmpty()){

                           Expression exp1 = getAndExpression(featuresPresent);
                           Expression exp2 = getAndNot(featuresAbsent);

                           this.constraintsProducts.add(new And(exp1, exp2));

                   }
                }
 
               this.cleanNoSelectedFeatures(allFeatures);
               Expression res= constructFullConstraint();
               
              
               if (res!=null) {
            	   pw.println("############### textual format#########");
               pw.println(res.toString());
                   
            	   pw.println(res.toXML());
               }
               pw.close();
               
               result.add(res);
               
                return result;


        }

                private Expression constructFullConstraint() {

                	     if (this.constraintsProducts.size()==0)
                	    	 return null;
                	                    System.out.println("#### Taille constraintsProducts :"+this.constraintsProducts.size());
                                        int h=2;

                                        Expression left= this.constraintsProducts.get(0);

                                        Expression right=this.constraintsProducts.get(1);

                                        Expression or =new Or(left,right);

                                        while(h<this.constraintsProducts.size()){
                                                left= or;
                                                right = this.constraintsProducts.get(h);
                                                or =new Or(left,right);
                                                h++;
                                        }

                                       
                                        
                                       
                                        return or ;

  

                }

                private Expression getAndNot(ArrayList<Feature> featuresAbsent) {


                        int h=2;
                        
                        Expression and=null;
                        
                        if (featuresAbsent.size()>=2){

                                Expression left= new Not(new Leaf(featuresAbsent.get(0)));

                                Expression right=new Not(new Leaf(featuresAbsent.get(1)));

                                and =new And(left,right);
                              
                                while(h<featuresAbsent.size()){
                                        left= and;
                                        right = new Not(new Leaf(featuresAbsent.get(h)));
                                        and =new And(left,right);
                                      
                                        h++;
                                }

                        return and;
                        }
                        else {
                                if (!featuresAbsent.isEmpty())
                                        return new Not(new Leaf(featuresAbsent.get(0)));
                                else return null;
                        }

        }



                private Expression getAndExpression(ArrayList<Feature> features) {



                           int h=2;

                           if (features.size()>=2){

                                Expression left= new Leaf(features.get(0));

                                Expression right=new Leaf(features.get(1));

                                Expression and =new And(left,right);
                               
                                while(h<features.size()){
                                        left= and;
                                        right = new Leaf(features.get(h));
                                        and =new And(left,right);
                                       
                                        h++;
                                }
                                return and;
                           }
                           else {
                                   if (!features.isEmpty())
                                                return new Leaf(features.get(0));
                                        else return null;

                           }

        }


        private ArrayList<Feature> cleanNoSelectedFeatures(HashMap<Feature, HashMap<String, ArrayList<Feature>>> 
        allFeatures) {
        	
        	//System.out.println("@@@@@@@@ FeatureOfProducts : " +this.featuresOfProducts.entrySet());
        	//System.out.println("Clean all Features :"+allFeatures.keySet());
        	ArrayList<Feature> result = new ArrayList<Feature>();
        	
        	for (Feature current:allFeatures.keySet()){
        		
        		HashMap<String, ArrayList<Feature>> allSubFeatures = 
        				allFeatures.get(current);
        		
        		for (String abstractFeature: allSubFeatures.keySet()){
        			ArrayList<Feature> subFeatures = allSubFeatures.get(abstractFeature);
        			clean(subFeatures);
        		
        			
        		}
        	}
        	
        	
        	return result;
        	
        	
        }

        private void clean(ArrayList<Feature> subFeatures) {
			
        	for(Integer idProduct : this.featuresOfProducts.keySet()){
        		
        		ArrayList<Feature> featuresOfPr = 
        				this.featuresOfProducts.get(idProduct);
        		ArrayList<Feature> featuresOutPr = 
        				this.featuresOutOfProducts.get(idProduct);
        		
        		for(Feature f: subFeatures){
        			
        			if (featuresOfPr.contains(f)){
        				
        				featuresOutPr.removeAll(subFeatures);
        			}
        		}
        	}
        	
		}

		private ArrayList<Feature> getNoSelectedFeatures(ArrayList<Feature> featureSelectedProducts,
                        HashMap<Feature, HashMap<String, ArrayList<Feature>>> allFeatures, Collection<Product> allProducts) {
               
        		ArrayList<Feature> result = new ArrayList<Feature>();

                for (Feature currentFeature: allFeatures.keySet()){

                	 
                       
                	  if (!featureSelectedProducts.contains(currentFeature) ){
                		  
                		  if (!PLConstructionUtil.isMandatory(currentFeature, allProducts))     															
                        	
                        	result.add(currentFeature);
                                
                       }
                     
                }
                return result;
        }
        /**
         * Noting fancy happens here to be compatible with older code
         */
        public ConstraintsRulesExatrctionFST()
        {
                super();
        }






        /**
         * Entry point for the package constraintextration.
         * This is where all happens
         * @param allFeatures
         * @param allProducts
         * @return ArrayList of Constraints, so two Expression of features, presence of one's implying the
         * presence of the other one.
         */
        public ArrayList<Constraint> extractRules(Collection<Feature> allFeatures,
                        Collection<Product> allProducts){

                ArrayList<Constraint> result = new ArrayList<Constraint>();
                ArrayList<Constraint> baseConstraints = new ArrayList<Constraint>();
                ArrayList<Constraint> baseConstraintsRemaining = new ArrayList<Constraint>();
                ArrayList<Constraint> fancyConstraints = new ArrayList<Constraint>();

                //First, Only simple constraints like F1 ==> F2
                for (Feature f:allFeatures)
                        for (Feature f1: allFeatures){
                                if(f != f1){
                                        if(f.getProdIds().containsAll(f1.getProdIds())
                                                        && !(f.getProdIds().size()==allProducts.size())
                                                        && !(f1.getProdIds().size()==allProducts.size())
                                                        )
                                        {
                                                baseConstraints.add(new Constraint(f1,f));
                                        }
                                }
                        }
                baseConstraintsRemaining.addAll(baseConstraints);

                //Here we try to reach as many constraints as possible
                //TODO A enfermer dans une boucle jusqu'à ce qu'aucune nouvelle modification soit faite ?

                fancyConstraints.addAll(ConstraintsCombineLeft(baseConstraints,baseConstraintsRemaining));
                fancyConstraints.addAll(ConstraintsCombineRight(baseConstraints,baseConstraintsRemaining));
                fancyConstraints.addAll(ConstraintsCombineTransitivity(baseConstraints,baseConstraintsRemaining));

                fancyConstraints.addAll(FancyConstraintsManager.ConstraintCombineJoints(baseConstraintsRemaining,fancyConstraints));
                //19rules before fancy

                //CODE A SUPPRIMER
                System.out.println("tests !!");
/*
                for(Product p: allProducts){
                        for(int ii=0; ii<p.size();ii++){
                                System.out.println(  ((Feature)p.get(ii)).getId()  ) );
                        }
                }
        */
                System.out.println("fin tests !!");
                //FIN CODE A SUPPR


                result.addAll(baseConstraintsRemaining);
                result.addAll(fancyConstraints);
                return result;

        }



        /**
         * Not doing anything yet
         * |    F1 => F2
         * |    F2 => F3
         * \_______>  F1 => F2 AND F3
         * @param baseConstraints
         * @param baseConstraintsRemaining will be modified, as the function will remove from it used constraints
         * @return
         */
        private ArrayList<Constraint> ConstraintsCombineTransitivity(
                        ArrayList<Constraint> baseConstraints,
                        ArrayList<Constraint> baseConstraintsRemaining) {
                // TODO Auto-generated method stub
                //The major problem with it is handling duplicate expressions on the right side of
                // the constraint
                ArrayList<Constraint> result = new ArrayList();
                //Detecter Expressions similaires
                //factoriser
                return result;
        }

        /**
         * |    F4 => F1
         * |    F4 => F2
         * \_______>  F4 => F1 AND F2
         * @param baseConstraints
         * @param baseConstraintsRemaining will be modified, as the function will remove from it used constraints
         * @return ArrayList with new constrains equivalent to the base constraint removed
         */
        private ArrayList<Constraint> ConstraintsCombineRight(
                        ArrayList<Constraint> baseConstraints,
                        ArrayList<Constraint> baseConstraintsRemaining) {

                ArrayList<Constraint> result = new ArrayList();

                //Detect similar expression
                //keys are features left on the constraints
                //Value is an array list containing all the feature present if key is present
                HashMap<Feature,ArrayList<Expression>> hm= new HashMap<Feature,ArrayList<Expression>>();
                ArrayList<Expression> current;
                for(Constraint c: baseConstraints)
                {
                        if (c.getLeft() instanceof Leaf)
                        {
                                Leaf currentLeaf = (Leaf) c.getLeft();
                                current = hm.get(currentLeaf.getFeature());
                                if(current == null)
                                        current = new ArrayList<Expression>();
                                current.add(c.getRight());
                                hm.put(currentLeaf.getFeature(),current);

                                //we remove the redundant constraint if it will be used later.
                                baseConstraintsRemaining.remove(c);
                        }
                }

                //factorize
                Set<Feature> keys = hm.keySet();
                Expression right;
                for(Feature featureOnLeft: keys)
                {
                        //if the constraint is simple we put it back in baseConstraintRemaining
                        if (hm.get(featureOnLeft).size()<2)
                        {
                                baseConstraintsRemaining.add(new Constraint(featureOnLeft, hm.get(featureOnLeft).remove(0)));
                                continue;
                        }
                        //Building right part of the constraint
                        right = new And(hm.get(featureOnLeft).remove(0),hm.get(featureOnLeft).remove(0));
                        for(Expression r: hm.get(featureOnLeft))
                        {
                                right = new And(r,right);
                        }
                        //Finally we add a new constraint to the result
                        result.add(new Constraint(featureOnLeft,right));
                }
                return result;
        }

        /**
         * Not doing anything yet
         * |    F1 => F4
         * |    F2 => F4
         * \_______>  F1 OR F2 => F4
         * @param baseConstraints
         * @param baseConstraintsRemaining will be modified, as the function will remove from it used constraints
         * @return
         */
        private ArrayList<Constraint> ConstraintsCombineLeft(
                        ArrayList<Constraint> baseConstraints,
                        ArrayList<Constraint> baseConstraintsRemaining) {
                // TODO Auto-generated method stub
                ArrayList<Constraint> result = new ArrayList();
                //Detecter Expressions similaires
                //factoriser
                return result;
        }



        /**
         * For debug Purposes, it displays the features list and associated products
         * @param allFeatures
         */
        public void displayContext(Collection<Feature> allFeatures)
        {
                for (Feature f:allFeatures){
                        System.out.println(f.getId()+" -> " + f.getProdIds());
                }
                System.out.println("");
        }

      
}