package fr.lip6.meta.splicfh;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import org.junit.Test;
import artefact.generic.Artefact;
import printer.PrintVisitorException;
import tmp.generated_java15.Java15Parser;
import cide.gparser.OffsetCharStream;
import cide.gparser.ParseException;
import de.ovgu.cide.fstgen.ast.FSTNonTerminal;



public class JavaParserTest {

	
	public static void runParser() throws FileNotFoundException, ParseException, PrintVisitorException {
		
				
		//Read variant 1
		
		Java15Parser p = new Java15Parser(new OffsetCharStream( new FileInputStream("Account.java")));
		
		p.CompilationUnit(false);
		
		FSTNonTerminal racine = (FSTNonTerminal)p.getRoot();
		
		SimplePrintVisitor spv = new SimplePrintVisitor();
		
		//FSTNonTerminal node = null;
		
		spv.visit((FSTNonTerminal)p.getRoot());
		
		FST2SoCPVisitor fst2cp = new FST2SoCPVisitor();
		fst2cp.visit(p.getRoot());
	//	System.out.println(spv.getResult());
//		JavaPrintVisitor jpv = new JavaPrintVisitor();
//		jpv.processNode(p.getRoot(), new File("testfst/v1/output"));
//		//spv.visit(p.getRoot());
//		
//		System.out.println(p.getRoot().printFST(0));
//		
//		Artefact a1 = fst2cp.getProduct();
//		
//		
//		a1.setId(1);
//		
//		//Read variant 2
//		
//		Java15Parser p2 = new Java15Parser(new OffsetCharStream( new FileInputStream("testfst/v2/Account.java")));
//		
//		
//		p2.CompilationUnit(false);
//		
//		FSTNonTerminal racine2 = (FSTNonTerminal)p2.getRoot();
//		
//		
//		SimplePrintVisitor spv2 = new SimplePrintVisitor();
//		//FSTNonTerminal node = null;
//		spv2.visit((FSTNonTerminal)p2.getRoot());
//		
//		FST2SoCPVisitor fst2cp2 = new FST2SoCPVisitor();
//		fst2cp2.visit(p2.getRoot());
//		System.out.println(spv2.getResult());
//		JavaPrintVisitor jpv2 = new JavaPrintVisitor();
//		jpv.processNode(p2.getRoot(), new File("testfst/v2/output"));
//		//spv.visit(p.getRoot());
//		
//		System.out.println(p2.getRoot().printFST(0));
//		
//		Artefact a2 = fst2cp2.getProduct();
//		a2.setId(2);
//		ArrayList<Artefact> artefacts = new ArrayList<Artefact>();
//		
//		artefacts.add(a1);
//		artefacts.add(a2);
//		String[] products =  {"p1", "p2"};
//		//ExtractFST.extract(artefacts, "fst", products);
//		
		
	}
	
	public static void main(String[] args) throws FileNotFoundException, ParseException{
		
		try {
			runParser();
		} catch (PrintVisitorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
