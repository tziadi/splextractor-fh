package fr.lip6.meta.splicfh;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import composer.FSTGenComposer;

import builder.ArtifactBuilder;
import builder.capprox.CApproxBuilder;
import builder.java.JavaBuilder;

import printer.PrintVisitorException;
import cide.gparser.ParseException;

import Jakarta.util.Util;
import artefact.generic.Artefact;

import de.ovgu.cide.fstgen.ast.FSTNode;
import de.ovgu.cide.fstgen.ast.FSTTerminal;
import fr.lip6.meta.ple.configsgenerator.ConfigsGenerator;
import fr.lip6.meta.ple.featureIdentification.Feature;
import fr.lip6.meta.ple.featureIdentification.NullRootException;
import fr.lip6.meta.ple.featureIdentification.Product;
import guidsl.variable;

public class ExtractorMain {

    static HashMap<String, HashMap<String, ArrayList<Integer>>> body = new HashMap<String, HashMap<String, ArrayList<Integer>>>();
    static ArrayList<Artefact> artefacts = new ArrayList<Artefact>();
    static HashMap<String, FSTNode> bodiesNodes = new HashMap<String, FSTNode>();
    static HashMap<Integer, HashMap<String, ArrayList<HashMap<String, FSTTerminal>>>> bodies_nodes =
            new HashMap<Integer, HashMap<String, ArrayList<HashMap<String, FSTTerminal>>>>();
    static ArtifactBuilder builder = null;

    /**
     * @param args
     */
    public static void main(String[] args) {

        String help = "    Usage: main dirOfinputProductvariants  typeOfSystem nameOfProductLine numberOfProductvariants" + "\n";

        help += "		where :" + "\n";
        help += "		* dirOfinputProductvariants:   " + "designates the source directory of product variants" + "\n";
        help += "		* typeOfSystem:   " + "designates the type of source code of paroduct variant : \"c\" for c-programs, \"java\" for java programs" + "\n";
        help += "		* nameOfProductLine:   " + "designates a name for the extracted SPL" + "\n";
        help += "		* numberOfProductvariants:   " + "designates the number of the considered product variants" + "\n";
        String repInputProducts = "";
        String type = "";
        String nameExample = "";
        int nbVariants = 0;

        if (!(args.length > 3)) {

            System.out.println(help);
        } else {



            repInputProducts = args[0];
            type = args[1];
            nameExample = args[2];
            nbVariants = Integer.parseInt(args[3]);

            System.out.println("Extracting product line from  : " + repInputProducts);
            System.out.println("   @@ " + type + " Systems @@");

            if (type.equals("java")) {


                LanguageConfigurator.LANGUAGE = new JavaLanguage();
                builder = new JavaBuilder();
            }

            if (type.equals("c")) {


                builder = new CApproxBuilder();
                LanguageConfigurator.LANGUAGE = new CLanguage();
            }

            /*
             * Step 1: Reading Input Product Variants 
             */

            System.out.println("@ SPLIC Step 1 : Reading Input Products");
            System.out.println("@------------");
            UtiliClassFiles diskFileExplorer = new UtiliClassFiles(repInputProducts, false);
            ArrayList<String> allDirVariants = diskFileExplorer.listDir(repInputProducts);
            
            
            //System.out.println("   Variants :"+allDirVariants);

            int j = 1;
            for (String dirVariant : allDirVariants) {
    
            	System.out.println("CURRENT DIR :"+dirVariant);
            	
                if (j > nbVariants) {
                    break;
                }

                try {

                    ReadFSTProduct rp1 = new ReadFSTProduct();
                    rp1.readProduct(j, dirVariant);
                    handleBodies(rp1.getBody(), j);
                    Artefact art = rp1.getArtefact();
                    System.out.println("MAIN FFFF :" + art.getId());
                    System.out.println("MAIN FFFF :" + art.getFirst());

                    artefacts.add(art);
                    HashMap<String, ArrayList<HashMap<String, FSTTerminal>>> bd = rp1.getBodies_nodes();
                    bodies_nodes.put(j, bd);
                    j++;



                } catch (Exception e) {

                    e.printStackTrace();
                }




            }

            String rep = type + "_" + nameExample + "_with_" + nbVariants + "_variants";

            try {
                UtiliClassFiles.recursifDelete(rep);
            } catch (IOException e1) {

                e1.printStackTrace();
            }
            String outPutresultDir = UtiliClassFiles.createRepWithDate(rep);


            try {
                ExtractFSTSol.extract(artefacts, body, bodies_nodes, outPutresultDir);
            } catch (NullRootException e) {

                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            String srepConfig = outPutresultDir + "/impl_view/allGeneratedInitialProducts";
            //regenerateInputProducts(outPutresultDir,srepConfig);
            //validate(repInputProducts, nameExample, nbVariants, outPutresultDir, srepConfig);
           
            
	

        }
    }

	private static void validate(String repInputProducts, String nameExample,
			int nbVariants, String outPutresultDir, String srepConfig) {
		/*
		 * Validation 
		 */

         Validation validation = new Validation();
        Validation.validateInputProducts(repInputProducts, srepConfig, outPutresultDir, nbVariants, ExtractFSTSol.allInputProducts.size(), nameExample);
		validation.validateMandatoryFeatures(repInputProducts + "/InputFeatures/mandatory", new File(repInputProducts +"/InputFeatures/mandatory").listFiles().length,
		     outPutresultDir + "/impl_view/features", nameExample, repInputProducts);
	}

	private static void regenerateInputProducts(String outPutresultDir, String srepConfig) {
		/*
		 * Generate all valid configurations
		 */


		System.out.println("@ Validation step : ");
		System.out.println("@------------");
		
		UtiliClassFiles.createRep(srepConfig);
//		    String cnfFile = srepConfig+"/cnfFile.cnf";
		int n = 1;
//			
		System.out.println("@ 		Validation step : Generate Configurations for all input product variants");
		System.out.println("		@------------");
		//System.out.println("Input produts :"+ExtractFSTSol.allInputProducts);

		//System.out.println("Feature Result  :"+ExtractFSTSol.allResultFeatures);

		for (Product p1 : ExtractFSTSol.allInputProducts) {

		    String nameFile = srepConfig + "/product_" + p1.getId() + ".config";

		    try {
		        //System.out.println("Generating products :"+nameFile);
		        generateConfigFilesForInputProducts(ExtractFSTSol.allResultFeatures, p1, nameFile);
		    } catch (IOException e) {

		        e.printStackTrace();
		    }

		    FSTGenComposer fstGen = new FSTGenComposer();
		    String[] arguments = {"--expression", nameFile, "--base-directory", outPutresultDir + "/impl_view/features/",
		        "--output-directory", srepConfig};
		    fstGen.run(arguments);

		}
	}

    public static void generateConfigFilesForInputProducts(Set<Feature> features, Product product, String fileName) throws IOException {
        FileWriter file = null;
        PrintWriter pw = null;

        file = new FileWriter(fileName);
        pw = new PrintWriter(file);

        for (Feature f : features) {
            if (f.getProdIds().contains(product.getId())) {
                pw.println(f.getId());
            }

        }

        file.close();
    }

    private static void handleBodies(HashMap<String, String> MethodsOfProduct, int idProduct) {

        for (String method : MethodsOfProduct.keySet()) {


            if (body.containsKey(method)) {

                String bMethod = MethodsOfProduct.get(method);
                HashMap<String, ArrayList<Integer>> bodiesOfMethod = body.get(method);


                if (bodiesOfMethod.containsKey(bMethod)) {

                    bodiesOfMethod.get(bMethod).add(idProduct);

                } else {


                    ArrayList<Integer> prod = new ArrayList<Integer>();
                    prod.add(idProduct);
                    bodiesOfMethod.put(MethodsOfProduct.get(method), prod);

                }
            } else {
                HashMap<String, ArrayList<Integer>> newb = new HashMap<String, ArrayList<Integer>>();
                ArrayList<Integer> prod = new ArrayList<Integer>();
                prod.add(idProduct);
                newb.put(MethodsOfProduct.get(method), prod);
                body.put(method, newb);

            }
        }


    }
}
