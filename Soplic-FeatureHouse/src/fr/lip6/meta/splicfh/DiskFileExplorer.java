package fr.lip6.meta.splicfh;
import java.io.File;
import java.util.ArrayList;


public class DiskFileExplorer {

    private String initialpath = "";
    private Boolean recursivePath = false;
    public int filecount = 0;
    public int dircount = 0;

/**
 * Constructeur
 * @param path chemin du r�pertoire
 * @param subFolder analyse des sous dossiers
 */
    public DiskFileExplorer(String path, Boolean subFolder) {
        super();
        this.initialpath = path;
        this.recursivePath = subFolder;
    }

   

    public ArrayList<String> listDirectory(String dir) {
        ArrayList<String> classes = new ArrayList<String>();
    	File file = new File(dir);
        File[] files = file.listFiles();
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
             
            
            	
            	
            	  if (files[i].isDirectory() == true) {
                 
                    //System.out.println("Dossier" + files[i].getAbsolutePath());
                    this.dircount++;
                } else {
                    //System.out.println("Fichier" + files[i].getAbsolutePath());

                    if (	LanguageConfigurator.getLanguage().isALanguageProgram(files[i].getAbsolutePath())) 
                    		//.endsWith("java"))
                    classes.add(files[i].getAbsolutePath());
                    this.filecount++;
                }
                if (files[i].isDirectory() == true && this.recursivePath == true) {
                    classes.addAll(this.listDirectory(files[i].getAbsolutePath()));
                }
            }
        }
       // System.out.println("ZISE before :"+classes.size());
        return classes;
    }

    /**
     * Exemple : lister les fichiers dans tous les sous-dossiers
     * @param args
     */
    public static void main(String[] args) {
        String pathToExplore = "./products/c/mail/Variant0001";
        DiskFileExplorer diskFileExplorer = new DiskFileExplorer(pathToExplore, true);
        Long start = System.currentTimeMillis();
      //  diskFileExplorer.list();
        System.out.println("----------");
        System.out.println("Analyse de " + pathToExplore + " en " + (System.currentTimeMillis() - start) + " mses");
        System.out.println(diskFileExplorer.dircount + " dossiers");
        System.out.println(diskFileExplorer.filecount + " fichiers");
    }
}