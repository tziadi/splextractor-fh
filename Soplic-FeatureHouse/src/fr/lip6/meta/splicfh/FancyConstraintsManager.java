package fr.lip6.meta.splicfh;

import java.util.ArrayList;

import aafst.constraint.And;
import aafst.constraint.Constraint;
import aafst.constraint.Leaf;

import fr.lip6.meta.ple.featureIdentification.Feature;
import fr.lip6.meta.ple.featureIdentification.Product;

public class FancyConstraintsManager {

	
	/**
	 * Not doing anything yet
	 * |    F1 => F2 AND F3
	 * \_______> look if F2 AND F3 ==> F1 
	 * @param baseConstraintsRemaining
	 * @param fancyConstraints of the form F1 => F2 AND ... AND Fx   (/!\ Only "AND")
	 * @return new fancy constraints.
	 */
	public static ArrayList<Constraint> ConstraintCombineJoints(
			ArrayList<Constraint> baseConstraintsRemaining,
			ArrayList<Constraint> fancyConstraints) {

		ArrayList<Constraint>results= new ArrayList<Constraint>();
		
		for(Constraint c: fancyConstraints){
			//we get all the leafs of the right side of the constraint
			ArrayList<Leaf> allLeafs = c.getRight().getAllLeafs();
			
			//Each time, we go towards a simple case like F1 => F2 AND F3
			//so we have to compute all possible bindings.
			for(int i =0; i< allLeafs.size(); i++){
				for(int j=i; j< allLeafs.size(); j++){
					Constraint simpleFancyConstraint = new Constraint(c.getLeft(),new And(allLeafs.get(i),allLeafs.get(j)));
					//Then, we verify if the pattern we look for is found in the base constraints
					if(lookForPattern(baseConstraintsRemaining,simpleFancyConstraint)){
						//we are here because the patter was found
						Constraint pattern=new Constraint(simpleFancyConstraint.getRight(),simpleFancyConstraint.getLeft());
						results.add(pattern);
						//System.out.println("got here2");
						

					}
				}	
			}			
		}

		

		return results;
	}
	/**
	 *	 
	 * @param baseConstraintsRemaining, All constraints remaining
	 * @param simpleFancyConstraint, a constraint of the form F1 => F2 AND F3
	 * @return true if the pattern F2 AND F3 => F1 is found
	 */
	private static Boolean lookForPattern(
			//Fonction dont je dois surveiller le comportement ?
			ArrayList<Constraint> baseConstraintsRemaining,
			Constraint simpleFancyConstraint) {
	
			Feature F1= ((Leaf)simpleFancyConstraint.getLeft()	).getFeature();
			Feature F2= ((Leaf)((And)simpleFancyConstraint.getRight()).getLeft()).getFeature();
			Feature F3= ((Leaf)((And)simpleFancyConstraint.getRight()).getRight()).getFeature();
		
			
			
			//Constraint pattern=new Constraint(simpleFancyConstraint.getRight(),simpleFancyConstraint.getLeft());
			//System.out.println("look for pattern : " + pattern );
			
			for(Integer p: F2.getProdIds()){ //p is a product Id
				if(F3.getProdIds().contains(p)){
					if(! F1.getProdIds().contains(p)){
						return false;
					}
				}
			}
			return true;
			

			//A partir d'ici, en chantier. !!
			
			
			
			//Ancien code
			
			/*
			Boolean F2Found=false;
			Boolean F3Found=false;
			for(Constraint baseCon: baseConstraintsRemaining){
				System.out.println("LFP: compare" +	((Leaf)baseCon.getLeft()).getFeature().getId() +
						" AND " +
				((Leaf)simpleFancyConstraint.getLeft()).getFeature().getId());
				if(((Leaf)baseCon.getLeft()).getFeature().getId() ==
						((Leaf)simpleFancyConstraint.getLeft()).getFeature().getId()){
					//Ici parties gauche de la contraintes sont égales
					System.out.println("partie gauche similaire detectée");//never go here ?!
					
					if(! F2Found){
						//check for F2
						F2Found=baseCon.getRight().containsFeature(F2);
						System.out.println("got here1");
					}
					if(! F3Found){
						//check for F3
						F3Found=baseCon.getRight().containsFeature(F3);
					}
					
					if(F2Found && F3Found)
						return true;
				}
			}
			
				return false;
			*/

	}
}
