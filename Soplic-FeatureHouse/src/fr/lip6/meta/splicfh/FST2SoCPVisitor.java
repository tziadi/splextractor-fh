package fr.lip6.meta.splicfh;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ArrayList;

import artefact.FST.CreateFSTNonTerminal;
import artefact.FST.CreateFSTTerminal;
import artefact.FST.FeatureTreeSet;
import artefact.FST.impl.FSTFactoryImpl;
import artefact.generic.Artefact;
import artefact.generic.ConstructionPrimitive;
import artefact.umlClassDiagram.CreatePackage;

import de.ovgu.cide.fstgen.ast.AbstractFSTPrintVisitor;
import de.ovgu.cide.fstgen.ast.FSTNode;
import de.ovgu.cide.fstgen.ast.FSTNonTerminal;
import de.ovgu.cide.fstgen.ast.FSTTerminal;

public class FST2SoCPVisitor extends AbstractFSTPrintVisitor  {
	
	 public ArrayList<String> constructions = new ArrayList<String>();
    private HashMap<String, ArrayList<HashMap<String, FSTTerminal>>> bodies_nodes = 
    			new HashMap<String, ArrayList<HashMap<String, FSTTerminal>>>(); 
	
    public HashMap<String, ArrayList<HashMap<String, FSTTerminal>>> getBodies_nodes() {
		return bodies_nodes;
  
	}
    
   
	
    boolean first = false;
    private ConstructionPrimitive currentPrimitive = null;

    private FSTFactoryImpl factory;
    private FeatureTreeSet product;
    
    private HashMap<String,  String> body = new HashMap<String,  String>();
	
	public boolean isFirst() {
		return first;
	}
	public ConstructionPrimitive getCurrentPrimitive() {
		return currentPrimitive;
	}
	public FSTFactoryImpl getFactory() {
		return factory;
	}
	public HashMap<String, String> getBody() {
		return body;
	}
	

	public FST2SoCPVisitor(PrintStream out) {
		super(out); generateSpaces=true;
	}
	public FST2SoCPVisitor() {
		super(); generateSpaces=true;
		factory = new FSTFactoryImpl();
		product = factory.createFeatureTreeSet();
		
	}
	
	public boolean visit(ArrayList<FSTNode> nodes){
		
		
		for (FSTNode node:nodes)
			 visit(node);
		
		return true;
	}
	
	public boolean visit(FSTNode theNodes) {
		
		//System.out.println(theNodes.getClass().getName());
		if (theNodes instanceof FSTNonTerminal){
			
			FSTNonTerminal nonTerminal = (FSTNonTerminal)theNodes;
			String key = nonTerminal.getName()+nonTerminal.getType();
		   // if (!this.constructions.contains(key)){
		    	this.constructions.add(key);
			CreateFSTNonTerminal createNT = factory.createCreateFSTNonTerminal();
		   
		      createNT.setName(nonTerminal.getName());
		      createNT.setType(nonTerminal.getType());
		      createNT.setFSTNode(nonTerminal);
		      //System.out.println("CreateFSTNonTerminal("+createNT.toString()+")");
		        if (!first){
		         currentPrimitive=createNT;
		         product.setFirst(createNT) ; 
		         first=true;
		         System.out.println("first "+createNT.toString());
		         
		        }
		         
		        else {
		         
		         
		         currentPrimitive.setNext(createNT);
		         currentPrimitive=createNT;
		        }
		        
		        	for (FSTNode n:nonTerminal.getChildren()){
		        		visit(n);
		        		}
		    //}
		}
		
		if (theNodes instanceof FSTTerminal){
			
			
			
			FSTTerminal terminal = (FSTTerminal)theNodes;
			
		 //negliger les declarations de packages.
			if (!LanguageConfigurator.getLanguage().isImportDec(terminal)
					&& !terminal.getType().equals("InitializerDecl")
					&& !terminal.getName().contains("auto")) {
			
			String key = terminal.getName()+terminal.getType();
		    //if (!this.constructions.contains(key)){
		    	this.constructions.add(key);
			if(LanguageConfigurator.getLanguage().isMethod(terminal) || 
					(LanguageConfigurator.getLanguage().isConstructor(terminal))){
				
				String methodName = terminal.getName();
				String methodBody = terminal.getBody();
				
				body.put(methodName, deleteteComments(methodBody));
				
				
				addToBodiesNodes(methodName, deleteteComments(methodBody), terminal);
				
			}
			
		    CreateFSTTerminal createT = factory.createCreateFSTTerminal();
		   
		      createT.setName(terminal.getName());
		      createT.setType(terminal.getType());
		      createT.setFSTNode(terminal);
		      //System.out.println("CreateFSTTerminal("+createT.toString()+")");
		        if (!first){
		         currentPrimitive=createT;
		         product.setFirst(createT) ;
		        
		         System.out.println("first "+createT.toString());
		         first=true;
		         
		        }
		         
		        else {
		         
		         
		         currentPrimitive.setNext(createT);
		         currentPrimitive=createT;
		        }
		        
		   }
			
		}
		
		 product.setLast(currentPrimitive);     
		
		return true;
	}
	private void addToBodiesNodes(String methodName, String methodBody,
			FSTTerminal terminal) {
		
		if (bodies_nodes.containsKey(methodName)){
			
			ArrayList<HashMap<String, FSTTerminal>> b= bodies_nodes.get(methodBody);
			for(HashMap<String, FSTTerminal> bb:b){
				
				if (bb.containsKey(methodBody))
					return;
			}
			//new body
			HashMap<String, FSTTerminal> newB = new HashMap<String, FSTTerminal>();
			newB.put(methodBody, terminal);
			b.add(newB);
		}	
		else {
			HashMap<String, FSTTerminal> newB = new HashMap<String, FSTTerminal>();
			newB.put(methodBody, terminal);
			ArrayList<HashMap<String, FSTTerminal>> newA = new ArrayList<HashMap<String, FSTTerminal>>();
			newA.add(newB);
			bodies_nodes.put(methodBody, newA);
				
			}
			
		}
		
	private static String deleteteComments(String test) {
		String[] result = test.split("\n");
		String chaine="";
		int x=0;
	    while (x<result.length){
	       	
	    	//System.out.println("Element :"+x + " :"+result[x]);
	    	
	    	if (result[x].startsWith("/*")) {
	    	    x++;	
	    		while ((x<result.length)&& !result[x].replaceAll("\\s",  "").startsWith("*/")){
	    			x++;
	    		}
	    		x++;
	    	}
	       	if ((x<result.length) &&!result[x].replaceAll("\\s",  "").startsWith("//")) {
	          chaine=chaine+result[x]+"\n";
	         
	    	}
	    	x++;
	    //	System.out.println("Iteration :"+x+ "  "+ chaine);
	    }
	      
	    return chaine.replaceAll("\\s", "") ;   
	    // return chaine.replaceAll("\\s", "");
		
		
	}
	
	@Override
	protected boolean isSubtype(String type, String expectedType) {
		// TODO Auto-generated method stub
		return false;
	}
	public Artefact getProduct() {
		// TODO Auto-generated method stub
		return product;
	}
	
}
