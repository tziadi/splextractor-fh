package fr.lip6.meta.splicfh;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import artefact.generic.Artefact;

import printer.PrintVisitorException;
import cide.gparser.ParseException;
import de.ovgu.cide.fstgen.ast.FSTNode;
import de.ovgu.cide.fstgen.ast.FSTTerminal;
import fr.lip6.meta.ple.featureIdentification.NullRootException;


public class MainCVariants {

	//HashMap<Integer, HashMap<String, HashMap<String, ArrayList<Integer>>>> bodyOfAllProducts;
	static HashMap<String, HashMap<String, ArrayList<Integer>>> body = new HashMap<String, HashMap<String, ArrayList<Integer>>>();
	static ArrayList<Artefact> artefacts = new ArrayList<Artefact>();
	static HashMap<String, FSTNode> bodiesNodes = new HashMap<String, FSTNode>();
	static HashMap<Integer,HashMap<String, ArrayList<HashMap<String, FSTTerminal>>>> bodies_nodes = 
			new HashMap<Integer,HashMap<String, ArrayList<HashMap<String, FSTTerminal>>>>();
	
	//� modifier pour chaque language
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i =0;
		LanguageConfigurator.LANGUAGE=new CLanguage();
		//Read variant 1,
		
		//String[] products = new String[6000];
		
		
		
		String p;
		for (int j=1; j<=10 ; j++) {
			
			
			if (j<10)
		      p= "Variant000"+j;
			else {
				
				if (j<100)
					p= "Variant00"+j;
				else{
					
				if ((j<1000))
					p= "Variant0"+j;
				else p= "Variant"+j;
			  }
			}
			try {
				ReadFSTProduct rp1 = new ReadFSTProduct();
				rp1.readProduct(j,"products/c/mail/"+p);
				handleBodies(rp1.getBody(),j);
			
				artefacts.add(rp1.getArtefact());
				
				//products[i]=p;
				HashMap<String, ArrayList<HashMap<String, FSTTerminal>>> bd = rp1.getBodies_nodes();
				bodies_nodes.put(j, bd);
								
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			
		}
		
		
		
		try {
		
			ExtractFSTSol.extract(artefacts, body, bodies_nodes,"Result");
			//ExtractFST.extract(artefacts, "fst", products, body, bodies_nodes);
		
		
		} catch (NullRootException e) {
		
			e.printStackTrace();
		
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		
		
	}
	
	private static void handleBodies(HashMap<String, String> MethodsOfProduct, int idProduct){
		
		
		  
		   for(String method : MethodsOfProduct.keySet()){
		 			
		 		  
		 		 if (body.containsKey(method)){	
		 			 
		 		   String bMethod = MethodsOfProduct.get(method); 	 
		 		   HashMap<String, ArrayList<Integer>> bodiesOfMethod = body.get(method);
		 		   
		 		   
		 		   	if (bodiesOfMethod.containsKey(bMethod)){
		 			       
		 		   		bodiesOfMethod.get(bMethod).add(idProduct);
		 			         
		 		   	}
		 		   	
		 		   	else {
		 		   		
		 		   		
		 		   	    ArrayList<Integer> prod  = new ArrayList<Integer>();
		 		   	    prod.add(idProduct);
		 		   	    bodiesOfMethod.put(MethodsOfProduct.get(method), prod);
		 		   		
		 		   	}
		 		 }
		 		 
		 		 else {
		 			 	HashMap<String, ArrayList<Integer>> newb = new HashMap<String, ArrayList<Integer>>();
		 		   	    ArrayList<Integer> prod  = new ArrayList<Integer>();
		 		   	    prod.add(idProduct);
		 		   	    newb.put(MethodsOfProduct.get(method), prod);
		 		   	    body.put(method, newb);
		 			 
		 		 }
		 		}
		 		

	
}
	
}
	


