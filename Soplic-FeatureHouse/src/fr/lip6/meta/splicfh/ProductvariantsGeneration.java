package fr.lip6.meta.splicfh;

import java.io.IOException;
import java.util.ArrayList;

import composer.FSTGenComposer;
import de.ovgu.cide.fstgen.ast.FSTNonTerminal;

import fr.lip6.meta.ple.configsgenerator.ConfigsGenerator;
import fr.lip6.meta.ple.featureIdentification.NullRootException;
import builder.capprox.CApproxBuilder;
import builder.java.JavaBuilder;

public class ProductvariantsGeneration {

	public static void main(String[] args){
		
		String repInputConfigs;
		
		String nameExample="" ;
		int nbConfigs=0;
		
		
			
		
			repInputConfigs=args[0];
			
			nameExample=args[1];
			nbConfigs = Integer.parseInt(args[2]);
		
		generate(nameExample,nbConfigs, repInputConfigs );
		
		
		
		
		
	
}
	
	
	private static void generate(String name, int nb, String repConfig){
		
		String repOutputGeneratedProducts = repConfig+"/generatedProducts_XMI_from_"+name+"_"+nb;
	    
	    try {
			UtiliClassFiles.recursifDelete(repOutputGeneratedProducts);
		} catch (IOException e1) {
			
			e1.printStackTrace();
		}
		 UtiliClassFiles.createRep(repOutputGeneratedProducts);
		
		UtiliClassFiles diskFileExplorer = new UtiliClassFiles(repConfig, true, "features");
		 System.out.println("   Rep Configs:"+repConfig);
        ArrayList<String> allFiles = diskFileExplorer.listeFilesName(repConfig);	
	     
        for (String fileName: allFiles){
     	   System.out.println("   File:"+fileName);
     	   if (fileName.contains(".features")){
     		 
     		  FSTGenComposer fstGen = new FSTGenComposer();
				
				String[] arguments = {"--expression", repConfig+"/" + fileName + "", "--base-directory",repConfig,
						"--output-directory",repOutputGeneratedProducts }; 
				fstGen.run(arguments);
     	  } 
        }
	
	    /*
	     * Generate all valid configurations
	     */
	    
	    
		
		
		
		
		
	}
	
	
	
}