package fr.lip6.meta.splicfh;

import java.io.FileNotFoundException;

import printer.PrintVisitorException;
import cide.gparser.ParseException;
import de.ovgu.cide.fstgen.ast.FSTNode;
import de.ovgu.cide.fstgen.ast.FSTNonTerminal;
import de.ovgu.cide.fstgen.ast.FSTTerminal;

public class UMLLanguage implements InterfaceLanguage {

	@Override
	public boolean isMethod(FSTNode node) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isConstructor(FSTNode node) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public FSTNonTerminal parseFile(String path) throws FileNotFoundException,
			ParseException, PrintVisitorException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void generateCode(FSTNode n, String dirPath, String featName) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isALanguageProgram(String absolutePath) {
		// TODO Auto-generated method stub
		return absolutePath.endsWith(".features") ;
	}

	@Override
	public String getPath() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isImportDec(FSTTerminal terminal) {
		// TODO Auto-generated method stub
		return false;
	}

}
