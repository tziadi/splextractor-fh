package fr.lip6.meta.splicfh;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import composer.FSTGenComposer;

import printer.PrintVisitorException;

import de.ovgu.cide.fstgen.ast.FSTNode;
import de.ovgu.cide.fstgen.ast.FSTNonTerminal;
import de.ovgu.cide.fstgen.ast.FSTTerminal;

import aafst.constraint.Constraint;
import aafst.constraint.Expression;
import artefact.generic.Artefact;







import fr.lip6.meta.ple.featureIdentification.DpFeatureExtraction;
import fr.lip6.meta.ple.featureIdentification.Feature;
import fr.lip6.meta.ple.featureIdentification.NullRootException;
import fr.lip6.meta.ple.featureIdentification.PlFeatureReduceLts;
import fr.lip6.meta.ple.featureIdentification.Product;
import fr.lip6.meta.ple.featureIdentification.SiFeatureExtraction;


import fr.lip6.meta.ple.plextraction.Artefact2File;

import fr.lip6.meta.splicfh.BehaviorFeatureExctraction;
import fr.lip6.meta.splicfh.JavaPrintVisitor;
import fr.lip6.meta.splicfh.SimplePrintVisitor;
import fr.lip6.meta.splicfh.Sop2FST;
import fr.lip6.meta.tools.info.Tracing;
import guidsl.variable;



public class ExtractFSTSol {

       public static Collection<Product> allInputProducts;
       public static Set<Feature> allResultFeatures;

        
        
        public static void extract(ArrayList<Artefact> artfacts,
                        HashMap<String, HashMap<String, ArrayList<Integer>>> body,
                        HashMap<Integer, HashMap<String, ArrayList<HashMap<String, FSTTerminal>>>> bodies_nodes, 
                        String path) throws NullRootException, IOException{

               

         System.out.println("@ SPLIC Step 2 : Product Line Extraction");
  		 System.out.println("@------------");
               

         /*
          * Feature Identification Invocation
          */
  		 	System.out.println("@ SPLIC Step 2 : Product Line Extraction Abstract View");
		    System.out.println("@------------                            -------------");
  		    System.out.println("	    @ SPLIC Step 2.1 : Feature Identification "+artfacts);
		    System.out.println("		@------------");
		    String pathStatic = path+"/abstract_view/";
		    UtiliClassFiles.createRep(pathStatic);
            UtiliClassFiles.createRep(pathStatic+"/sops");
            
		    SiFeatureExtraction sif = new SiFeatureExtraction();
            
            Collection<Feature> si_features = sif.extract(artfacts);
            
            sif.featuresToFile(pathStatic+"features.txt");
            Collection<Product> si_products=sif.getAllP();
            allInputProducts = si_products;
            System.out.println("	    @ SPLIC Step 2.2 : Constraint Identification");
		    System.out.println("		@------------");
//            ConstraintsRulesExatrctionFST ce = new ConstraintsRulesExatrctionFST();
//            Collection<Constraint> constraintsAbstract = ce.extractProductRules(si_features, 
//            		si_products,new HashMap<Feature, HashMap<String, ArrayList<Feature>>>());
//            
		    ConstraintsRulesExatrction ce = new ConstraintsRulesExatrction();
            ArrayList<Constraint> constraintsAbstract = ce.extractImplicationRules(si_features, si_products, 
            		new HashMap<Feature, HashMap<String, ArrayList<Feature>>>());
            System.out.println("	    @ SPLIC Step 2.3 : Feature Model Construction");
		    System.out.println("		@------------");
            
		    GenerateFeatureModelFST fmGenerator = new GenerateFeatureModelFST();
		     fmGenerator.generateFeatureModelStatic(si_features, constraintsAbstract,si_products, pathStatic);
           
            System.out.println("	    @ SPLIC Step 2.4 : Code Generation for Features");
 		    System.out.println("		@------------");
                        
             Sop2FST ss = new Sop2FST();
             ss.toFST(si_features);
             HashMap<Feature,ArrayList<FSTNode>> nodesS = ss.getResult();
        
           
                for(Feature f : si_features){

                		ArrayList<FSTNode> nds = nodesS.get(f);
                		System.out.println("         @Code Generation for Feature :"+f.getId());
                		System.out.println("		                          @-------------------");
                		for (FSTNode n:nds){

                        String featName=f.getId();
                        LanguageConfigurator.getLanguage().generateCode(n,pathStatic,featName);
                     }

                }

        /*
         * Implementation level
         */

        System.out.println("@ SPLIC Step 2 : Product Line Extraction Implementation View");
    		System.out.println("@------------                            -------------------");        
        String pathImpl = path+"/impl_view/";
        UtiliClassFiles.createRep(pathImpl);

        System.out.println("	    @ SPLIC Step 2.1 : Feature Identification/Impl. Viiew");
	    System.out.println("		@------------");
        BehaviorFeatureExctraction bfe = new  BehaviorFeatureExctraction();
        bfe.extract(si_features, body, bodies_nodes);
        HashMap<Feature, HashMap<String, ArrayList<Feature>>> features = bfe.getResult();
        ArrayList<Feature> allFeatures = new ArrayList<Feature>();



        for(Feature ff:features.keySet()){
                 allFeatures.add(ff);
                 HashMap<String, ArrayList<Feature>> cc = features.get(ff);
                 for (String key: cc.keySet())
                 allFeatures.addAll(cc.get(key));
           }

          sif.featuresToFile(pathImpl+"features_impl.txt");




                Sop2FST s = new Sop2FST();
                s.toFST(allFeatures);

                HashMap<Feature,ArrayList<FSTNode>> nodes = s.getResult();
                  featuresToFile(pathImpl + "features_nodes.txt", nodes, sif.getAllP());

            for(Feature f : nodes.keySet()){

            	ArrayList<FSTNode> nds = nodes.get(f);
            	System.out.println("Printing Feature :"+f.getId());
            	for (FSTNode n:nds){

                String featName=f.getId();
             LanguageConfigurator.getLanguage().generateCode(n,pathImpl, featName);
                     }

            }
         allResultFeatures = nodes.keySet();
            
          // ConstraintsRulesExatrctionFST ce2 = new ConstraintsRulesExatrctionFST();
           GenerateFeatureModelFST gfm2 = new GenerateFeatureModelFST();
          // Collection<Constraint>  constraintsBeh = ce2.extractProductRules(allFeatures, si_products,features  );
           
           ConstraintsRulesExatrction ce2 = new ConstraintsRulesExatrction();
         //  Collection<Constraint>  constraintsBeh = ce2.extractImplicationRules(allFeatures, si_products,features  );
			gfm2.generateFeatureModelWithBehavior(features, new ArrayList<Constraint>(), si_products,pathImpl);
           
			
			    
			   
       /*
        * Generate Feature Model au format GUIDSL
        */
	
					
			//		GenerateFeatureModelGuidslFST guidsl = new GenerateFeatureModelGuidslFST();
			 //guidsl = new GenerateFeatureModelGuidslFST ();
		            
		     // guidsl.generateFeatureModelGUIDSL(features, constraintsBeh, si_products,
		                //        pathImpl);
        }
              



        public static void featuresToFile(String fileName, HashMap<Feature,ArrayList<FSTNode>> nodes, Collection<Product> AllP) throws NullRootException,
        IOException {
                        if (nodes == null) throw new NullRootException();

                        FileWriter fw = new FileWriter(fileName);;
                        PrintWriter pw = new PrintWriter(fw);
                        int pSize = AllP.size();

                        for (Feature f: nodes.keySet()){

                                Collection<Integer> p = f.getProdIds();
                                int rate = (p.size() * 100) / pSize;
                                pw.println(f.getId() + " [rate " + rate + "%]: " + p);
                                pw.println(toStringLines(f,nodes.get(f), 1 ));

                        }
           fw.close();
}

        public static String toStringLines(Feature f, ArrayList<FSTNode> nodes ,int leng) {
                String s = "";
                String ind = "";
                for (int i=0; i<leng; i++) {
                        ind += "  ";
                }
                if (f.isEmpty()) return s;
                for (int i = 0; i < nodes.size() - 1; i++) {
                        s += ind + nodes.get(i) + "\n";
                }
                s += ind + nodes.get(nodes.size() - 1);
                return s;
        }

        public static String toStr(Feature f, ArrayList<FSTNode> nodes) {
                return          "Feature [" + "\n"
                                +       "  id=" + f.getId()+ "\n"
                                +       "  prodIds=" + f.getProdIds() + "\n"
                                +       "  triggers=\n" + toStringLines(f,nodes, 2) /*super.toString()*/ + "\n"
                                +       "]";
        }


        
}