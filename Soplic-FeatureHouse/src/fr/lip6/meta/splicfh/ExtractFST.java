package fr.lip6.meta.splicfh;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

import printer.PrintVisitorException;

import de.ovgu.cide.fstgen.ast.FSTNode;
import de.ovgu.cide.fstgen.ast.FSTNonTerminal;
import de.ovgu.cide.fstgen.ast.FSTTerminal;

import aafst.constraint.Constraint;
import aafst.constraint.Expression;
import artefact.generic.Artefact;







import fr.lip6.meta.ple.featureIdentification.DpFeatureExtraction;
import fr.lip6.meta.ple.featureIdentification.Feature;
import fr.lip6.meta.ple.featureIdentification.NullRootException;
import fr.lip6.meta.ple.featureIdentification.PlFeatureReduceLts;
import fr.lip6.meta.ple.featureIdentification.Product;
import fr.lip6.meta.ple.featureIdentification.SiFeatureExtraction;


import fr.lip6.meta.ple.plextraction.Artefact2File;

import fr.lip6.meta.splicfh.BehaviorFeatureExctraction;
import fr.lip6.meta.splicfh.JavaPrintVisitor;
import fr.lip6.meta.splicfh.SimplePrintVisitor;
import fr.lip6.meta.splicfh.Sop2FST;
import fr.lip6.meta.tools.info.Tracing;



public class ExtractFST {

        public static String nameTestCase="";
        public static String[] products;

        public static void extract(ArrayList<Artefact> artfacts, String nameTestCase1, String[] products1,
                        HashMap<String, HashMap<String, ArrayList<Integer>>> body,
                        HashMap<Integer, HashMap<String, ArrayList<HashMap<String, FSTTerminal>>>> bodies_nodes) throws NullRootException, IOException{

                //for each artefact, create an LTS

                nameTestCase=nameTestCase1;
                products=products1;

//              for(Artefact ar : artfacts){
//                      Artefact2File.print(ar, "generatedFiles/"+nameTestCase+"/primitives/"+nameTestCase+"_"+ar.getId());
//          }

         /*
          * Feature Identification Invocation
          */

            SiFeatureExtraction sif = new SiFeatureExtraction();
                Collection<Feature> si_features = sif.extract(artfacts);
                String pathStatic = LanguageConfigurator.getLanguage().getPath()+"/STATIC_LEVEL/";
            sif.featuresToFile(pathStatic+"features.txt");
            Collection<Product> si_products=sif.getAllP();

            ConstraintsRulesExatrction ce = new ConstraintsRulesExatrction();
                ArrayList<Constraint> constraints = ce.extractImplicationRules(si_features, si_products, 
                		new HashMap<Feature, HashMap<String, ArrayList<Feature>>>());
                		
                //Expression constraintStatic = ce.extractProductRules(si_features, si_products,new HashMap<Feature, HashMap<String, ArrayList<Feature>>>());
                               // constraints.add(ce.extractProductRules(si_features, si_products,new HashMap<Feature, HashMap<String, ArrayList<Feature>>>()));
                                                //new HashMap<Feature, HashMap<String, ArrayList<Feature>>>());
                //Generate Feature Model : static

                GenerateFeatureModelFST gfm = new GenerateFeatureModelFST();
                gfm.generateFeatureModelStatic(si_features, constraints, si_products,
                        pathStatic);

                Sop2FST ss = new Sop2FST();
                ss.toFST(si_features);
                HashMap<Feature,ArrayList<FSTNode>> nodesS = ss.getResult();
        
                //Generated assets for static features
        
                for(Feature f : si_features){

                		ArrayList<FSTNode> nds = nodesS.get(f);
                		//System.out.println("Printing Feature :"+f.getId());
                		for (FSTNode n:nds){

                        String featName=f.getId();
                        LanguageConfigurator.getLanguage().generateCode(n,pathStatic,featName);
                     }

                }

        /*
         * Implementation level
         */

        String pathImpl = LanguageConfigurator.getLanguage().getPath()+"/IMPL_LEVEL/";

        BehaviorFeatureExctraction bfe = new  BehaviorFeatureExctraction();
        bfe.extract(si_features, body, bodies_nodes);
        HashMap<Feature, HashMap<String, ArrayList<Feature>>> features = bfe.getResult();
        ArrayList<Feature> allFeatures = new ArrayList<Feature>();



        for(Feature ff:features.keySet()){
                 allFeatures.add(ff);
                 HashMap<String, ArrayList<Feature>> cc = features.get(ff);
                 for (String key: cc.keySet())
                 allFeatures.addAll(cc.get(key));
           }

          sif.featuresToFile(pathImpl+"features_impl.txt");




                Sop2FST s = new Sop2FST();
                s.toFST(allFeatures);

                HashMap<Feature,ArrayList<FSTNode>> nodes = s.getResult();
                  featuresToFile(pathImpl + "features_nodes.txt", nodes, sif.getAllP());

            for(Feature f : nodes.keySet()){

            	ArrayList<FSTNode> nds = nodes.get(f);
            	System.out.println("Printing Feature :"+f.getId());
            	for (FSTNode n:nds){

                String featName=f.getId();
             LanguageConfigurator.getLanguage().generateCode(n,pathImpl, featName);
                     }

            }

           ConstraintsRulesExatrction ce2 = new ConstraintsRulesExatrction();
           GenerateFeatureModelFST gfm2 = new GenerateFeatureModelFST();
           Collection<Constraint>  constraintsBeh = ce2.extractImplicationRules(allFeatures, si_products,features  );
           gfm2.generateFeatureModelWithBehavior(features, constraintsBeh, si_products,
                        pathImpl);
           
       	//System.out.println("Printing GUIDSL FILE @@@@@@@@@@@");
         //  GenerateGuidlModelFST gg = new GenerateGuidlModelFST ();
           //gg.generateGuidlModelWithBehavior(features, constraintsBeh, si_products,
                       // pathImpl);
        }
                //rules extraction

//              ConstraintsRulesExatrction re = new ConstraintsRulesExatrction();
//              System.out.println("############INVOCATION##################");
//              ArrayList<Constraint> rules = re.extractImplicationRules(si_features, si_products);
//              // ProductLineConstruction.generateFeatureModel(si_features,rules);
//              // ProductLineConstruction.construct(si_features,si_products);
//              System.out.println("Generating product");
//
//
//              System.out.println("RULES generated :"+rules.size());
//
//
//
//              /*GnerateXmi r11=*/new GenerateFromXmiFHJava();
//              CompareMethode cmp=new CompareMethode();
//
//              ArrayList<Feature>      clonf = cmp.extract((ArrayList<Feature>) si_features,rules);
//              ArrayList<Constraint> rules1 = cmp.getConstraints();
//              System.err.println("Number of features: "+clonf.size());
//
//
//
//
//              ArrayList<Constraint> rrr=ConstraintsRulesExatrction.normaLizertheConrtaints(rules1);
//
//              //
//              // Gestion of Java extension output method (FeatureHouse, AspectJ, ...)
//              // -->
//              GenerationContext genContext = Configuration.getInstance().getGenerationContext();
//
//              genContext.setProducts(si_products);
//
//              switch(Configuration.getInstance().getExtensionType()) {
//              case AJ:
//                      genContext.construct(clonf, si_products); // (1)
//                      genContext.generateFeatureModel(clonf, rrr); // (2)
//                      break;
//              default:
//                      genContext.generateFeatureModel(clonf, rrr);
//                      genContext.construct(clonf, si_products);
//                      break;
//              }
//              // <--
//              // Gestion of Java extension output method (FeatureHouse, AspectJ, ...)
//              //
//
//              sif.features=clonf;
//
//
//
//
//              try {
//
//
//
//
//                      sif.featuresToFile("generatedFiles/"+nameTestCase+"/"+nameTestCase+"_SI" + "_features.txt");
//              } catch (Exception e) {
//                      // TODO Auto-generated catch block
//                      e.printStackTrace();
//              }








        @SuppressWarnings("unused")
        private static boolean areEquivalent(Collection<Feature> f1, Collection<Feature> f2) {
                if (f1.size() != f2.size()) return false;
                for (Feature f: f1)
                        if (f.getEquivalent(f2) == null) return false;
                return true;
        }


        public static void featuresToFile(String fileName, HashMap<Feature,ArrayList<FSTNode>> nodes, Collection<Product> AllP) throws NullRootException,
        IOException {
                        if (nodes == null) throw new NullRootException();

                        FileWriter fw = new FileWriter(fileName);;
                        PrintWriter pw = new PrintWriter(fw);
                        int pSize = AllP.size();

                        for (Feature f: nodes.keySet()){

                                Collection<Integer> p = f.getProdIds();
                                int rate = (p.size() * 100) / pSize;
                                pw.println(f.getId() + " [rate " + rate + "%]: " + p);
                                pw.println(toStringLines(f,nodes.get(f), 1 ));

                        }
           fw.close();
}

        public static String toStringLines(Feature f, ArrayList<FSTNode> nodes ,int leng) {
                String s = "";
                String ind = "";
                for (int i=0; i<leng; i++) {
                        ind += "  ";
                }
                if (f.isEmpty()) return s;
                for (int i = 0; i < nodes.size() - 1; i++) {
                        s += ind + nodes.get(i) + "\n";
                }
                s += ind + nodes.get(nodes.size() - 1);
                return s;
        }

        public static String toStr(Feature f, ArrayList<FSTNode> nodes) {
                return          "Feature [" + "\n"
                                +       "  id=" + f.getId()+ "\n"
                                +       "  prodIds=" + f.getProdIds() + "\n"
                                +       "  triggers=\n" + toStringLines(f,nodes, 2) /*super.toString()*/ + "\n"
                                +       "]";
        }

private void createRepWithDate(){        
        String format = "dd.MM.yyyy_H.mm.ss";
		java.text.SimpleDateFormat formater = new java.text.SimpleDateFormat( format );
		Date date = new Date(); 
		String sdate=formater.format(date);
		String srep="./Results/Results_"+sdate+"_Pour_";
		File out_rep=new File(srep);
		out_rep.mkdirs();
}
}