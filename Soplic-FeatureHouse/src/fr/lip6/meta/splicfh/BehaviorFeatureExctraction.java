package fr.lip6.meta.splicfh;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;

import artefact.FST.CreateFSTNode;
import artefact.FST.CreateFSTNonTerminal;
import artefact.FST.CreateFSTTerminal;
import artefact.FST.impl.FSTFactoryImpl;
import artefact.generic.ConstructionPrimitive;
import de.ovgu.cide.fstgen.ast.FSTNode;
import de.ovgu.cide.fstgen.ast.FSTTerminal;

import fr.lip6.meta.ple.featureIdentification.Feature;
import fr.lip6.meta.tools.StatementPrimitive;
import fr.lip6.meta.tools.Trigger;

public class BehaviorFeatureExctraction {

	
	
	public HashMap<Feature, HashMap<String, ArrayList<Feature>>> result=
			new HashMap<Feature, HashMap<String, ArrayList<Feature>>>();
	private HashMap<String, Feature> idSubFeatures = new HashMap<String, Feature>();
	
	
	
	
	public HashMap<Feature, HashMap<String, ArrayList<Feature>>> getResult() {
		return result;
	}

	
	
	
	
	public void extract(Collection<Feature> si_features, HashMap<String, HashMap<String, ArrayList<Integer>>> body,
			HashMap<Integer, HashMap<String, ArrayList<HashMap<String, FSTTerminal>>>> bodies_nodes){
		
		
//		System.out.println("BHE Printing body");
//		for(String m:body.keySet()){
//			
//			System.out.println("	BHE Printing body Method: " +m);
//			HashMap<String, ArrayList<Integer>> b = body.get(m);
//			for (String bb:b.keySet()){
//				System.out.println("	    "+bb+ "IN Products "+b.get(bb));
//			}
//		}
		
	
		for(Feature f: si_features){
			
			 System.out.println("  BHE : Traitement Feature "+f.getId());
			  ArrayList<Trigger> toBeRemoved = new ArrayList<Trigger>();
			  ArrayList<Feature> subfeatures = new   ArrayList<Feature>();
			  ArrayList<HashMap<String, ArrayList<Feature>>> allSubTree = 
					  new  ArrayList<HashMap<String, ArrayList<Feature>>>();
			  HashMap<String, ArrayList<Feature>> subTree = new HashMap<String, ArrayList<Feature>>();
			  for(Trigger t: f){
				
                StatementPrimitive st = (StatementPrimitive)t;
                ConstructionPrimitive consPrimitive = st.getPrimitive();
                CreateFSTNode cfst = (CreateFSTNode)consPrimitive;
                FSTNode node = cfst.getNode();
				
                if(node instanceof FSTTerminal ){
                	FSTTerminal nt = (FSTTerminal)node;
  		    	      
                	if (LanguageConfigurator.getLanguage().isMethod(nt)||
                			LanguageConfigurator.getLanguage().isConstructor(nt)) {
                	
  		    	    	System.out.println("       BHE : Traitement Method "+nt.getName());
  		    		   if (!sameCodeInAllProducts(f,nt.getName(), body )){
  		    		      System.out.println("              CE N EST PAS LE MEME CODE de :"+nt.getName());
                	     // if(f.size()>1) {
                	    	  String methodName = nt.getName();
                	    	  subfeatures = createSubFeatures(f,methodName, body, t, bodies_nodes);
                	          toBeRemoved.add(t);
                	          //find the name of the abstract feature
                	          subTree.put(this.handleNameFeature(methodName), subfeatures);
                	          
                	      //}
                  }
          
				
			}
			
			
		}
	  }
	        for(Trigger tt : toBeRemoved){
	        	      f.remove(tt);
	        }
			if (f.isEmpty())
				f.setAbst(true);
	        result.put(f, subTree);
	}	
		
		
}
	
	
	private static String deleteteComments(String test) {
		String[] result = test.split("\n");
		String chaine="";
		int x=0;
	    while (x<result.length){
	       	
	    	//System.out.println("Element :"+x + " :"+result[x]);
	    	
	    	if (result[x].startsWith("/*")) {
	    	    x++;	
	    		while ((x<result.length)&& !result[x].startsWith("*/")){
	    			x++;
	    		}
	    		x++;
	    	}
	    	if ((x<result.length) &&!result[x].startsWith("\\\\")) {
	          chaine=chaine+result[x]+"\n";
	         
	    	}
	    	x++;
	    //	System.out.println("Iteration :"+x+ "  "+ chaine);
	    }
	      
	        return chaine.replaceAll("\\s", "");
		
		
	}
	
	
	private String handleNameFeature(String n) {
		String name;
		name = n;
		name =name.replace("(", "_");
		name =name.replace(")", "_");
		name =name.replace("{", "_");
		name =name.replace("}", "_");
		name =name.replace("-", "_");
		name =name.replace("*", "_");
		name =name.replace(",", "_");
		name =name.replace("[", "_");
		name =name.replace("]", "_");
		name =name.replace("*", "_");
		name =name.replace("<", "_");
		name =name.replace(">", "_");
		
		
		
		return name;
	}
	

	private ArrayList<Feature> createSubFeatures(Feature f,
			String methodName,
			HashMap<String, HashMap<String, ArrayList<Integer>>> body, Trigger t, 
			HashMap<Integer, HashMap<String, ArrayList<HashMap<String, FSTTerminal>>>> bodies_nodes) {
		 
		int i=0;
		 ArrayList<Feature> result = new  ArrayList<Feature>();
		 HashMap<String, ArrayList<Integer>> bodiesOfTheMehod = body.get(methodName);
		
		 for(String bodyOfTheMethod: bodiesOfTheMehod.keySet()){
			 //get node from bodies_nodes
			HashMap<String, ArrayList<HashMap<String, FSTTerminal>>> 
			 //take the fisrt IdProduct
			 bn = bodies_nodes.get(bodiesOfTheMehod.get(bodyOfTheMethod).get(0));
			 ArrayList<HashMap<String, FSTTerminal>> bnn = bn.get(bodyOfTheMethod);
			 HashMap<String, FSTTerminal> h =null;
			 if (bnn!=null){
			 for (HashMap<String, FSTTerminal> hbnn: bnn){
				 if (hbnn.containsKey(bodyOfTheMethod)){
					 h=hbnn;
					 break;
					 
				 }
			 }
			 }
			 if (h!=null){
				 
				 FSTTerminal nodeTerminal = h.get(bodyOfTheMethod);
				 FSTFactoryImpl factory = new FSTFactoryImpl();
				 CreateFSTTerminal createT = factory.createCreateFSTTerminal();
			   	
				 createT.setName(nodeTerminal.getName());
				 createT.setType(nodeTerminal.getType());
				 createT.setFSTNode(nodeTerminal);
				// System.out.println("CreateFSTTerminal("+createT.toString()+")");

				 //create a new sub-feature of "f"
				 Feature newF = new Feature();
				 String nameNewF = this.handleNameFeature(methodName)+"_v"+i;
				 newF.setId(nameNewF);
				 newF.getProdIds().addAll(bodiesOfTheMehod.get(bodyOfTheMethod));
				 
				 StatementPrimitive st =  new StatementPrimitive(createT);
				 
				 newF.add(st);
				 result.add(newF);
				 this.idSubFeatures.put(nameNewF, newF);
				 System.out.println("                 Creating a new SubFeature "+newF.getId());
				 i++;
				 
			 }
			// System.out.println("        "+newF.toString());
		
		 }
		 
		 return result;
	}

	private void handleAbstractFeatures(){
		
		for (Feature f: result.keySet()){
			
			if (f.isAbst()){
				
				
			}
		}
		
	}
	private boolean sameCodeInAllProducts(Feature f, String nt,
		
			HashMap<String, HashMap<String, ArrayList<Integer>>> body) {
			LinkedHashSet<Integer> allP=f.getProdIds();
			HashMap<String, ArrayList<Integer>> allBody = body.get(nt);
			
			return allBody.keySet().size()==1;
		
			

	
	  }



}
