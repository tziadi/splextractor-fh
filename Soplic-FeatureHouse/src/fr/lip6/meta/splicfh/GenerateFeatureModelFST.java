package fr.lip6.meta.splicfh;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import aafst.constraint.Constraint;
import aafst.constraint.Expression;
import artefact.FST.CreateFSTNode;
import artefact.generic.ConstructionPrimitive;
import de.ovgu.cide.fstgen.ast.FSTNode;
import de.ovgu.cide.fstgen.ast.FSTTerminal;


import fr.lip6.meta.ple.featureIdentification.Feature;
import fr.lip6.meta.ple.featureIdentification.Product;

import fr.lip6.meta.tools.PLConstructionUtil;
import fr.lip6.meta.tools.StatementPrimitive;
import fr.lip6.meta.tools.Trigger;
import fr.lip6.meta.tools.info.Tracing;

public class GenerateFeatureModelFST  {
	private ArrayList<Feature> abstractFeatures = new ArrayList<Feature>();
	private static boolean indentXMLModel = true;
	
	public void generateFeatureModelStatic(Collection<Feature> features, Collection<Constraint> rules, 
			Collection<Product> products, String path) {
		
		
		PrintStream out;
		String rep1 = path;
		File repit = new File (rep1);
		IndentHandler indentHandler;
		String xml = "";
	    repit.mkdirs();
        File dotFile = new File(rep1+"/abstract_model.xml");
		FileOutputStream fout;

		try {
			//System.out.println("Generating Feature Model STATIC");
			fout = new FileOutputStream(dotFile);
			out = new PrintStream(fout);
			indentHandler = new IndentHandler(out);

			xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>"+"\n";
			xml += "<featureModel>"+"\n";
			xml += "<struct>"+"\n";
			xml += "<and abstract=\"true\" mandatory=\"true\" name=\"SPLConstruction\">"+"\n";
			//	xml += "<feature mandatory=\"true\" name=\"F0\"/>";

			for (Feature currentFeature : features) {
				
				
				boolean isCurrentFeatMandatory = PLConstructionUtil.isMandatory(currentFeature, products);

				
					ArrayList<Feature> allSubFeatures = new ArrayList<Feature>();
                  
					if (isCurrentFeatMandatory) {
						xml += "<and mandatory=\"true\" name=\""+currentFeature.getId()+"\""+" >"+"\n";
					}
					else {
						xml += "<and name=\""+currentFeature.getId()+"\""+" >"+"\n";
					}
					
					
					
					if (allSubFeatures.size() > 0) {
						//String nameFunction = getNameMethod(allSubFeatures.get(0));
						
						
						String nameFunction = this.getnameAbstractFeature(allSubFeatures.get(0));
						xml += 
								"<alt abstract=\"true\" "
							+	"mandatory=\"true\" " 
							+	"name=\""+nameFunction+"\" >"+"\n";
						
						
						
						for(Feature child : allSubFeatures) {
							xml += "<feature "
										+	"mandatory=\"true\" "
										+	"name=\""+this.getnameAbstractFeature(child)+"\" />"+"\n";
							}
						 xml += "</alt>"+"\n";
						}
						
					
					xml += "</and>"+"\n";	
					}
				
			

			xml += "</and>"+"\n";//la racine
			xml += "</struct>"+"\n";
			xml += "<constraints>"+"\n";

			
			for(Constraint rule : rules){
				xml += "<rule>"+"\n";
				
				
				if (rule!=null){
				
					if (rule.getClass().getName().equals("Or")){
					
						xml +=rule.toString();
					}
					else {
						xml += "<imp>"+"\n";
		
						xml +=rule.getLeft().toXML()+"\n";
						
						xml += rule.getRight().toXML()+"\n";
						xml += "</imp>"+"\n";
				}
					xml += "</rule>"+"\n";
			}
			}
				


			xml += "</constraints>"+"\n";
			xml += "<comments/>"+"\n";

			xml += "</featureModel>";
			
			// indentation of the feature model xml code
			//if (indentXMLModel)
				//indentHandler.indentXml(xml, out);
			//else
				out.print(xml);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
	
	

	
	
	
	//generate Feature model behavior
	public void generateFeatureModelWithBehavior(HashMap<Feature, HashMap<String, ArrayList<Feature>>> features, 
			Collection<Constraint> rules, 
			Collection<Product> products, String path) {
		
		//System.out.println("Generating Feature Model..");
		PrintStream out;
		String rep1 = path;
		File repit = new File (rep1);
		IndentHandler indentHandler;
		String xml = "";
	    repit.mkdirs();
        File dotFile = new File(rep1+"/model.xml");
		FileOutputStream fout;

		try {
			System.out.println("Generating Feature Model Behavior");
			
			
              
            			
			
			fout = new FileOutputStream(dotFile);
			out = new PrintStream(fout);
			indentHandler = new IndentHandler(out);

			xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>"+"\n";
			xml += "<featureModel>"+"\n";
			xml += "<struct>"+"\n";
			xml += "<and abstract=\"true\" mandatory=\"true\" name=\"SPLConstruction\">"+"\n";
			//	xml += "<feature mandatory=\"true\" name=\"F0\"/>";

			for (Feature currentFeature : features.keySet()) {
				
				//System.out.println("  CurrentFeature :"+currentFeature.getId());
				boolean isCurrentFeatMandatory = PLConstructionUtil.isMandatory(currentFeature, products);

				
					HashMap<String, ArrayList<Feature>> allSubFeatures = features.get(currentFeature);

					if (isCurrentFeatMandatory) {
						xml += "<and mandatory=\"true\" name=\""+this.handleNameFeature(currentFeature.getId())+"\""+" >"+"\n";
					}
					else {
						xml += "<and name=\""+this.handleNameFeature(currentFeature.getId())+"\""+" >"+"\n";
					}
					
					
					
					for (String abstractFeature: allSubFeatures.keySet()) {
						//String nameFunction = getNameMethod(allSubFeatures.get(0));
						
						//System.out.println("        abstract feature :"+abstractFeature);
						xml += 
								"<alt abstract=\"true\" "
							+	"mandatory=\"true\" " 
							+	"name=\""+this.handleNameFeature(abstractFeature)+"\" >"+"\n";
						 for(Feature child : allSubFeatures.get(abstractFeature)) {
							 
							 System.out.println("            child feature :"+child.getId());
							xml += "<feature "
										+	"mandatory=\"true\" "
										+	"name=\""+this.handleNameFeature(child.getId())+"\" />"+"\n";
							}
						 xml += "</alt>"+"\n";
						}
						
					
					xml += "</and>"+"\n";	
					}
				
			

			xml += "</and>"+"\n";//la racine
			xml += "</struct>"+"\n";
			xml += "<constraints>"+"\n";

			for(Constraint rule : rules){
				xml += "<rule>"+"\n";
				
				
				if (rule!=null){
				
					if (rule.getClass().getName().equals("Or")){
					
						xml +=rule.toString();
					}
					else {
						xml += "<imp>"+"\n";
		
						xml +=rule.getLeft().toXML()+"\n";
		
						xml += rule.getRight().toXML()+"\n";
						xml += "</imp>"+"\n";
				}
					xml += "</rule>"+"\n";
			}
			}
				
       			xml += "</constraints>"+"\n";
		     	xml += "<comments/>"+"\n";
			xml += "</featureModel>";
			
			// indentation of the feature model xml code
			//if (indentXMLModel)
				//indentHandler.indentXml(xml, out);
			//else
				out.print(xml);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
	
	
	private String getnameAbstractFeature(Feature child){
		
		String result ="";
		String name="";
		if (child.size()==1){
			
			 StatementPrimitive st = (StatementPrimitive)child.get(0);
             ConstructionPrimitive consPrimitive = st.getPrimitive();
             CreateFSTNode cfst = (CreateFSTNode)consPrimitive;
             FSTNode node = cfst.getNode();
		 		
             if(node instanceof FSTTerminal ){
             	FSTTerminal nt = (FSTTerminal)node;
             	 name = handleNameFeature(nt.getName());
             	
             }
             
		}
		result=name;
		return result;
	}




	private String handleNameFeature(String n) {
		String name;
		name = n;
		name =name.replace("(", "_");
		name =name.replace(")", "_");
		name =name.replace("{", "_");
		name =name.replace("}", "_");
		name =name.replace("-", "_");
		name =name.replace("*", "_");
		name =name.replace(",", "_");
		
		name =name.replace(">", "_");
		name =name.replace("<", "_");
		
		
		return name;
	}
	
	private String getNameMethod(Feature child) {
		
		
		String result ="";
		if (child.size()==1){
			
			 StatementPrimitive st = (StatementPrimitive)child.get(0);
             ConstructionPrimitive consPrimitive = st.getPrimitive();
             CreateFSTNode cfst = (CreateFSTNode)consPrimitive;
             FSTNode node = cfst.getNode();
		 		
             if(node instanceof FSTTerminal ){
             	FSTTerminal nt = (FSTTerminal)node;
		    	    
             	
             	
             	
             	if (LanguageConfigurator.getLanguage().isMethod(nt)||
             			LanguageConfigurator.getLanguage().isConstructor(nt)) {
             		String n = nt.getName();
             		String[] nlist = n.split("\\(");
             		result=nlist[0];
             	}
             }
		}
		return result;
	}

	public static void deleteRecursive(File f)  {
		if (f.exists()){
			if (f.isDirectory()){
				File[] childs = f.listFiles();
				int i = 0;
				for(i = 0; i<childs.length; i++){
					deleteRecursive(childs[i]);
				}

			}
			f.delete();
		}
		
		
		
	}
	
	
	
	/**
	 * @author Simon Grandsire
	 * Indents XML code
	 */
	public class IndentHandler extends DefaultHandler {
		protected String indent;
		protected PrintStream out;
		protected boolean varTag = false;

		public IndentHandler (PrintStream out) {
			indent = "";
			this.out = out;
		}

		public void startDocument () throws SAXException {
			out.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
		}

		public void startElement (String uri, String localName, String qName,
				Attributes attributes) throws SAXException {
			
			out.print(indent + "<" + qName);
			
			for (int i=0, lg=attributes.getLength(); i<lg; ++i)
				out.print(" " + attributes.getQName(i) + "=\"" + attributes.getValue(i) + "\"");
			
			if (qName.equalsIgnoreCase("feature")) {
				out.println(" />");
			} else {
				if (qName.equalsIgnoreCase("var"))
					out.print(">");
				else
					out.println(">");
			}
			
			indent += "\t";
		}

		public void endElement (String uri, String localName, String qName)
				throws SAXException {
			
			indent = indent.substring(0, indent.length() - 1);
			
			if (!qName.equalsIgnoreCase("feature")) {
				if (qName.equalsIgnoreCase("var"))
					out.println(/*indent + */"</" + qName + ">");
				else
					out.println(indent + "</" + qName + ">");
			}
		}

		public void characters (char[] ch, int start, int length)
				throws SAXException {
			
			String s = new String(ch, start, length);
			
			if (!s.matches("^\\s*$"))
				out.print(/*indent + */s.trim());
		}

		public void fatalError (SAXParseException e) throws SAXException {
			System.err.println("\n***** Erreur fatale à la ligne " +
					e.getLineNumber() + ", \tau colonne " +
					e.getColumnNumber());
		}

		public void error (SAXParseException e) throws SAXException {
			System.err.println("\n***** Erreur non fatale à la ligne " +
					e.getLineNumber() + ", \tau colonne " +
					e.getColumnNumber());
		}
		
		public void indentXml (String xml, PrintStream out) {
			IndentHandler handler = new IndentHandler(out);

			try {
				XMLReader xmlReader = XMLReaderFactory.createXMLReader();

				xmlReader.setContentHandler(handler);
				xmlReader.setErrorHandler(handler);

		        InputStream xmlStream = new ByteArrayInputStream(xml.getBytes("UTF-8"));
		        xmlReader.parse(new InputSource(xmlStream));
			} catch (SAXException e) {
				System.err.println("Problème(s) SAX : " + e.getMessage());
				System.exit(0);
			} catch (IOException e) {
				System.err.println("Problème(s) d'entrée : " +
						e.getMessage());
				System.exit(0);
			}
		}
	}

}
