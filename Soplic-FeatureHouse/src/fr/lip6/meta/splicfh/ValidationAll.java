package fr.lip6.meta.splicfh;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import artefact.FST.CreateFSTNonTerminal;
import artefact.FST.CreateFSTTerminal;
import artefact.generic.Artefact;
import artefact.generic.ConstructionPrimitive;
import de.ovgu.cide.fstgen.ast.FSTTerminal;
import fr.lip6.meta.ple.configsgenerator.ConfigsGenerator;
import fr.lip6.meta.ple.featureIdentification.Feature;
import fr.lip6.meta.ple.plextraction.Artefact2File;

public class ValidationAll {
       public static HashMap<Integer, String> productPathOutput = new HashMap<Integer, String>();
       public static HashMap<Integer, String> productPathInputput = new  HashMap<Integer, String>();
       public static HashMap<Integer, ArrayList<Integer>> equivalance = new HashMap<Integer, ArrayList<Integer>> ();
        
       public static void main(String[] args){
    	   
    	   // ConfigsGenerator.generateConfigs({"./model.m"});
       }
       
       public static void validate(){

               
    	   
    	   
    	   			System.out.println("Validation result :"+
                validateProducts("./Validation/input/", "./Validation/output/"));
                //printing  result in a file;
                FileWriter fw;
				try {
					//System.out.println("Prinintg evaluation file");
					fw = new FileWriter("./Validation/evaluationResult.txt");
					PrintWriter pw = new PrintWriter(fw);
	    			  
	    			    for (Integer i: equivalance.keySet()){
	    			    	
	    			    			
	    			    			
	    			    			String left = "input/"+productPathInputput.get(i);
	    			    			ArrayList<Integer> sim = equivalance.get(i);
	    			    			String right ="[";
	    			    			for(Integer j: sim){
	    			    				right=right+"output/"+productPathOutput.get(j)+",";
	    			    			}
	    			    			 pw.println (left+ "  IS EQUIVALAENT TO  :"+right+"]");
	    				    }
	    			    fw.close();
	    			    
				} catch (IOException e) {
					
					e.printStackTrace();
				}
    				
                
        }

        public static boolean validateProducts(String pathInputProducts, String pathOutputProducts){

                ArrayList<Artefact> inputs = readInputProducts(pathInputProducts, 8, productPathInputput);
//                for(Artefact ar : inputs){
//        			Artefact2File.print(ar, pathInputProducts+"/primitives/"+ar.getId());
//                  }
                ArrayList<Artefact> outputs = readInputProducts(pathOutputProducts, 8, productPathOutput);
                
//                for(Artefact ar : outputs){
//        			Artefact2File.print(ar, pathOutputProducts+"/primitives/"+ar.getId());
//                  }


                System.out.println("NP Products Inputs :"+inputs.size());
                System.out.println("NP Products Output :"+outputs.size());

                for (Artefact a: inputs){

                        if (!existArtefact(a, outputs)){
                                //System.out.println("NonEqual :"+a);
                        	
                                return false;
                        }

                }

                return true;
        }

        private static boolean existArtefact(Artefact a, ArrayList<Artefact> outputs) {
                ArrayList<Integer> res= new ArrayList<Integer>();
                for( Artefact ar:outputs){
                        if (equalsArtefact(a,ar)){
                        	   res.add(ar.getId());
                                
                        }
                        else {
                        ArrayList<ConstructionPrimitive> diff = diff(a, ar);
                       	 
                       	 display(diff);
                       	
                        }
                }
                
                if (!res.isEmpty()) {
               
                		equivalance.put(a.getId(), res);
                		System.out.println("res "+res);
                		return true;
                	}
             	
                else 
                return false;
        }

        private static void display(ArrayList<ConstructionPrimitive> diff) {
        
        	for (ConstructionPrimitive p : diff){
        		if(p instanceof CreateFSTNonTerminal) {
        			CreateFSTNonTerminal nt = (CreateFSTNonTerminal)p;
                
                System.out.println("        Diff CP :  "+nt.getName() +nt.getType());
        		}
        		if(p instanceof CreateFSTTerminal) {
        			CreateFSTTerminal nt = (CreateFSTTerminal)p;
       
              System.out.println("        Diff CP :  "+nt.getName() +nt.getType());
      
       
              }
        		
        	}
        	
			
		}

		private static ArrayList<ConstructionPrimitive> pcOfArtefact(Artefact a){

                ArrayList<ConstructionPrimitive> result = new ArrayList<ConstructionPrimitive>();
                
                
                ConstructionPrimitive first = a.getFirst();
                
                if (first!=null ) {
                	
                	//if (first instanceof CreateFSTNonTerminal
                			
                			
                			result.add(first);
                }
                ConstructionPrimitive next = first.getNext();
                if (next!=null) result.add(next);

                while(next != null) {
                        next = next.getNext();
                        result.add(next);
                }

                
                return result;
        }

        private static ArrayList<ConstructionPrimitive> diff(Artefact a1, Artefact a2){
        	
        	 ArrayList<ConstructionPrimitive> result = new  ArrayList<ConstructionPrimitive>();
        	
            ArrayList<ConstructionPrimitive> pc1 = pcOfArtefact(a1);
            ArrayList<ConstructionPrimitive> pc2 = pcOfArtefact(a2);
           

            for(ConstructionPrimitive p: pc1){
                     
            	  if (p!=null && pc2!=null){
            		  if (!existConstructionPrimitive(p, pc2)){

                              result.add(p);

                      }
            	  }
            }
        	
        	return result;
        	
        }
        
        private static boolean equalsArtefact(Artefact a, Artefact ar) {

                System.out.println("Compraraison :  "+a.getId() + " et "+ar.getId());
                ArrayList<ConstructionPrimitive> pc1 = pcOfArtefact(a);
                System.out.println("                    Artefact :  "+a.getId() + " est de taille ="+pc1.size());


                ArrayList<ConstructionPrimitive> pc2 = pcOfArtefact(ar);
                System.out.println("                    Artefact :  "+ar.getId() + " est de taille ="+pc2.size());

//                if (pc1.size()!=pc2.size())
//                        return false;

                for(ConstructionPrimitive p: pc1){
                         
                	  if (p!=null && pc2!=null){
                		  if (!existConstructionPrimitive(p, pc2)){

                                  System.out.println("                        Compraraison :  "+a.getId() + " et "+ar.getId()+ " FALSE");
                                  return false;

                          }
                	  }
                }

                
                return true;
        }

        private static boolean existConstructionPrimitive(ConstructionPrimitive p,
                        ArrayList<ConstructionPrimitive> pc2) {
        	
          if (p!=null && pc2!=null){	
        	    for(ConstructionPrimitive pp: pc2){
                        if (pp!=null && p!=null) {
                                if (equalsConstructionPtimitive(p, pp)){

                                        return true;
                                }


                        }
                }
               
          }
                return false;
        }

        private static boolean equalsConstructionPtimitive(ConstructionPrimitive p,
                        ConstructionPrimitive pp) {

                if (p == pp)
                        return true;
                else if((p.getClass() != pp.getClass()))
                        return false;
                else {
                if((p instanceof CreateFSTNonTerminal) &&
                                (pp instanceof CreateFSTNonTerminal)){
                   CreateFSTNonTerminal nt = (CreateFSTNonTerminal)p;
                   CreateFSTNonTerminal nt2 = (CreateFSTNonTerminal)pp;
                  // System.out.println("        Equal CP :  "+nt.getName() +nt.getType()+" et "+ nt2.getName()+nt2.getType());
                   return nt.getName().equals(nt2.getName())&&nt.getType().equals(nt2.getType());
                   }

               if((p instanceof CreateFSTTerminal) &&
                                (pp instanceof CreateFSTTerminal)){
                   CreateFSTTerminal nt = (CreateFSTTerminal)p;
                   CreateFSTTerminal nt2 = (CreateFSTTerminal)pp;
                   //System.out.println("        Equal CP :  "+nt.getName() +nt.getType()+" et "+ nt2.getName()+nt2.getType());
                  
                   return nt.getName().equals(nt2.getName())&&nt.getType().equals(nt2.getType());
               }

        }
                return false;
        }
        private static ArrayList<Artefact> readInputProducts(
                        String pathInputProducts, int nb, HashMap<Integer, String> hash) {

                ArrayList<Artefact> artefacts =  new ArrayList<Artefact>();
                LanguageConfigurator.LANGUAGE=new CLanguage();
            String p;
                for (int j=1; j<=nb; j++) {
                        if (j<110)
                              p= "Variant000"+j;
                                else {

                                        if (j<100)
                                                p= "Variant00"+j;
                                        else
                                                p= "Variant0"+j;
                                }

                        try {

                                System.out.println("Reading Input Products");
                                ReadFSTProduct rp1 = new ReadFSTProduct();
                                rp1.readProduct(j,pathInputProducts+"/"+p);
                                artefacts.add(rp1.getArtefact());
                                hash.put(j, p);
                                
                        } catch (Exception e) {
                                e.printStackTrace();
                        }

                }

                return artefacts;

        }




}