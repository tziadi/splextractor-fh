package fr.lip6.meta.splicfh;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import tmp.generated_java15.Java15Parser;
import cide.gparser.OffsetCharStream;
import cide.gparser.ParseException;
import de.ovgu.cide.fstgen.ast.FSTNonTerminal;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
	Java15Parser p;
	
		try {
			p = new Java15Parser(new OffsetCharStream( new FileInputStream("Account.java")));
			
			p.CompilationUnit(false);
			FSTNonTerminal racine = (FSTNonTerminal)p.getRoot();
			
			SimplePrintVisitor spv = new SimplePrintVisitor();
			spv.visit((FSTNonTerminal)p.getRoot());
			System.out.println(p.getRoot());
			
			String test = "public deposit(double-double)  {\n";
			test=test+"\n";
			test=test+"\n";
			
	
			test =test+"//commentaire deposit\n";
			test=test+"\n";
			test=test+"this.balance += amount;\n";
			
			test=test+"\n";
			test=test+"/*\n";
			test=test+"test test test \n";
			test=test+"tetste tetetetetet ette\n";
			test=test+"*/\n";
			
			test=test+ "hhh;\n";
			test =test+"    // tetststst\n";
			
			test=test+"}";
			
			String c1 = deleteteComments(test);
			//test="";
			test = "public deposit(double-double)  {\n";
			test=test+"\n";
			
			test=test+"this.balance += amount;\n";
			
			
			test=test+ "hhh;\n";
			
			test=test+"}";
			
			String c2 = deleteteComments(test);
			System.out.println("TEST C1 \n"+c1);
			
		
			
			System.out.println("Equals :"+c1.equals(c2));
			
			//test =(test.length() != 0 ? test.replaceAll("\\s", " ") : "");
			//test.replaceAll("\\/\\/", "");
			
			 //System.out.println("AllElement :"+test);

		    // for (int x=0; x<result.length; x++)
		      //   System.out.println("Element :"+x + " :"+result[x]);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	
	}

	private static String deleteteComments(String test) {
		String[] result = test.split("\n");
		String chaine="";
		int x=0;
	    while (x<result.length){
	       	
	    	//System.out.println("Element :"+x + " :"+result[x]);
	    	
	    	if (result[x].startsWith("/*")) {
	    	    x++;	
	    		while ((x<result.length)&& !result[x].startsWith("*/")){
	    			x++;
	    		}
	    		x++;
	    	}
	    	if ((x<result.length) &&!result[x].replaceAll("\\s",  "").startsWith("//")) {
	          chaine=chaine+result[x]+"\n";
	         
	    	}
	    	x++;
	    	//System.out.println("Iteration :"+x+ "  "+ chaine);
	    }
	      return chaine;
	      //return chaine.replaceAll("\\s", " ");
		
		
	}

}
