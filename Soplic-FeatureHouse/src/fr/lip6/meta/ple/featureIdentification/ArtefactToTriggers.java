package fr.lip6.meta.ple.featureIdentification;

import java.util.ArrayList;
import java.util.List;

import artefact.generic.Artefact;
import artefact.generic.ConstructionPrimitive;

import fr.lip6.meta.tools.StatementPrimitive;
import fr.lip6.meta.tools.Trigger;

public class ArtefactToTriggers {
	
	static List<Trigger> extract(Artefact artefact) {
		
		ArrayList<Trigger> result = new ArrayList<Trigger>();
		
		ConstructionPrimitive first = artefact.getFirst();
		StatementPrimitive st = new StatementPrimitive(first);
		//if (!(st.toString().contains("$$")))
	    result.add(st);
  
		ConstructionPrimitive next = first.getNext();

		while(next!=null){
			st = new StatementPrimitive(next);
			
			//if (!(st.toString().contains("$$")))
				result.add(st);
			next = next.getNext();
		}
		
		return result;
	}
	
}
