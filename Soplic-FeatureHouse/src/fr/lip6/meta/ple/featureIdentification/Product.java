package fr.lip6.meta.ple.featureIdentification;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import fr.lip6.meta.tools.Trigger;

public class Product extends ArrayList<Trigger> {

	
	
	
	/**
	 * For avoiding warnings
	 */
	private static final long serialVersionUID = 4L;
	
	/**
	 * Product id
	 */
	private int id;
	
	/**
	 * Constructor
	 * @param c - collection of triggers
	 * @param id - id
	 */
	public Product(Collection<? extends Trigger> c, int id) {
		super(c);
		this.id = id;
	}
	
	/**
	 * Constructor
	 * @param id - id
	 */
	public Product(int id) {
		super();
		this.id = id;
	}

	/**
	 * ID getter
	 * @return id
	 */
	public int getId() {
		return id;
	}

	
	
	public List<Feature> featuresImplemented(Collection<Feature> features) {
		ArrayList<Feature> productFeatures = new ArrayList<Feature>();
		for (Feature f : features)
			if (f.getProdIds().contains(id))
				productFeatures.add(f);
		return productFeatures;
	}
	
	public String featuresImplementedString(Collection<Feature> features) {
		String s = "";
		for (Feature f : featuresImplemented(features))
			s += f.getId() + " ";
		return s;
	}
	
	/**
	 * Removes the repetitions of a trigger
	 */
	public void removeRepeated() {
		for (int i = 0; i < size() - 1; i++)
			for (int j = i + 1; j < size(); j++){
				if (get(i).equals(get(j)))
					remove(j);
			}
	}
	
	public String toString() {
		return id + ": " + super.toString();
	}
	
	public Feature intersection(Feature feature) {
		@SuppressWarnings("unchecked")
		Collection<Trigger> copy = (Collection<Trigger>) super.clone();
		copy.retainAll(feature);
		
		Feature f = new Feature(copy);
		
		Collection<Integer> ids = f.getProdIds();
		ids.add(id);
		ids.addAll(feature.getProdIds());
		
		return f;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Product))
			return false;
		Product other = (Product) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
	
}
