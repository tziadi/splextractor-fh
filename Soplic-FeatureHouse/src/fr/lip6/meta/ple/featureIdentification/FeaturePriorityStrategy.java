package fr.lip6.meta.ple.featureIdentification;

import java.util.Collection;

import fr.lip6.meta.tools.Trigger;

/**
 * Design Pattern Strategy for choosing a set of common triggers to merge
 * @author Luz
 *
 */
public interface FeaturePriorityStrategy {
	
	/**
	 * Returns the chosen set of triggers to merge
	 * @param scen - the collection of triggers to study
	 * @return a collection of triggers
	 */
	public Collection<Trigger> chooseFeatureToMerge(Collection<? extends Feature> feature);
	
}
