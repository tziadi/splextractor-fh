package fr.lip6.meta.ple.featureIdentification;

public class NullRootException extends Exception {
	/**
	 * For avoiding warnings
	 */
	private static final long serialVersionUID = 1L;

	public NullRootException () {
		super("Null root. Have you called transform() with a valid LTS?");
	}
}
