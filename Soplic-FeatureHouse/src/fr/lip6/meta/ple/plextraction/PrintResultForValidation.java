package fr.lip6.meta.ple.plextraction;

import java.io.BufferedReader;
//import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
//import java.util.List;
//import java.util.Map;

//import org.eclipse.emf.ecore.EObject;

import fr.lip6.meta.ple.featureIdentification.Feature;
import fr.lip6.meta.ple.featureIdentification.Product;
//import fr.lip6.meta.ple.featureIdentification.SiFeatureExtraction;

public class PrintResultForValidation {

	
	
	public void printInitialFile(Collection<Feature> features, String nameTestCase) throws IOException{
		
		FileWriter fw = new FileWriter("generatedFiles/"+nameTestCase+"/"+nameTestCase+"MapResult.txt");
		 PrintWriter pw = new PrintWriter(fw);
		 
		 
		 for (Feature f : features){
			 
			 pw.println(f.getId()); 
		 }
		 fw.close();
	}
	
	public void printResult(Collection<Product> AllP, Collection<Feature> features,  String nameTestCase) throws IOException{
		
		
		
		
		FileWriter fw = new FileWriter("generatedFiles/"+nameTestCase+"/"+nameTestCase+"outputProduct.txt");
		 PrintWriter pw = new PrintWriter(fw);
		 
		 
		 
		
		for (Product prod : AllP) {
			
			pw.print("c_"+prod.getId()+ " :" );
			for (Feature f : prod.featuresImplemented(features)) {
				 
				
				pw.print(f.getId()+ "   ");
			}
			pw.println("");
		}
		
		 fw.close();
       
		
		
		
	}
	
	private ArrayList<String>  printFile(String f){
		
		ArrayList<String> tabs = new ArrayList<String>();
		//lecture du fichier texte	
		try{
			InputStream ips=new FileInputStream(f); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String ligne;
			while ((ligne=br.readLine())!=null){
				System.out.println(ligne);
				tabs.add(ligne);
			}
			br.close(); 
		}		
		catch (Exception e){
			System.out.println(e.toString());
		}
		return tabs;
	}
	
	public void printConfiguration(String nameTestCase) throws IOException{
		
		FileWriter fw = new FileWriter("generatedFiles/"+nameTestCase+"/"+nameTestCase+"inputProduct.txt");
		 PrintWriter pw = new PrintWriter(fw);
		
		ArrayList<String> tab=null;
		
		for (int i=1; i<=219; i++)
		{
			String p ="c"+i;
			String fileName = "GPL219/generatedConfs/c"+i+".config";
			//File f = new File(fileName);
			tab=printFile(fileName);
			pw.print(p+ " : ");
			for(String st:tab){
			    pw.print(st + "  ");	
				
			}
			pw.println("");
		}
		 fw.close();       
	}
	

}
