package fr.lip6.meta.ple.plextraction;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import artefact.FST.CreateFSTNonTerminal;
import artefact.FST.CreateFSTTerminal;
//import meta.lip6.fr.tests.ExampleClassDiagramCreation;
import artefact.generic.Artefact;
import artefact.generic.ConstructionPrimitive;
import artefact.umlClassDiagram.CreateAssociation;
import artefact.umlClassDiagram.CreateAttribute;
import artefact.umlClassDiagram.CreateClass;
import artefact.umlClassDiagram.CreateGeneralization;
import artefact.umlClassDiagram.CreateOperation;
import artefact.umlClassDiagram.CreateOperationRefinement;
import artefact.umlClassDiagram.CreatePackage;
import artefact.umlClassDiagram.impl.CreateGeneralizationImpl;

public class Artefact2File {

	static PrintStream out;

	public static void print(Artefact artefact, String fileName) {
		File dotFile = new File(fileName);
		FileOutputStream fout;
		
		try {
			fout = new FileOutputStream(dotFile);
			out = new PrintStream(fout);

			ConstructionPrimitive first = artefact.getFirst();
			out.println(printConstructionPrimitive(first)+";");
			System.out.println("Artefact2File.print(): First construction primitive: "+first.toString());
			ConstructionPrimitive next = first.getNext();

			while(next != null) {
				out.println(printConstructionPrimitive(next)+";");
				next = next.getNext();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static String printConstructionPrimitive(ConstructionPrimitive cp){
		if (cp instanceof CreateClass){
			CreateClass cc = (CreateClass)cp;
			return ("CreateClass("+cc.getName()+")");
		}

		if (cp instanceof CreateAssociation){
			CreateAssociation ca = (CreateAssociation)cp;
			return ("CreateAssociation("+ca.getSource()+","+ca.getTarget()+ ")");
		}

		if (cp instanceof CreatePackage){
			CreatePackage ca = (CreatePackage)cp;
			return ("CreatePackage("+ca.getName()+ ")");
		}

		if (cp instanceof CreateAttribute){
			CreateAttribute ca = (CreateAttribute)cp;
			return ("CreateAtribute("+ca.getName()+","+ca.getOwner()+ ")");
		}

		if (cp instanceof CreateOperation){
			CreateOperation ca = (CreateOperation)cp;
			return ("CreateOperation("+ca.getName()+","+ca.getOwener()+ ")");
		}

		if (cp instanceof CreateOperationRefinement){
			CreateOperationRefinement ca = (CreateOperationRefinement)cp;
			return ("CreateOperationRefinement("+ca.getName()+","+ca.getOwener() +","+ca.getSuper()+")");
		}
		
		if(cp instanceof CreateGeneralization) {
			CreateGeneralizationImpl ca = (CreateGeneralizationImpl)cp;
			return ("CreateOperationRefinement("+ca.getSub()+","+ca.getSuper()+ ")");
		}
		 if(cp instanceof CreateFSTNonTerminal){
		   	   CreateFSTNonTerminal nt = (CreateFSTNonTerminal)cp;
		   	   if (nt.getNode()!=null){
		   		   return ("CreateFSTNonterminal("+nt.getName()+","+nt.getType());
		   	   }
		   	   else  return ("CreateFSTNonterminal("+nt.getName()+","+nt.getType());
			   }

		      if(cp instanceof CreateFSTTerminal){
		   	   CreateFSTTerminal nt = (CreateFSTTerminal)cp;
		   	   if (nt.getNode()!=null){
		   		   return ("CreateFSTTerminal("+nt.getName()+","+nt.getType());
		   	   }
		   	   else  return ("CreateFSTTterminal("+nt.getName()+","+nt.getType());
			   }
		return "CreateCP??";
	}

}
