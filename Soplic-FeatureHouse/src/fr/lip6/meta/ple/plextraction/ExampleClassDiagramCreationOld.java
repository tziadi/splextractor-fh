package fr.lip6.meta.ple.plextraction;

import java.util.ArrayList;



import artefact.generic.Artefact;
import artefact.generic.ConstructionPrimitive;
//import artefact.generic.export.Artefact2File;
import artefact.umlClassDiagram.impl.UmlClassDiagramFactoryImpl;
import artefact.umlClassDiagram.*;
public class ExampleClassDiagramCreationOld {

	public static String printConstructionPrimitive(ConstructionPrimitive cp){
		
		if (cp instanceof CreateClass){
			CreateClass cc = (CreateClass)cp;
			return ("CreateClass("+cc.getName()+")");
		}
		
		if (cp instanceof CreateAssociation){
			CreateAssociation ca = (CreateAssociation)cp;
			return ("CreateAssociation("+ca.getSource()+","+ca.getTarget()+ ")");
		}
		
		if (cp instanceof CreatePackage){
			CreatePackage ca = (CreatePackage)cp;
			System.out.println("TESTING PACKAGE :"+ca.getName());
			return ("CreatePackage("+ca.getName()+ ")");
		}
		
		if (cp instanceof CreateAttribute){
			CreateAttribute ca = (CreateAttribute)cp;
			
			System.out.println("TESTING ATTRUBUTE :"+ca.getName());
			return ("CreatePackage("+ca.getName()+ ")");
		}
		
		if (cp instanceof CreateOperation){
			CreateOperation ca = (CreateOperation)cp;
			return ("CreateOperation("+ca.getName()+ ")");
		}
		
		return "CreateUML??";
		}
	
	
	public static ArrayList<Artefact> createBankClassDiagrams(){
		
		
		ArrayList<Artefact> artefacts = new ArrayList<Artefact>();
		
		//Create Class Diagrams of product 1
		
		
		UmlClassDiagramFactoryImpl factory = new UmlClassDiagramFactoryImpl();
		
		
		ClassDiagram productCD1 = factory.createClassDiagram();
		
		CreateClass modelC1 = factory.createCreateClass();
		productCD1.setFirst(modelC1);
		modelC1.setName("Product 1");

		
		
		CreateClass bankC = factory.createCreateClass();
		modelC1.setNext(bankC);
		bankC.setName("Bank");
		
		CreateClass AccountWithLimitC = factory.createCreateClass();
		bankC.setNext(AccountWithLimitC);
		AccountWithLimitC.setName("AccountWithLimit");
		
		CreateClass ClientC = factory.createCreateClass();
		AccountWithLimitC.setNext(ClientC);
		ClientC.setName("Client");
		
		CreateAssociation bankAcc = factory.createCreateAssociation();
		bankAcc.setSource("Bank");
		bankAcc.setTarget("AccountWithLimit");
		ClientC.setNext(bankAcc);
		
		CreateAssociation bankCl = factory.createCreateAssociation();
		bankCl.setSource("Bank");
		bankCl.setTarget("Client");
		bankAcc.setNext(bankCl);
		
		CreateAssociation AccCl = factory.createCreateAssociation();
		AccCl.setSource("AccountWithLimit");
		AccCl.setTarget("Client");
		bankCl.setNext(AccCl);
		
		artefacts.add(productCD1);
		
	//GenerateDOT.printDot(FactoryLTS.createLTSforArtefact(productCD1), "Arefatct1.dot");
		
		//Create Class Diagrams of product 2
		
		
		
		
		ClassDiagram productCD2 = factory.createClassDiagram();
		
		CreateClass modelC2 = factory.createCreateClass();
		productCD2.setFirst(modelC2);
		modelC2.setName("Product 2");
		
		
		
		
		CreateClass bankC2 = factory.createCreateClass();
		modelC2.setNext(bankC2);
		bankC2.setName("Bank");
		
		CreateClass AccountWithoutLimitC2 = factory.createCreateClass();
		bankC2.setNext(AccountWithoutLimitC2);
		AccountWithoutLimitC2.setName("AccountWithLimit");
		
		CreateClass ClientC2 = factory.createCreateClass();
		AccountWithoutLimitC2.setNext(ClientC2);
		ClientC2.setName("Client");
		
		CreateClass ConverterC2 = factory.createCreateClass();
		AccountWithoutLimitC2.setNext(ConverterC2);
		ConverterC2.setName("Converter");
		
		CreateAssociation bankAcc2 = factory.createCreateAssociation();
		bankAcc2.setSource("Bank");
		bankAcc2.setTarget("AccountWithLimit");
		ConverterC2.setNext(bankAcc2);
		
		CreateAssociation bankCl2 = factory.createCreateAssociation();
		bankCl2.setSource("Bank");
		bankCl2.setTarget("Client");
		bankAcc2.setNext(bankCl2);
		
		CreateAssociation AccCl2 = factory.createCreateAssociation();
		AccCl2.setSource("AccountWithLimit");
		AccCl2.setTarget("Client");
		bankCl2.setNext(AccCl2);
		
		CreateAssociation ConvBan = factory.createCreateAssociation();
		ConvBan.setSource("Bank");
		ConvBan.setTarget("Converter");
		AccCl2.setNext(ConvBan);
		
		artefacts.add(productCD2);
		
		//GenerateDOT.printDot(FactoryLTS.createLTSforArtefact(productCD2), "Arefatct2.dot");
		
		
//Create Class Diagrams of product 3
		
		
		
		
		ClassDiagram productCD3 = factory.createClassDiagram();
		
		CreateClass modelC3 = factory.createCreateClass();
		productCD3.setFirst(modelC3);
		modelC3.setName("Product 3");
		
		CreateClass bankC3 = factory.createCreateClass();
		modelC3.setNext(bankC3);
		bankC3.setName("Bank");
		
		CreateClass ClientC3 = factory.createCreateClass();
		bankC3.setNext(ClientC3);
		ClientC3.setName("Client");
		
		
		
		
		CreateClass AccountWithLimitC3 = factory.createCreateClass();
		ClientC3.setNext(AccountWithLimitC3);
		AccountWithLimitC3.setName("AccountWitLimit");
		
		CreateClass ConverterC3 = factory.createCreateClass();
		AccountWithLimitC3.setNext(ConverterC3);
		ConverterC3.setName("Converter");
		
		
		
		CreateAssociation bankAcc3 = factory.createCreateAssociation();
		bankAcc3.setSource("Bank");
		bankAcc3.setTarget("AccountWithLimit");
		AccountWithLimitC3.setNext(bankAcc3);
		
		CreateAssociation bankCl3 = factory.createCreateAssociation();
		bankCl3.setSource("Bank");
		bankCl3.setTarget("Client");
		bankAcc3.setNext(bankCl3);
		
		CreateAssociation AccCl3 = factory.createCreateAssociation();
		AccCl3.setSource("AccountWithLimit");
		AccCl3.setTarget("Client");
		bankAcc3.setNext(AccCl3);
		
		CreateAssociation ConvBan3 = factory.createCreateAssociation();
		ConvBan3.setSource("Bank");
		ConvBan3.setTarget("Converter");
		AccCl3.setNext(ConvBan3);
		
		artefacts.add(productCD3);
		
		//GenerateDOT.printDot(FactoryLTS.createLTSforArtefact(productCD3), "Arefatct3.dot");
		
	/*	
		ArrayList<LTS> products = new ArrayList<LTS>();
		products.add(FactoryLTS.createLTSforArtefact(productCD1));
		products.add(FactoryLTS.createLTSforArtefact(productCD2));
		products.add(FactoryLTS.createLTSforArtefact(productCD3));
		
		GenerateDOT.printDot(FactoryLTS.createInitialLTSforKTail(products), "ktailInitial.dot");
		*/
		return artefacts;
		
	}
}
