package fr.lip6.meta.ple.plextraction;

import java.io.BufferedReader;
//import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
//import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

//import org.eclipse.emf.ecore.EObject;

import fr.lip6.meta.ple.featureIdentification.Feature;
import fr.lip6.meta.ple.featureIdentification.Product;
//import fr.lip6.meta.ple.featureIdentification.SiFeatureExtraction;

public class ResultValidation {

	 Map<String, String> map = new HashMap<String, String>();
	
	public  void openMap(String nameTestCase){
		
		 ArrayList<String> tabs = new ArrayList<String>();
			//lecture du fichier texte	
			try{
				InputStream ips=new FileInputStream("generatedFiles/"+nameTestCase+"/"+nameTestCase+"MapResult.txt"); 
				InputStreamReader ipsr=new InputStreamReader(ips);
				BufferedReader br=new BufferedReader(ipsr);
				String ligne;
				while ((ligne=br.readLine())!=null){
					//traitement d'une ligne 
					StringTokenizer stk  = new StringTokenizer(ligne);
					String ft=null;
					String fn = null;
					int i=0;
					while (stk.hasMoreElements()){
					
					if (i==0) {
						ft =stk.nextToken();
					}
					else  fn =stk.nextToken();
					i++;
					}
					
										
					
					
							map.put(ft, fn);
						
						
					
					
					System.out.println(ligne);
					tabs.add(ligne);
				}
				br.close(); 
			}		
			catch (Exception e){
				System.out.println(e.toString());
			}
		 
			
			System.out.println("End Map creation");
	}
		
	
	
	public  void validate(String nameTestCase) throws IOException{
		
		
		this.openMap(nameTestCase);
		
		System.out.println("creation");
		
		FileWriter fw = new FileWriter("generatedFiles/"+nameTestCase+"/"+nameTestCase+"Rasult.txt");
		 PrintWriter pw = new PrintWriter(fw);
		
		 
		
			//lecture du fichier texte	
			try{
				InputStream ips=new FileInputStream("generatedFiles/"+nameTestCase+"/"+nameTestCase+"outputProduct.txt"); 
				InputStreamReader ipsr=new InputStreamReader(ips);
				BufferedReader br=new BufferedReader(ipsr);
				String ligne;
				while ((ligne=br.readLine())!=null){
					//traitement d'une ligne 
					
					StringTokenizer stk  = new StringTokenizer(ligne);
					
					while (stk.hasMoreElements()){
						String token =stk.nextToken();
						if (token.contains("c")){
							
							pw.print(token+"  ");
						}
						
						if (token.contains("F")){
							String nf =map.get(token) ;
							pw.print(nf+ "  ");
						}
						
						
					}
					pw.println("");
					
				}
				br.close(); 
			}		
			catch (Exception e){
				System.out.println(e.toString());
			}
		 
			fw.close();
	}
	
	public void printInitialFile(Collection<Feature> features, String nameTestCase) throws IOException{
		
		FileWriter fw = new FileWriter("generatedFiles/"+nameTestCase+"/"+nameTestCase+"MapResult.txt");
		 PrintWriter pw = new PrintWriter(fw);
		 
		 
		 for (Feature f : features){
			 
			 pw.println(f.getId()); 
		 }
	}
	
	public void printResult(Collection<Product> AllP, Collection<Feature> features ,Map<String, String> map, String nameTestCase) throws IOException{
		
		
		
		
		FileWriter fw = new FileWriter("generatedFiles/"+nameTestCase+"/"+nameTestCase+"outputProduct.txt");
		 PrintWriter pw = new PrintWriter(fw);
		 
		 
		 
		
		for (Product prod : AllP) {
			
			pw.print(prod.getId()+ " :" );
			for (Feature f : prod.featuresImplemented(features)) {
				 
				String ft = map.get(f.getId());
				pw.print(ft+ "   ");
			}
			pw.print("");
		}
       
		
		
		
	}
	
	private ArrayList<String>  readFile(String f){
		
		ArrayList<String> tabs = new ArrayList<String>();
		//lecture du fichier texte	
		try{
			InputStream ips=new FileInputStream(f); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String ligne;
			while ((ligne=br.readLine())!=null){
				System.out.println(ligne);
				tabs.add(ligne);
			}
			br.close(); 
		}		
		catch (Exception e){
			System.out.println(e.toString());
		}
		return tabs;
	}
	
	public void printConfiguration(String nameTestCase) throws IOException{
		
		FileWriter fw = new FileWriter("generatedFiles/"+nameTestCase+"/"+nameTestCase+"inputProduct.txt");
		 PrintWriter pw = new PrintWriter(fw);
		
		ArrayList<String> tab=null;
		
		for (int i=1; i<=219; i++)
		{
			String p ="c"+i;
			String fileName = "GPL219/generatedConfs/c"+i+".config";
			//File f = new File(fileName);
			tab=readFile(fileName);
			pw.print(p+ " : ");
			for(String st:tab){
			    pw.print(st + "  ");	
				
			}
			pw.println("");
		}
       
	}
}
