/**
 * 
 */
package fr.lip6.meta.tools.info;

import java.io.PrintStream;

/**
 * @author Simon Grandsire
 *
 */
public class Tracing {
	private static String indent = "";
	
	/*
	 * public:
	 */
	
	public static void out(String message) {
		out.println(message);
	}
	
	public static void out(Object component, String message) {
		out.println(getString(component)+" "+message);
	}
	
	public static void out(Object component, String message, int indent) {
		setIndent(indent);
		out.println(Tracing.indent+getString(component)+" "+message);
	}
	
	public static void out(String component, String message) {
		out.println("["+component+"]"+" "+message);
	}
	
	public static void out(String component, String message, int indent) {
		setIndent(indent);
		out.println(Tracing.indent+"["+component+"]"+" "+message);
	}
	
	public static void err(String message) {
		err.println(message);
	}
	
	public static void err(Object component, String message) {
		err.println(getString(component)+" "+message);
	}
	
	public static void err(Object component, String message, int indent) {
		setIndent(indent);
		err.println(Tracing.indent+getString(component)+" "+message);
	}
	
	public static void err(String component, String message) {
		err.println("["+component+"]"+" "+message);
	}
	
	public static void err(String component, String message, int indent) {
		setIndent(indent);
		err.println(Tracing.indent+"["+component+"]"+" "+message);
	}
	
	/*
	 * private:
	 */
	
	private static PrintStream out = System.out;
	private static PrintStream err = System.err;
	
	private static String getString(Object o) {
		return "["+o.getClass().getSimpleName()+"]";
	}
	
	private static void setIndent(int n) {
		indent = "";
		
		for(int i=0; i<n; i++) {
			indent += "  ";
		}
	}
}
