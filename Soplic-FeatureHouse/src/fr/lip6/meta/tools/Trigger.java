package fr.lip6.meta.tools;

/**
 * Classe qui represente le type de la transition
 * @author Neko
 *
 */
public abstract class Trigger {

	public abstract void display();
	public abstract boolean equals(Object o);
	
	
}
