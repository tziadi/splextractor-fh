package fr.lip6.meta.tools;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fr.lip6.meta.ple.featureIdentification.Feature;
import fr.lip6.meta.ple.featureIdentification.Product;

public abstract class PLConstructionUtil {
	
	/**
	 * @param feature
	 * @param products
	 * @return
	 */
	public static boolean isMandatory(	Feature feature,
										Collection<Product> products	) {
		Collection<Feature> featureAL = new ArrayList<Feature>();
		
		featureAL.add(feature);
		if(products==null)
		{
			return false;
		}
		for(Product prod : products) {
			List<Feature> featL = prod.featuresImplemented(featureAL);
			
			if (featL.size() == 0)
				return false;
		}
		
		return true;
	}
	
	/**
	 * @param feat
	 * @param parentFeat
	 * @param products
	 * @return
	 */
	public static boolean isMandatoryChild(	Feature feat,
											Feature parentFeat,
											Collection<Product> products	) {
		Collection<Feature> featAL = new ArrayList<Feature>();
		Collection<Feature> parentFeatAL = new ArrayList<Feature>();
		
		featAL.add(feat);
		parentFeatAL.add(parentFeat);
		
		for(Product prod : products) {
			List<Feature> parentFeatL = prod.featuresImplemented(parentFeatAL);
			
			if (parentFeat.getId().equals("F0") && feat.getId().equals("F0_0"))
				System.out.println("isMandatoryChild(): "+feat+" "+prod);
			
			if (parentFeatL.size() == 1) {
				List<Feature> featL = prod.featuresImplemented(featAL);
				
				if (featL.size() == 0)
					return false;
			}
		}
		
		return true;
	}
	
	/**
	 * @param feat
	 * @param products
	 * @return
	 */
	public static Map<Integer, Product> getFeatureProducts(Feature feat, Collection<Product> products) {
		Map<Integer, Product> featureProducts = new HashMap<Integer, Product>();
		
		for(Product prod : products) {
			if (feat.getProdIds().contains(prod.getId())) {
				featureProducts.put(prod.getId(), prod);
			}
		}
		
		return featureProducts;
	}
	
	/**
	 * @param parentId
	 * @param features
	 * @return children of named parent feature <em>parentId</em> among the features
	 */
	public static ArrayList<Feature> getSubfeaturesLevel1(String parentId,
			Collection<Feature> features) {
		ArrayList<Feature> subFeatures = new ArrayList<Feature>();
		
		for (Iterator<Feature> iter1 = features.iterator(); iter1.hasNext();	) { 	
			Feature f = iter1.next();
			String name[] = f.getId().split("_");
			
			if (name.length == 2) {
				if (name[0].equals(parentId)) {
					subFeatures.add(f);
				}
			}
		}

		return subFeatures;
	}

	/**
	 * @param parentId
	 * @param features
	 * @return children of named parent feature <em>parentId</em> among the features
	 */
	public static ArrayList<Feature> getSubfeaturesLevel2(String parentId,
			Collection<Feature> features) {
		ArrayList<Feature> subFeatures = new ArrayList<Feature>();
		
		for (Iterator<Feature> iter1 = features.iterator(); iter1.hasNext();	) { 	
			Feature f = iter1.next();
			String parentName[] = parentId.split("_");
			String res = parentName[0]+parentName[1];

			String name[] = f.getId().split("_");
			if (name.length == 3) {
				String s = name[0]+name[1];
				if (s.equals(res)) {
					subFeatures.add(f);
				}
			}
		}

		return subFeatures;
	}
	
	/**
	 * @param f
	 */
	public static void deleteRecursive(File f) {
		if (f.exists()) {
			if (f.isDirectory()) {
				File[] childs = f.listFiles();
				int i = 0;
				for(i=0; i<childs.length; i++) {
					deleteRecursive(childs[i]);
				}
			}
			f.delete();
		}
	}
	
	/**
	 * @param features
	 * @param products
	 * @return
	 */
	/*public static Feature findBaseFeature(	Collection<Feature> features,
											Collection<Product> products	) {
		ArrayList<Feature> allF = new ArrayList<Feature>(features);
		
		for(Feature feat : features) {
			for(Product prod : products) {
				if (!feat.getProdIds().contains(prod.getId())) {
					allF.remove(feat);
					break;
				}
			}
		}
		
		if (allF.size() == 1)
			return allF.get(0);
		else
			return null;
	}*/
}