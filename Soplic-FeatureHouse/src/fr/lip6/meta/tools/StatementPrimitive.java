package fr.lip6.meta.tools;

import de.ovgu.cide.fstgen.ast.FSTTerminal;
import fr.lip6.meta.splicfh.LanguageConfigurator;
import artefact.FST.CreateFSTNonTerminal;
import artefact.FST.CreateFSTTerminal;
import artefact.generic.ConstructionPrimitive;
import artefact.umlClassDiagram.CreateAssociation;
import artefact.umlClassDiagram.CreateAttribute;
import artefact.umlClassDiagram.CreateClass;
import artefact.umlClassDiagram.CreateGeneralization;
import artefact.umlClassDiagram.CreateInterface;
import artefact.umlClassDiagram.CreateOperation;
import artefact.umlClassDiagram.CreateOperationRefinement;
import artefact.umlClassDiagram.CreatePackage;

public class StatementPrimitive extends Trigger {
	private ConstructionPrimitive primitive;

	public StatementPrimitive(ConstructionPrimitive primitive) {
		this.primitive = primitive;
	}

	@Override
	public void display() {
		//System.out.println("Creation Statement :"+
		//this.getClass().getName());
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		else if((o == null) || (o.getClass() != this.getClass()))
			return false;
		else {
			StatementPrimitive st = (StatementPrimitive) o;
			
			if (this.primitive.getClass() != st.primitive.getClass())
				return false;

			if (st.primitive instanceof CreateClass) {
				CreateClass cc = (CreateClass)st.primitive;
				CreateClass oo = (CreateClass)this.primitive;
				boolean res = ( ("CreateClass("+cc.getName()+")").equals(
						"CreateClass("+oo.getName()+")")&&cc.getOwener().equals(oo.getOwener()));

				//System.out.println("EQUALS Create CLASS :"+("CreateClass("+cc.getName()+")"+ " and "+
				//	"CreateClass("+oo.getName()+")")+"==>"+res );

				return res;
			}

			if (st.primitive instanceof CreateGeneralization) {
				CreateGeneralization ca = (CreateGeneralization)st.primitive;
				CreateGeneralization coo = (CreateGeneralization)this.primitive;
				String p1=ca.getlien().split("::")[1];
				String p2=coo.getlien().split("::")[1];
				return (("CreateGeneralization("+ca.getSub()+","+ca.getSuper()+")").equals(
						"CreateGeneralization("+coo.getSub()+","+coo.getSuper()+")")&&p1.equals(p2));
			}

			if (st.primitive instanceof CreateClass) {
				CreateClass ca = (CreateClass)st.primitive;
				CreateClass coo = (CreateClass)this.primitive;
				return ("CreateClass("+ca.getName()+","+ca.getOwener()+")").equals(
						"CreateClass("+coo.getName()+","+coo.getOwener()+")");
			}

			if (st.primitive instanceof CreatePackage) {
				CreatePackage ca = (CreatePackage)st.primitive;
				CreatePackage coo = (CreatePackage)this.primitive;
				return ("CreatePackage("+ca.getName()+")").equals(
						"CreatePackage("+coo.getName()+")");
			}

			if (st.primitive instanceof CreateAttribute) {
				CreateAttribute ca = (CreateAttribute)st.primitive;
				CreateAttribute coo = (CreateAttribute)this.primitive;
				String p1=ca.getlien().split("::")[1];
				String p2=coo.getlien().split("::")[1];
				return (ca.getName().equals(coo.getName())&&ca.getOwner().equals(coo.getOwner())&&p1.equals(p2));
			}

			if (st.primitive instanceof CreateOperation && !(primitive instanceof CreateOperationRefinement)) {
				CreateOperation crOp = (CreateOperation) st.primitive;
				CreateOperation crOpThis = (CreateOperation) this.primitive;
				String[] liens = crOp.getlien().split("::");
				String[] liensThis = crOpThis.getlien().split("::");
				String pckg = liens[1];
				String pckgThis = liensThis[1];
				String parameters = liens.length > 2 ? liens[2] : null;
				String parametersThis = liensThis.length > 2 ? liensThis[2] : null;

				return (	pckg.equals(pckgThis)
						&&	crOp.getOwener().equals(crOpThis.getOwener())
						&&	crOp.getName().equals(crOpThis.getName())
						&&	(	(parameters != null && parametersThis != null && parameters.equals(parametersThis))
							||	(parameters == null && parametersThis == null)	)
				);
			}
			
			if (st.primitive instanceof CreateOperationRefinement) { // redefinition
				CreateOperationRefinement crOpRef = (CreateOperationRefinement) st.primitive;
				CreateOperationRefinement crOpRefThis = (CreateOperationRefinement) this.primitive;
				String[] liens = crOpRef.getlien().split("::");
				String[] liensThis = crOpRefThis.getlien().split("::");
				String pckg = liens[1];
				String pckgThis = liensThis[1];
				String parameters = liens.length > 2 ? liens[2] : null;
				String parametersThis = liensThis.length > 2 ? liensThis[2] : null;

				return (	pckg.equals(pckgThis)
						&&	crOpRef.getSuper().equals(crOpRefThis.getSuper())
						&&	crOpRef.getOwener().equals(crOpRefThis.getOwener())
						&&	crOpRef.getName().equals(crOpRefThis.getName())
						&&	(	(parameters != null && parametersThis != null && parameters.equals(parametersThis))
							||	(parameters == null && parametersThis == null)	)
				);
			}

			if (st.primitive instanceof CreateInterface) {
				CreateInterface ca = (CreateInterface)st.primitive;
				CreateInterface coo = (CreateInterface)this.primitive;

				return (ca.getName().equals(coo.getName())&&ca.getOwener().equals(coo.getOwener()));
			}
			
			 if(st.primitive instanceof CreateFSTNonTerminal){
		    	   CreateFSTNonTerminal nt = (CreateFSTNonTerminal)st.primitive;
		    	   CreateFSTNonTerminal nt2 = (CreateFSTNonTerminal)this.primitive;
		    	   return nt.getName().equals(nt2.getName())&&nt.getType().equals(nt2.getType());
			   }

		       if(st.primitive instanceof CreateFSTTerminal){
		    	   CreateFSTTerminal nt = (CreateFSTTerminal)st.primitive;
		    	   CreateFSTTerminal nt2 = (CreateFSTTerminal)this.primitive;
		    	   if (LanguageConfigurator.getLanguage().isMethod(nt.getNode()) || 
		    			   LanguageConfigurator.getLanguage().isConstructor(nt.getNode())){
		    		   
		    		   if (LanguageConfigurator.getLanguage().isMethod(nt2.getNode())
		    				   || 
		    				   LanguageConfigurator.getLanguage().isConstructor(nt2.getNode())){
		    			   FSTTerminal sftn1 = (FSTTerminal)nt.getNode();
		    			   FSTTerminal sftn2 = (FSTTerminal)nt2.getNode();
		    			   
		    			   return sftn1.getName().equals(sftn2.getName());
		    			   //sftn1.getBody().equals(sftn2.getBody());
		    		   }
		    	   }
		    	   
		    	   
		    	   
		    	   return nt.getName().equals(nt2.getName())&&nt.getType().equals(nt2.getType());
			   }
		}
		
		return false;
	}	


	public String toString(){

		if (primitive instanceof CreateClass){
			if(primitive.getlien()==null)
			{
				CreateClass cc = (CreateClass)this.primitive;
				return ("CreateClass("+cc.getName()+","+cc.getOwener()+ ")");
			}
			else
			{

				CreateClass cc = (CreateClass)this.primitive;
				return ("CreateClass("+cc.getName()+","+cc.getOwener()+ ","+cc.getlien()+")");

			}
		}

		if (primitive instanceof CreateInterface){
			if(primitive.getlien()==null)
			{
				CreateInterface cc = (CreateInterface)this.primitive;
				return ("CreateInterface("+cc.getName()+","+cc.getOwener()+ ")");
			}
			else
			{

				CreateInterface cc = (CreateInterface)this.primitive;
				return ("CreateInterface("+cc.getName()+","+cc.getOwener()+ ","+cc.getlien()+")");

			}
		}
		if (primitive instanceof CreateAssociation){
			CreateAssociation ca = (CreateAssociation)this.primitive;
			return ("CreateAssociation("+ca.getSource()+","+ca.getTarget()+")");
		}

		if (primitive instanceof CreatePackage){
			CreatePackage ca = (CreatePackage)primitive;
			if(primitive.getlien()==null)
			{
				return ("CreatePackage("+ca.getName()+ ")");
			}
			else{

				return ("CreatePackage("+ca.getName()+ "," +ca.getlien()+")");
			}
		}

		if (primitive instanceof CreateAttribute){
			CreateAttribute ca = (CreateAttribute)primitive;
			if(primitive.getlien()==null)
			{
				return ("CreateAttribute("+ca.getName()+","+ca.getOwner()+ ")");
			}
			else
			{
				return ("CreateAttribute("+ca.getName()+","+ca.getOwner()+ ","+ca.getlien()+")");	
			}
		}

		if (primitive instanceof CreateOperation && !(primitive instanceof CreateOperationRefinement)) {
			CreateOperation ca = (CreateOperation)primitive;
			if(primitive.getlien()==null)
			{
				return ("CreateOperation("+ca.getName()+","+ca.getOwener()+ ")");
			}
			else
			{
				return ("CreateOperation("+ca.getName()+","+ca.getOwener()+ ","+ca.getlien()+")");
			}
		}

		if (primitive instanceof CreateOperationRefinement){
			if(primitive.getlien()==null)
			{
				CreateOperationRefinement ca = (CreateOperationRefinement) primitive;
				return ("CreateOperationRefinement("+ca.getName()+","+ca.getOwener()+ ","+ca.getSuper()+")");
			}
			else
			{
				CreateOperationRefinement ca = (CreateOperationRefinement) primitive;
				return ("CreateOperationRefinement("+ca.getName()+","+ca.getOwener()+ ","+ca.getSuper()+","+ca.getlien()+")");	
			}
		}

		if (primitive instanceof CreateGeneralization)
		{
			if(primitive.getlien()==null)
			{
				CreateGeneralization ca = (CreateGeneralization)primitive;
				return ("CreateGeneralization("+ca.getSub()+","+ca.getSuper()+ ")");
			}
			else
			{
				CreateGeneralization ca = (CreateGeneralization)primitive;
				return ("CreateGeneralization("+ca.getSub()+","+ca.getSuper()+","+ca.getlien()+ ")");	
			}
		}

		 if(primitive instanceof CreateFSTNonTerminal){
		   	   CreateFSTNonTerminal nt = (CreateFSTNonTerminal)primitive;
		   	   if (nt.getNode()!=null){
		   		   return ("CreateFSTNonterminal("+nt.getName()+","+nt.getType());
		   	   }
		   	   else  return ("CreateFSTNonterminal("+nt.getName()+","+nt.getType());
			   }

		      if(primitive instanceof CreateFSTTerminal){
		   	   CreateFSTTerminal nt = (CreateFSTTerminal)primitive;
		   	   if (nt.getNode()!=null){
		   		   return ("CreateFSTTerminal("+nt.getName()+","+nt.getType());
		   	   }
		   	   else  return ("CreateFSTTterminal("+nt.getName()+","+nt.getType());
			   }

		return primitive.toString();

	}

	public ConstructionPrimitive getPrimitive() {
		return primitive;
	}

	public void setPrimitive(ConstructionPrimitive primitive) {
		this.primitive = primitive;
	}
}
