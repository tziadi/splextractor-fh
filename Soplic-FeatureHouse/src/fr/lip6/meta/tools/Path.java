package fr.lip6.meta.tools;

import java.util.ArrayList;

import fr.lip6.meta.tools.Trigger;


public class Path {

	private ArrayList<Trigger> path;
	
	public Path(){
		path = new ArrayList<Trigger>();
	}
	
	public void addTriggerToPath(Trigger t){
		
		path.add(t);
	}
	
	
	public ArrayList<Trigger> getPath() {
		return path;
	}

	public void setPath(ArrayList<Trigger> path) {
		this.path = path;
	}

	public boolean equals(Object path){
		
		if (!(path instanceof Path)) return false;
		
		else {

			Path p = (Path) path;

			if (this.getPath().size()!=p.getPath().size())
				return false;
		
			for (int i =0; i<this.getPath().size(); i++){
				/*Trigger t = */p.getPath().get(i);
				if (!this.path.get(i).equals(p.getPath().get(i))) return false;
			}
			
		}

		return true;
		
	}

	public ArrayList<Path> concatPath(ArrayList<Path> outPaths) {
		// TODO Auto-generated method stub
		
		
		ArrayList<Path> result = new ArrayList<Path>();
		//Path p;
		if (outPaths.size()==0)
			
		{
			
		  result.add(this);
			
		}
		
		else {
		for(Path path : outPaths){
		
		 result.add(this.concat(path));
			
		}
		}
		return result;
	}
	
	public Path concat(Path p){
		
		Path result = new Path();
		for(Trigger t : this.path){
			
			result.addTriggerToPath(t);
		}
		
		for (Trigger tr : p.getPath()){
			
			result.addTriggerToPath(tr);
		}
		
		return result;
	}
	
	
	 public boolean belongTo(ArrayList<Path> array){

		   boolean found = false;
		   int i =0;
		   while (!found && i<array.size()) {
			   
			   if (this.equals(array.get(i)))

				   found = true;
			   
			   i++;
		  }
		   
		   return found;
	   }
	   
	 
	 /*h
	  *  check equivalence between two paths
	  */
	 public boolean equivalent(Path p){
		 boolean result = true;
         int i=0;
		 
         if (p.size()!=this.size()) return false;
         
		 
		 while(result && i<p.size()){
			 
			 if (!p.getPath().get(i).equals(this.getPath().get(i))) 
				 result = false;
			 
			 i++;
		 }
		 
		 return result;
		 
		 
		 
	 }
	 
	 /*
	  * check inclusion between two paths
	  */
	 
	 public boolean inclusion(Path p){
		 
		 boolean result = true;
		 int i =0;
		 
		 if (p.size() <=this.size()){
			 
			 while (result && i<p.size()){
				 if (!p.getPath().get(i).equals(this.getPath().get(i))) 
					 result = false;
				 
				 
			 }
		 }
			 
			 else {
				 while (result && i<this.size()){
					 if (!this.getPath().get(i).equals(p.getPath().get(i))) 
						 result = false;
					 
					 
				 }
				 
				 
				 
			 }
			 
		return result; 
	 }
	 
	 /*
	  * 
	  */
	 public int size(){
		 
		 return path.size();
	 }
	 
	 
	 /*
	  * (non-Javadoc)
	  * @see java.lang.Object#toString()
	  */
	 public String toString(){
		 
		 String result = " ";
		 for (Trigger t : path){
			 result=result+"-"+t.toString();
		 }
		 return result;
	 }
}
